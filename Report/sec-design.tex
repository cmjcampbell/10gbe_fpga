\section{Architectural Design}
\subsection{System}
The system consists of six major components: a Dual XAUI to SFP+ HSMC board, a XAUI PHY, an XGE MAC, transmit and receive FIFOs, transmit and receive DMAs, and the HPS (fig. 1). The Dual XAUI to SFP+ HSMC board is a mezzanine card that interfaces the network and the FPGA. The XGE MAC, FIFOs, and DMAs make up the transmit and receive paths on the FPGA. The DMAs shuttle data between the FPGA and HPS SDRAM using the FPGA-to-SDRAM bridge. Additionally, the HPS communicates with the FPGA (e.g. to write descriptors to the DMAs) using the Lightweight HPS-to-FPGA AXI bridge.\\\\
\begin{center}
\includegraphics[scale=0.35]{images/design/system_architectural_design.jpg}\\
\small
Figure 1 - System architectural design.\\
\normalsize
\end{center}
\newpage
\subsection{Dual XAUI to SFP+ HSMC Board}
The Dual XAUI to SFP+ HSMC board is a mezzanine card from terasIC whose chief purpose is to interface the network with the FPGA\cite{terasic:xaui} (fig. 2). It interfaces with the network through SFP+ ports and it interfaces with the FPGA through the HSMC on the SoCKit board. On the network side, it has 2 full duplex 10G SFP+ channels and two independent SFI interfaces from the SFP+ cages to a Broadcom BCM8725 transceiver. An SFP+ copper twin-axial cable was used to connect one of the SFP+ channels to the network. The Broadcom BCM8725 is a Dual-Channel 10-GbE SFI-to-XAUI transceiver that is fully compliant with the 10-GbE IEEE 802.3aq standard\cite{broadcom:bcm8727}. On the HSMC side, it has two independent XAUI interfaces from the BCM8725 to the HSMC. Each XAUI interface has four high-speed serial transmit and receive lanes that operate at 3.125 Gbps. The high-speed serial lanes are connected to pins on the HSMC connector which interface directly with the FPGA.\\\\
\begin{center}
\includegraphics[scale=0.60]{images/xaui_component_diagram.png}\\
\small
Figure 2 - Dual XAUI to SFP+ HSMC board layout.\\
\normalsize
\end{center}
\newpage
\subsection{SoCKit Board \& Cyclone V FPGA SoC}
The SoCKit development board is a hardware design platform from terasIC built around the Altera Cyclone V SoC FPGA\cite{terasic:sockit} (fig. 3). The main components of the board that were used in the system are: the High Speed Mezzanine Connector (HSMC), the Cyclone V SoC FPGA, and the HPS DDR3 RAM. The HSMC is used to interface the Dual XAUI to SFP+ board with the FPGA. The Cyclone V SoC FPGA is used to implement the system, and the HPS DDR3 RAM is used as main memory by the Cyclone V HPS.  Additionally, the SoCKit board provides a 50 MHz oscillator that is used as the base clock for the system.\\\\
\begin{center}
\includegraphics[scale=0.55]{images/sockit_component_diagram.png}\\
\small
Figure 3 - SoCKit development board layout.\\
\normalsize
\end{center}
\newpage
The Cyclone V SX SoC FPGA (5CSXFX6D6F31C8ES) was used to implement the system\cite{altera:cyclonev} (fig. 4 \& 5). The Cyclone V SX integrates an ARM Cortex-A9 MPCore Hard Processor System (HPS) with the FPGA fabric using a high-bandwidth interconnect backbone. This allows the FPGA and HPS to easily communicate and use each other's resources, which was very important to the system design. The soft logic was implemented in the FPGA and the HPS was used run embedded Linux and serve as a client to the FPGA. The Cyclone V SX also has hard transceiver blocks that were used in the system. It has six 3.125 Gbps transceivers, four of which were used to implement a portion of the XAUI PHY. The fact that these transceivers are high-speed, low-power and low-cost, and are easily configurable is fundamental to the system. Their high-speed allows the system to reach 10 Gbps+ serial data rates, their low-power and low-cost keeps the cost of the system low, and their configurability makes it easy to implement protocols like XAUI.\\\\
\begin{center}
\includegraphics[scale=0.50]{images/cyclonev_diagram1.png}\\
\small
Figure 4 - Cyclone V FPGA fabric diagram.\\
\end{center}
\begin{center}
\normalsize
\includegraphics[scale=0.50]{images/cyclonev_diagram2.png}\\
\small
Figure 5 - Cyclone V HPS diagram.\\
\normalsize
\end{center}
\newpage
\subsection{XAUI PHY}
In this system the 10 Gigabit Media Independent Interface (XGMII)\cite{10gea:xaui} is used to interface the PHY (OSI layer 1) with the MAC (OSI layer 2). The strict timing requirements of XGMII (especially the standard DDR XGMII) make it difficult to route the  bus more than 7 cm; This makes chip-to-chip, board-to-board, and chip-to-optical module applications impractical when using XGMII. The 10 Gigabit Attachment Unit Interface (XAUI), which is part of the 10-GbE IEEE 802.3ae standard, is used to address the limitations of XGMII and extend it between the PHY and the MAC. XAUI is a full duplex interface that uses four self-clocked 3.125 Gbps serial differential links in both directions to achieve 10 Gbps data throughput. As discussed in section X (Architectural Design - Dual XAUI to SFP+ HSMC Board), the Dual XAUI to SFP+ HSMC board interfaces with the FPGA (where the PHY is implemented) using XAUI.\\\\
The Altera XAUI PHY IP core was used as the PHY in the system because it supports XAUI on the network-side and XGMII on the client-side (fig 6.).
\begin{center}
\includegraphics[scale=0.45]{images/xaui_toplevel.png}\\
\small
Figure 6 - XAUI PHY top level signals.\\
\normalsize
\newpage
\end{center}
The XAUI PHY implements the Physical Medium Attachment (PMA) and Physical Coding Sublayer (PCS) of the PHY\cite{altera:cyclonev2}. The PMA is implemented using the hard transceiver blocks in the Cyclone V SoC FPGA, but the PCS is implemented partially in the hard blocks and partially in soft logic (fig. 7).\\\\
\begin{center}
\includegraphics[scale=0.40]{images/xaui_phy_channels.png}\\
\small
Figure 7 - XAUI PHY PMA \& PCS block diagram.\\
\normalsize
\end{center}
On the receive PMA path there are three blocks, the receive buffer, the channel PLL, and the deserializer. The receiver buffer receives the serial data outside the XAUI PHY and feeds it to the  channel PLL. The channel PLL is configured as a clock data recovery (CDR) PLL and recovers the clock from the serial data stream. The deserializer, which uses a 10-bit deserialization factor, clocks in serial data from the receive buffer using the high-speed serial recovered clock and deserializes it using the low-speed parallel recovered clock. The deserializer forwards the deserialized data to the receive PCS.\\\\
On the receive PCS path there are six blocks, the byte deserializer, the RX phase compensation FIFO, the word aligner, the deskew FIFO, the reatematch FIFO, and the 8B/10B decoder. In configurations that have a receive PCS frequency greater than the upper limit of the FPGA fabric-transceiver interface frequency, the parallel received data cannot be forwarded directly to the FPGA fabric. In such configurations, the byte deserializer is used to reduce the FPGA fabric transceiver interface frequency to half while doubling the parallel data width. The byte deserializer was used in this system to ensure that the upper limit was not being violated. The RX phase compensation FIFO compensates for the phase difference between the low-speed parallel clock and the FPGA fabric interface clock. The word aligner restores the word boundary of the upstream transmit, which is lost during deserialization in the PMA and PCS. During link synchronization, the word aligner searches for a pre-defined alignment pattern in the deserialized data and uses it to identify and restore the correct word boundary. After word alignment, the deskew FIFO aligns the four XAUI lanes. The rate match FIFO compensates for small clock frequency differences between the upstream transmit and the local receive clocks by inserting or deleting skip symbols. The 8B/10B decoder is used to decode the 8-bit code groups and indicate whether they are valid data code groups or control code groups. It operates in single-width and double-width mode, but double-width mode was used in this system because the data-path is operating in double-width mode. In single-width mode, the 8B/10B decoder decodes the received 10-bit code groups into 8 data bits and 1 control bit. The control bit indicates if the deoded 8-bit code is data or a control code. In double-width mode, two 8B/10B decoders are cascaded to decode 21-bit code groups into two sets of 8 data bits and 1 control bit. When receiving the 21-bit code group, the 10-bit code group least significant byte is decoded first and the ending running disparity is forwarded to the other 8B/10B decoder for decoding the 10-bit most significant byte.\\\\
On the transmit PCS path there are three blocks, the 8B/10B encoder, the TX phase compensation FIFO, and the byte serializer. Like the 8B/10B decoder in the receive path, the 8B/10B encoder operates in single-width and double-width mode, but double-width mode was used in this system because the data-path is operating in double-width mode. 8B/10B encoding limits the maximum number of consecutive 1s and 0s in the serial data stream to five, ensuring DC balance as well as enough transitions for the receive to successfully recover the clock. In single-width mode, the 8B/10B encoder generates 10-bit code groups from 8 data bits and 1 control bit with proper disparity. The 10-bit code groups are generated as valid data code-groups or special control
code-groups, depending on the 1-bit control identifier.In double-width mode, two 8B/10B encoders are cascaded to generate two sets of 10-bit code groups from 15 data bits and 2 control bits. When receiving the 15-bit data, the 8-bit least significant byte is encoded first, followed by the 8-bit most significant byte. The TX phase compensation FIFO compensates for the phase difference between the FPGA fabric interface clock and the low-speed parallel clock. The byte serializer divides the parallel data width by two to run the transceiver channel at higher data rates while keeping the FPGA fabric interface frequency within the maximum limit.\\\\
On the transmit PMA path there is two blocks, the serializer and the transmit buffer. The serializer, which uses a 10-bit serializaton factor, converts the incoming low-speed parallel data from the transmit PCS to high-speed serial data and sends it least significant bit first to the transmit buffer. The transmit buffer receives serial data from the serializer and sends it downstream.
\newpage
\subsection{XGE MAC}
Originally, we attempted to use Altera's 10 Gbps Ethernet MAC IP core as the system's MAC, but it was eventually determined that it is not compatible with the Cyclone V SoC FPGA. It was rigorously and systematically debugged for approximately a month until an Altera Knowledge Base article was found that stated it would not work with the Cyclone V\cite{altera:kb}. The knowledge base article stated that the issue was with version 13.1 SP1 of the IP core (the version we were using at the time) and that the issue would be resolved in a future version. We upgraded to version 15.1, but it appears that the issue is still unresolved.\\\\
After determining that Altera's 10 Gbps Ethernet MAC IP core was not compatible with the Cyclone V, we decided to implement our own MAC - the 10-Gigabit Ethernet MAC (XGE MAC). The XGE MAC is capable of receiving/transmitting Ethernet packets, CRC checking, and preamble generation. In order to do this the XGE MAC has to be able to convert data between the Altera Avalon-ST format and SDR-XGMII format while handling Frame Check Sequence (FCS) checking, preamble generation, and the interpacket gap (IPG).\\\\
\textbf{Data mapping between Avalon-ST to SDR-XGMII}
\begin{verbatim}
data_in[63:0] ---------> xgmii_out[71:0]
data_in   | 63:56 | 55:48 | 47:40 | 39:32 | 31:24 | 23:16 | 15:08 | 07:00 |
xgmii_out | 34:27 | 25:18 | 16:09 | 07:00 | 70:63 | 61:54 | 52:45 | 43:36 |
\end{verbatim}
As is shown above, from data\textunderscore in to xgmii\textunderscore out, the data being sent firstly reversed endianess then performed endianess reverse within each word.\\\\
The XGE MAC receive and transmit blocks were implemented as separate modules. The XGE MAX transmit module, receive module, and potential future work are discussed below.\\\\
\textbf{MAC TX}\\
The transmitting path of the MAC receives data with Avalon-ST format and outputs data to XAUI PHY with SDR-XGMII format. Upon that, Tx path should also handle pre-amble and FCS and leaving inter packets gap at leat 5 bytes. \\
To handle auto preamble on the transmitting path and meeting the Avalon-ST timing requirement, the input data has to be buffered for one clock cycle at least in order to insert preamble before the first packet shows on XGMII.\\
The output of MAC with XGMII format should be clock buffered otherwise the XAUI PHY will not regard the data transmitted as a packet.\\
To make it easier for further development, the MAC TX path is coded with a Moore finite state machine (FSM). 
The state diagram is shown in figure 8.\\\\
\begin{center}
\includegraphics[scale=0.50]{images/mactx_fsm.png}\\
\small
Figure 8 - Mac Tx state machine diagram.\\
\normalsize
\end{center}
IDLE: No streaming packets shown from Avalon-ST input, valid might be asserted but sop keeps deasserted.\\
SOP: Detected input sop, sending preamble on output.\\
SEND: Send data with XGMII format untill no more inputs data coming.\\
BUF: Send out the buffered data.\\
EOP: The end of XGMII data, might padding CRC depending on the tx\textunderscore empty input.\\
END: Output rest CRC and end of packet deliminator(FD) with rest data padded with 0xBC.\\\\
The MAC Tx timing diagram is shown in figure 9.
\begin{center}
\includegraphics[scale=0.70]{images/mactx_timing_diagram.png}\\
\small
Figure 9 - Mac Tx timing diagram.\\
\normalsize
\end{center}
As is shown on the timing diagram, two packets are being sent through the MAC Tx path. The output is actually delayed 2 clock cycles due to input buffer and output buffer repectively. tx\textunderscore ready signal is deasserted when MAC is still sending buffered data when there will possibly be data streaming into the MAC. 
From the FSM diagram we can see the MAC will go back to IDLE state when the input data is no longer valid namely dropping the packet being sent.\\\\
\textbf{MAC Rx}\\
The receiving path of the MAC receives data with XGMII format and output data to FIFO with Avalon-ST format. Originally, the data received from XAUI PHY may be aligned to either 1st or 5th byte of XGMII input, thanks to the align module implemented by previous group, the realization of MAC Rx can be simplified. However, based on signal tapping from rx XGMII signal, the XGMII data is corrupted if the packet length is not multiple of 4.
Eg. when receiving 60bytes of data, the last xgmii data would be 4bytes of CRC with 0XFDBCBCBC appended. when receiving 63 bytes of data, the last xgmii would be corrupted data and corrupted CRC with 0xFD appended.
This may due to the function error of XAUI PHY or due to the error packet output from Solarflare card and need to be further tested. To minimize delay and improve throughput, a Mealy FSM is implemented, output of Avalon-ST format is also buffered. Typically the delay from active input to active output is 3 clock cycles in current implementation. The state diagram is shown in figure 10.\\\\
\begin{center}
\includegraphics[scale=0.50]{images/macrx_fsm.png}\\
\small
Figure 10 - Mac Rx state machine diagram.\\
\normalsize
\end{center}
IDLE: No active data shown from XAUI PHY output, xgmii\textunderscore control remains FF.\\
START: Preamble shows on wire.\\
SOP: data on buffer is the start of the packet, assert sop signal and valid signal.\\
RECEIVE: data frame coming in, all xgmii\textunderscore control bits are 0, eop may be asserted based on control signal.\\
EOP: data ends in current XGMII data, assert eop, buffer crc bytes.\\
CRC: compare received CRC with computed CRC value and raise flag if equals.\\\\
The MAC Tx timing diagram is shown in figure 11.
\begin{center}
\includegraphics[scale=0.70]{images/macrx_timing_diagram.png}\\
\small
Figure 11 - Mac Rx timing diagram.\\
\normalsize
\end{center}
The output of rx\textunderscore data is the output buffered from internal data register which is the output of FSM. The data register outputs data from data\textunderscore buf which is the buffered input data. CRC is also calculated based on data\textunderscore buf. It is shown in the timing diagram that CRC is checked in CRC state and flag is raised because of checking successful.From the FSM diagram we can see the MAC Rx will return IDLE state when input ready is no longer asserted.\\\\
\textbf{Future Work}\\
There are other features need to be implemented to form a fully functional MAC:\\
\begin{enumerate}
\item Payload Padding: pad zeros when input data length is too short to send on wire.
\item MAC Address setup: by setting up MAC address, TX path can auto-set MAC address to relief data transfer burden. Rx Path can drop packets with wrong MAC destination address.
\item Wrong FCS dropping: now it's only implemented with a crc\textunderscore flag, further can implement with a register to setup whether to drop packets with wrong FCS.
\item CSMA/CD: implement CSMA/CD algorithm to avoid collision on wire.
\end{enumerate}
\newpage
\subsection{FIFOs}
The FIFOs were implemented using the Altera Avalon-ST Single Clock FIFO IP core\cite{altera:fifo} (fig. 12). The FIFO on the recieve path buffers data between the receive MAC and the receive mSGDMA. It has a 64-bit Avalon Stream (Avalon-ST) sink interface on the MAC-side and a 64-bit Avalon-ST source interface on the mSGDMA-side. To support the notion of packets, it was configured with the use packets feature. Using packets adds the following control signals: start-of-packet, end-of-packet, and empty. The empty signal indicates the number of empty symbols in the last packet data.\\\\
\begin{center}
\includegraphics[scale=0.40]{images/fifo_diagram.png}\\
\small
Figure 12 - Single clock FIFO diagram.\\
\normalsize
\end{center}
The FIFO on the transmit path buffers data between the transmit mSGDMA and the transmit MAC. It has a 64-bit Avalon-ST sink interface on the mSGDMA-side and a 64-bit Avalon-ST source interface on the MAC-side. Like the receive FIFO it is configured to use packets, but it is also configured to use fill level and store and forward. Using fill level and store and forward makes the FIFO buffer  an entire packet before it begins feeding it to the transmit MAC. This is very important to the system because the transmit MAC must be fed packet data contiguously (i.e. there cannot be any gaps in the packet data once the transmit MAC begins receiving it).
\newpage
\subsection{DMAs}
A 10GbE system that is constantly receiving and transmitting packets can quickly overwhelm the processor with data transfer requests. In order to offload data transfer work from the processor, the system uses Direct Memory Access (DMA). In DMA, the processor allows a DMA engine to temporarily master the memory bus so it can directly transfer data to and from main memory. In this system, the receive DMA receives a packet from the receive FIFO and transfers it to HPS main memory and the transmit DMA reads a packet from HPS main memory and transfers it to the transmit FIFO.\\\\
The DMAs were implemented using the Altera Modular Scatter-Gather DMA (mSGDMA)\cite{altera:peripherals}. The mSGDMA consists of a dispatcher block with optional read master and write master blocks. The descriptor block receives and decodes descriptors, which contain data transfer instructions, and dispatch the instructions to the read master and write master. The dispatcher has a descriptor buffer so it can receive multiple descriptors at once (i.e. a descriptor chain); the depth of the buffer can be configured at compile time. The read master and write master carry out any necessary operations and update relevant status registers. The read and write masters have data buffers whose depth can also be configured at compile time. The mSGDMA has two descriptor formats, a standard descriptor (fig. 13) format and an extended descriptor format (fig. 14). The extended descriptor format was used in this system because it supports burst transfers and the Blaze XGE driver (discussed in Section x Drivers - Blaze XGE) uses bursts. Burst transfers contribute to achieving high throughput because a single request can transfer larger chunks of data.\\\\
\begin{center}
\includegraphics[scale=0.40]{images/dma_standarddescriptor.png}\\
\small
Figure 13 - mSGDMA standard descriptor format.\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.40]{images/dma_extendeddescriptor.png}\\
\small
Figure 14 - mSGDMA extended descriptor format.\\
\normalsize
\end{center}
The receive mSGDMA implements a dispatcher block and a write master block (fig. 15). The write master block has a 64-bit Avalon-ST sink interface that receives packet data from the receive FIFO and a 64-bit Avalon-MM master interface that writes data to the HPS main memory via the HPS SDRAM controller. The mSGDMA Avalon-ST interface also uses packet data, so it knows when there is a start of packet, end of packet, and how many empty bytes there are in the last packet data.\\\\
\begin{center}
\includegraphics[scale=0.25]{images/design/rxdma_design.jpg}\\
\small
Figure 15 - Receive mSGDMA diagram.\\
\normalsize
\end{center}
The Avalon-MM interfaces for the mSGDMA and the SDRAM controller support burst transfers. When the mSGDMA receives a packet it interrupts the HPS processor through the HPS General Interrupt Controller (GIC). The Blaze XGE driver (discussed in Section x Drivers - Blaze XGE) handles the interrupt and begins writing descriptors to the mSGDMA dispatcher that instruct it to transfer the packet data to a ring buffer in the HPS main memory. The driver monitors the status of the transfer through the mSGDMA CSR registers (fig. 16) and stops writing descriptors to the mSGDMA when the transfer is complete. The driver can then check the success of the transfer through the mSGDMA response port (fig. 17).\\\\
\begin{center}
\includegraphics[scale=0.50]{images/dma_csr.png}\\
\small
Figure 16 - mSGDMA CSR registers.\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.50]{images/dma_responseport.png}\\
\small
Figure 17 - mSGDMA response port.\\
\normalsize
\end{center}
The transmit mSGDMA implements a dispatcher block and a read master block (fig. 18). The read master block has a 64-bit Avalon-MM master interface that reads from the HPS main memory via the HPS SDRAM controller and a 64-bit Avalon-ST source interface that sends packet data to the transmit FIFO. Like the receive mSGDMA, it is also configured to use packets. When the HPS wants to send a packet the Blaze XGE driver begins writing descriptors to the mSGDMA dispatcher that instruct it to transfer the packet data from a ring buffer in the HPS main memory. The mSGDMA interrupts the HPS processor through the GIC when it is complete.
\begin{center}
\includegraphics[scale=0.25]{images/design/txdma_design.jpg}\\
\small
Figure 18 - Transmit mSGDMA diagram.\\
\normalsize
\end{center}
\newpage
\subsection{SDRAM Controller}
The HPS SDRAM controller is used to access the HPS main memory (1 GB DDR3 SDRAM)\cite{terasic:sockithardware}. The SDRAM controller provides an interface between the FPGA fabric and the HPS\cite{altera:hps} (fig. 19). The interface accepts Avalon-MM transactions, converts those commands to the correct commands for the SDRAM, and manages the details of the SDRAM access. The SDRAM controller consists of a multi-port front end (MPFE) and a single-port controller. The MPFE provides multiple independent interfaces to the single-port controller (including an FPGA-to-SDRAM interface). The single-port controller communicates with and manages the external memory devices through the DDR PHY.  The DDR PHY provides a physical layer interface for read and write memory operations between the memory controller and external memory devices.
\begin{center}
\includegraphics[scale=0.40]{images/sdram_controller_diagram.png}\\
\small
Figure 19 - SDRAM controller block diagram.\\
\normalsize
\end{center}
The FPGA-to-HPS SDRAM interface provides masters implemented in the FPGA fabric access to the SDRAM controller. The interface has three ports types that can be used to construct AXI or Avalon-MM interfaces (fig. 20):\\
\begin{enumerate}
\item Command ports — issue read/write commands (for receive they write acknowledge responses)
\item 64-bit read data ports — receive data returned from a memory read
\item 64-bit write data ports — transmit data associated with a memory write\\
\end{enumerate}
\begin{center}
\includegraphics[scale=0.40]{images/sdram_controller_diagram2.png}\\
\small
Figure 20 - SDRAM controller ports.\\
\normalsize
\end{center}
In this system, a 64-bit Avalon-MM interface is used, which requires one command port, one read data port, and one write data port.
\section{Timing Design}
\subsection{System}
A 156.25 MHz PLL reference clock is supplied to the XAUI PHY. The XAUI PHY outputs another 156.25 MHz clock that is used as the input clock for all interfaces on the XGE MAC , FIFOs, DMAs, and bridges.
\subsection{XAUI PHY}
On the network-side, the XAUI PHY has four high-speed serial transmit and receive lanes that operate at 3.125 Gbps\cite{altera:xcvr} (fig. 21). On the receive path, the channel PLL is configured as a clock data recovery (CDR) PLL and recovers the clock from the serial data stream\cite{altera:cyclonev2} (fig. 22). On the transmit path, the channel PLL is configured as a clock multiplier (CMU) PLL, which generates the transmitter serial and parallel clocks. A 156.25 MHz PLL reference clock is supplied to the XAUI PHY. The XAUI PHY outputs another 156.25 MHz clock that is used for its client-side receive and transmit interfaces, as well as for all interfaces on the XGE MAC , FIFOs, DMAs, and bridges.\\\\
\begin{center}
\includegraphics[scale=0.30]{images/xaui_diagram.png}\\
\small
Figure 21 - XAUI PHY diagram.\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.40]{images/xaui_clocking.png}\\
\small
Figure 22 - XAUI PHY clocking.\\
\normalsize
\end{center}
On the client-side, the XAUI PHY has a transmit and receive XGMII interfaces (fig. 23). The XGMII specification requires that each of the four XAUI lanes transfer 8 data bits accompanied by 1 control bit (used to determine if the 8 data bits are valid) at both the positive and negative edge (DDR) of each clock cycle, but the XAUI PHY is unable to support DDR\cite{altera:cyclonev2}. Instead, each of the four XAUI lanes transfer 15 bits of data and 2 control bits only at the positive edge (SDR) of each clock cycle. Transferring 4 x 15  bits of data at 156.25 Mhz gives a throughput of 10 Gbps. The XAUI PHY treats the datapath as two 32-bit buses and interleaves them\cite{altera:xcvr}. Because of the interleaving, the start of control character (0xFB) is aligned to either byte 0 or byte 5. The timing diagram for byte 0 alignment is shown in figure 20 and the timing diagram for byte 5 alignment is shown in figure 24. The XAUI PHYY XGMII interface is an implementation of the Avalon-ST interface that lacks valid and ready signals, so data is transmitted/received on every rising clock edge.\\\\
\begin{center}
\includegraphics[scale=0.35]{images/xgmii_timing_byte0.png}\\
\small
Figure 23 - XGMII timing diagram (aligned to byte 0).\\
\normalsize
\end{center}
\begin{center}
\includegraphics[scale=0.35]{images/xgmii_timing_byte5.png}\\
\small
Figure 24 - XGMII timing diagram (aligned to byte 5).\\
\normalsize
\end{center}
\newpage
\subsection{XGE MAC}
Because it was convenient to describe the design and timing of the XGE MAC at the same time, the timing details of the XGE MAC were discussed in section 2.1.5 (Design - Architectural Design - XGE MAC).
\subsection{FIFOs}
The receive and transmit Single Clock FIFOs have 64-bit Avalon Streaming (Avalon-ST) interfaces with packet support on their network and client-sides. The input clock for the FIFOs is a 156.25 MHz clock output from the XAUI PHY. 64 bits at 156.25 Mhz gives a throughput of 10 Gbps. The Avalon-ST interface has the notion of source and sink - the source outputs data and the sink receives data\cite{altera:avalon} (fig. 25). The ready signal is asserted by the sink to indicate to the source that it is ready to receive data. The valid signal is asserted by the source to indicate to the sink that it has valid data on its interface. The start-of-packet (SOP) signal marks the active cycle containing the start of the packet. The end-of-packet (EOP) signal marks the active cycle containing the end of the packet. The empty signal indicates the number of symbols that are empty in the last packet data; it is asserted in the same active cycle as EOP.\\\\
\begin{center}
\includegraphics[scale=0.35]{images/avalonst_packet_interface.png}\\
\small
Figure 25 - Avalon-ST interface.\\
\normalsize
\end{center}
The timing diagram for transferring a 16-byte packet is shown in figure 26.\\
\begin{center}
\includegraphics[scale=0.50]{images/avalonst_timing_diagram.png}\\
\small
Figure 26 - Avalon-ST timing diagram.\\
\normalsize
\end{center}
\begin{enumerate}
\item  Data transfer occurs on cycles 1, 2, 4, 5, and 6, when the ready and valid signals are both asserted.
\item During cycle 1, SOP is asserted and the first 4 bytes of the packet are transferred.
\item  During cycle 6, EOP is asserted and the empty signal has a value of 3 (this indicates that this is the end of the packet and that 3 out of the 4 symbols are empty).\\
\end{enumerate}
\subsection{DMAs}
The receive mSGDMA has a 64-bit Avalon Streaming (Avalon-ST) sink interface with packet support on its network-side and a 64-bit Avalon Memory-Mapped (Avalon-MM) master write interface on its client-side. The input clock for both interfaces is a 156.25 MHz clock output from the XAUI PHY. 64 bits at 156.25 Mhz gives a throughput of 10 Gbps. Avalon-MM is used to implement address-based read and write interfaces for master and slave components and has burst support\cite{altera:avalon} (fig. 27).\\\\
\begin{center}
\includegraphics[scale=0.35]{images/avalonmm_interface.png}\\
\small
Figure 27 - Avalon-MM interface.\\
\normalsize
\end{center}
The Avalon-MM master write interface supports burst transfers. A burst executes multiple word transfers as a unit, rather than treating every word independently. Bursts help increase the throughput of this system because the SDRAM controller, which is the Avalon-MM slave for the receive mSGDMA, achieves greater efficiency when handling multiple words at a time. The timing diagram for a write burst transfer is shown in figure 28.\\\\
\begin{center}
\includegraphics[scale=0.35]{images/avalonmm_burst_write.png}\\
\small
Figure 28 - Avalon-MM write burst transfer.\\
\normalsize
\end{center}
\begin{enumerate}
\item  The master asserts the address, burstcount and write signals, and drives the first unit of writedata.
\item The slave asserts the waitrequest singal, indicating that it is not ready to proceed with the transfer.
\item The slave has deasserted the waitrequest signal, indicating that it is ready to proceed with the transfer. The slave captures addr1, burstcount, and the first unit of writedata.
\item  The slave captures the second unit of data.
\item The burst is paused while the write signal is deasserted.
\item The slave captures the third unit of data.
\item The slave asserts the waitrequest signal. In response, all outputs are held constant through another clock cycle.
\item The slave captures the last unit of data. The slave write burst ends.\\
\end{enumerate}
The transmit mSGDMA has a 64-bit Avalon-MM master read interface on its client-side and a 64-bit Avalon-ST source interface with packet support on its network-side. Like the receive mSGDMA, the input clock for both interfaces is a 156.25 MHz clock output from the XAUI PHY. 64 bits at 156.25 Mhz gives a throughput of 10 Gbps. Like the Avalon-MM read master, the write master supports burst transfers. The timing diagram for a read burst transfer is shown in figure 29.\\\\
\begin{center}
\includegraphics[scale=0.40]{images/avalonmm_burst_read.png}\\
\small
Figure 29 - Avalon-MM read burst transfer.\\
\normalsize
\end{center}
\begin{enumerate}
\item Master A asserts address (A0), burstcount, and read signals after the rising edge of clk. The slave asserts
the waitrequest signal, causing all inputs except beginbursttransfer signal to be held constant through the next clock cycle.
\item The slave captures address (A0) and burstcount signals at this rising edge of clk. A new transfer may start on the next cycle.
\item Master B drives address (A1), burstcount, and read signals. The slave asserts waitrequest, causing all
inputs except beginbursttransfer signal to be held constant. At the earliest, the slave could have returned read data from the first read request at this time.
\item The slave presents valid readdata and asserts the readdatavalid signal, transferring the first word of data for master A.
\item The second word for master A is transferred. The slave deasserts the readdatavalid signal pausing the read burst. The slave port can keep the readdatavalid signal deasserted for an arbitrary number of clock cycles.
\item The first word for master B is returned.
\end{enumerate}