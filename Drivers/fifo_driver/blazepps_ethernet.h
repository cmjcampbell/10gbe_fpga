/* BlazePPS Ethernet Driver
 * Copyright (C) 2015 Valeh Valiollahpour Amiri, Christopher Campbell, Sheng Qian.
 *
 * Contributors:
 *	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 *	Christopher Campbell (cc3769@columbia.edu)
 *	Sheng Qian (sq2168@columbia.edu)
 *
 * References:
 *  	snull.c
 * 	altera_tse_main.c
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Last Modified: Math 12th, 2015
 */

#ifndef _BLAZEPPS_ETHERNET_H
#define _BLAZEPPS_ETHERNET_H

/* Default packet pool size (can also be defined at module load time) */
#define BLAZE_POOL_SIZE 8

/* Default timeout period (can also be defined at module load time) */
#define BLAZE_TIMEOUT 5

#endif /* _BLAZEPPS_ETHERNET_H_ */
