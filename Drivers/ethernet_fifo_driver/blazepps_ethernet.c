/* BlazePPS Ethernet Driver
 * Copyright (C) 2015 Valeh Valiollahpour Amiri, Christopher Campbell, Sheng Qian.
 * 
 * Contributors:
 * 	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 * 	Christopher Campbell (cc3769@columbia.edu)
 * 	Sheng Qian (sq2168@columbia.edu)
 *
 * Referenes:
 * 	snull.c
 * 	altera_tse_main.c
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Last Modified: Math 12th, 2015
 */

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/platform_device.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_net.h>
#include <linux/skbuff.h>
#include <linux/byteorder/generic.h>
#include <asm/checksum.h>
#include "blazepps_ethernet.h"

#define SOP 0x00000001
#define EOP 0x00000002
#define CSR_OFF 0x4
#define BLAZE_RX_INTR 0x0001
#define BLAZE_TX_INTR 0x0002
#define DRIVER_NAME "blazepps_ethernet"

#define TXRX

#ifdef TXRX
#define PTXRX(fmt, args...) printk( KERN_NOTICE fmt, ## args)
#else
#define PTXRX(fmt, args...)
#endif

#define DEBUG

#ifdef DEBUG
#define PDEBUG(fmt, args...) printk( KERN_DEBUG "blazepps_ethernet: " fmt, ## args)
#else
#define PDEBUG(fmt, args...)
#endif

/* Module Parameters (exposed in sysfs) */
static int pool_size = BLAZE_POOL_SIZE;
module_param(pool_size, int, S_IRUGO);

static int timeout = BLAZE_TIMEOUT;
module_param(timeout, int, S_IRUGO);

/* Hardware Tx FIFO */
struct hw_tx_fifo {
	u64 write_interface;
};

/* Hardware Rx FIFO */
struct hw_rx_fifo {
	u64 read_interface;
};

/* In-flight packet */
struct blaze_packet {
	u8 data[ETH_DATA_LEN];
	int datalen;
	struct blaze_packet *next;
	struct net_device *dev;
	};

/* Private data */
struct blaze_priv {
	int status;
	int tx_packetlen;
	int en_rx_interrupt;
	struct device *device;
	struct net_device *dev;
	struct sk_buff *skb;
	struct blaze_packet *pkt_pool;
	struct blaze_packet *rx_queue;
	struct net_device_stats stats;
	struct hw_tx_fifo __iomem *tx_dev;
	struct hw_rx_fifo __iomem *rx_dev;
	spinlock_t lock;
};

/* Setup packet pool */
void setup_pool(struct net_device *dev)
{
	int i;
	struct blaze_packet *pkt;
	struct blaze_priv *priv = netdev_priv(dev);

	priv->pkt_pool = NULL;
	for (i = 0; i < pool_size; i++) {
		pkt = kmalloc(sizeof(struct blaze_packet), GFP_KERNEL);
		if (!pkt) {
			PDEBUG("ran out of memory for packet pool\n");
			return;
		}

		pkt->dev = dev;
		pkt->next = priv->pkt_pool;
		priv->pkt_pool = pkt;
	}
}

/* Teardown packet pool */
void teardown_pool(struct net_device *dev)
{
	struct blaze_packet *pkt;
	struct blaze_priv *priv = netdev_priv(dev);

	pkt = priv->pkt_pool;
	while (pkt) {
		priv->pkt_pool = pkt->next;
		kfree(pkt);
		pkt = priv->pkt_pool;
	}
}

/* Get packet from packet pool */
struct blaze_packet *get_pool_pkt(struct net_device *dev)
{
	unsigned long flags;
	struct blaze_packet *pkt;
	struct blaze_priv *priv = netdev_priv(dev);

	spin_lock_irqsave(&priv->lock, flags);
	pkt = priv->pkt_pool;
	priv->pkt_pool = pkt->next;
	if (!priv->pkt_pool) {
		PDEBUG("packet pool empty\n");
		netif_stop_queue(dev);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	return pkt;
}

/* Return packet to packet pool */
void return_pool_pkt(struct blaze_packet *pkt)
{
	unsigned long flags;
	struct blaze_priv *priv = netdev_priv(pkt->dev);

	spin_lock_irqsave(&priv->lock, flags);
	pkt->next = priv->pkt_pool;
	priv->pkt_pool = pkt;
	spin_unlock_irqrestore(&priv->lock, flags);
	if (netif_queue_stopped(pkt->dev) && !pkt->next)
		netif_wake_queue(pkt->dev);
}

/* Enqueue rxed packet */
void enqueue_rx(struct net_device *dev, struct blaze_packet *pkt)
{
	unsigned long flags;
	struct blaze_packet *rx_queue_pkt;
	struct blaze_priv *priv = netdev_priv(dev);

	pkt->next = NULL;
	spin_lock_irqsave(&priv->lock, flags);
	if (!priv->rx_queue) {
		priv->rx_queue = pkt;
	} else {
		rx_queue_pkt = priv->rx_queue;
		while (rx_queue_pkt->next) {
			rx_queue_pkt = rx_queue_pkt->next;
		}
		rx_queue_pkt->next = pkt;
	}
	spin_unlock_irqrestore(&priv->lock, flags);
}

/* Enable/disable rx interrupts */
static void rx_ints(struct net_device *dev, int en)
{
	struct blaze_priv *priv = netdev_priv(dev);

	priv->en_rx_interrupt = en;
}

/* Open device (called by kernel) */
int blaze_open(struct net_device *dev)
{
	netif_start_queue(dev);

	return 0;
}

/* Stop tx queue (called by kernel) */
int blaze_release(struct net_device *dev)
{
	netif_stop_queue(dev);

	return 0;
}

/* Receive a packet */
void passup_rx(struct net_device *dev, struct blaze_packet *pkt)
{
	struct sk_buff *skb;
	struct blaze_priv *priv = netdev_priv(dev);

	skb = dev_alloc_skb(pkt->datalen + 2);
	if (!skb) {
		PDEBUG("low memory, packet dropped\n");
		priv->stats.rx_dropped++;
		goto out;
	}
	skb_reserve(skb, 2);
	memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);

	/* Write metadata and pass to upper level */
	skb->dev = dev;
	skb->protocol = eth_type_trans(skb, dev);
	skb->ip_summed = CHECKSUM_UNNECESSARY;
	priv->stats.rx_packets++;
	priv->stats.rx_bytes += pkt->datalen;
	netif_rx(skb);
 out:
	return;
}

/* Receive packet from hardware */
void hw_rx(struct net_device *dev)
{
	int i;
        u32 data[256];
        int num_bytes;
	struct blaze_packet *pkt = NULL;
        struct blaze_priv *priv = netdev_priv(dev);

        PTXRX("\n");

	i = 0;
        num_bytes = 0;
        while (i < 15) {	
                num_bytes += 4;
               	data[4 * i] = ioread32(&priv->rx_dev->read_interface);
                PTXRX("rx: 0x%08x\n", data[4 * i]);
		i++;
        }

	/* Create packet */
	pkt = get_pool_pkt(dev);
	pkt->datalen = num_bytes;
	memcpy(pkt->data, data, num_bytes);

	/* Enqueue packet */
	enqueue_rx(dev, pkt);
}

/* Typical interrupt handler */
static void blaze_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	int statusword;
	struct blaze_priv *priv;
	struct blaze_packet *pkt = NULL;
	struct net_device *dev = (struct net_device *)dev_id;

	if (!dev)
		return;

	/* Receive packet from hardware and put it in rx queue */
	priv = netdev_priv(dev);
	statusword = priv->status;
	if (statusword & BLAZE_RX_INTR) {
		hw_rx(dev);
	}

	spin_lock(&priv->lock);
	priv->status = 0;
	/* Get packet from queue and pass it to upper layers */
	if (statusword & BLAZE_RX_INTR) {
		pkt = priv->rx_queue;
		if (pkt) {
			/* Dequeue packet */
			priv->rx_queue = pkt->next;

			/* Pass to upper layers */
			passup_rx(dev, pkt);
		}
	}
	/* Tx successful */
	if (statusword & BLAZE_TX_INTR) {
		priv->stats.tx_packets++;
		priv->stats.tx_bytes += priv->tx_packetlen;
		dev_kfree_skb(priv->skb);
	}
	spin_unlock(&priv->lock);

	if (statusword & BLAZE_RX_INTR)
		if (pkt)
			return_pool_pkt(pkt);
}

/* Transmit a packet through hardware */
static void hw_tx(char *buf, int len, struct net_device *dev)
{
	int i;
	int tmp_len;
	int remainder_bytes;
	u32 buf32, empty_bytes, eop_empty;
	struct blaze_priv *priv;

	PTXRX("\n");

	if (len < sizeof(struct ethhdr) + sizeof(struct iphdr)) {
		PDEBUG("packet too short\n");
		return;
	}

	priv = netdev_priv(dev);

	printk("Packet Length: %d\n", len);
	remainder_bytes = len % 4;
	printk("Remainder Bytes: %d\n", remainder_bytes);

	/* Get remainder/empty bytes */
	if (remainder_bytes) {
		tmp_len = len;
		empty_bytes = 4 - remainder_bytes;
	} else {
		tmp_len = len - 4;
		empty_bytes = 0;
	}

	printk("Empty Bytes: %d\n", empty_bytes);
	
	/* SOP */
	iowrite32((u32) SOP, ((char *) &priv->tx_dev->write_interface) + CSR_OFF);

	/* n through n-1 symbols (symbol = 4 bytes) */
	for (i = 0; i < (tmp_len / 4); i++) {
		memcpy(&buf32, &buf[4 * i], sizeof(u32));
		iowrite32(buf32, &priv->tx_dev->write_interface);
		PTXRX("tx: 0x%08x\n", buf32);
	}

	/* EOP and empty bytes */
	eop_empty = (u32) EOP | (empty_bytes << 2);
	printk("eop + empty: %d\n", eop_empty);
	iowrite32(eop_empty, ((char *) &priv->tx_dev->write_interface) + CSR_OFF);
	
	/* nth symbol */
	memcpy(&buf32, &buf[4 * i], sizeof(u32));
	iowrite32(buf32, &priv->tx_dev->write_interface);
	PTXRX("tx: 0x%08x\n", buf32);

	/* Simulate rx interrupt */
	if (priv->en_rx_interrupt) {
		priv->status |= BLAZE_RX_INTR;
		blaze_interrupt(0, dev, NULL);
	}

	/* Simulate tx interrupt */
	priv->status |= BLAZE_TX_INTR;
	blaze_interrupt(0, dev, NULL);
}

/* Transmit a packet (called by the kernel) */
int blaze_tx(struct sk_buff *skb, struct net_device *dev)
{
	int len;
	char *data, shortpkt[ETH_ZLEN];
	struct blaze_priv *priv = netdev_priv(dev);
	
	data = skb->data;
	len = skb->len;
	if (len < ETH_ZLEN) {
	memset(shortpkt, 0, ETH_ZLEN);
		memcpy(shortpkt, skb->data, skb->len);
		len = ETH_ZLEN;
		data = shortpkt;
	}

	dev->trans_start = jiffies;
	priv->skb = skb;

	/* Transmit data through hardware */
	hw_tx(data, len, dev);

	return 0;
}

/* Handle tx timeout (called by kernel) */
void blaze_tx_timeout(struct net_device *dev)
{
	struct blaze_priv *priv = netdev_priv(dev);

	PDEBUG("Transmit timeout at %ld, latency %ld\n", jiffies,
	       jiffies - dev->trans_start);

	/* Simulate a transmission interrupt so we don't have to wait */
	priv->status = BLAZE_TX_INTR;
	blaze_interrupt(0, dev, NULL);
	priv->stats.tx_errors++;
	netif_wake_queue(dev);
}

/* Return stats (called by kernel) */
struct net_device_stats *blaze_stats(struct net_device *dev)
{
	struct blaze_priv *priv = netdev_priv(dev);

	return &priv->stats;
}

static const struct net_device_ops blaze_netdev_ops = {
	.ndo_open = blaze_open,
	.ndo_stop = blaze_release,
	.ndo_start_xmit = blaze_tx,
	.ndo_tx_timeout = blaze_tx_timeout,
	.ndo_set_mac_address = eth_mac_addr,
	.ndo_get_stats = blaze_stats,
};

/* Gets device resources */
static int get_resource(struct platform_device *pdev, const char *name,
			struct resource **res, void __iomem **ptr)
{
	struct resource *region;
	struct device *device = &pdev->dev;

	*res = platform_get_resource_byname(pdev, IORESOURCE_MEM, name);
	printk("resource size: %d\n", resource_size(*res));
	if (*res == NULL) {
		dev_err(device, "resource %s not defined\n", name);
		return -ENODEV;
	}

	region =
	    devm_request_mem_region(device, (*res)->start, resource_size(*res),
				    dev_name(device));
	printk("region starts: %llu\n", (u64) region->start);
	printk("region ends: %llu\n", (u64) region->end);
	if (region == NULL) {
		dev_err(device, "unable to request %s\n", name);
		return -EBUSY;
	}

	*ptr =
	    devm_ioremap_nocache(device, region->start, resource_size(region));
	if (*ptr == NULL) {
		dev_err(device, "ioremap_nocache of %s failed!", name);
		return -ENOMEM;
	}

	return 0;
}

/* Probes for device */
int blazepps_ethernet_probe(struct platform_device *pdev)
{
	int result;
	int ret = -ENODEV;
	const unsigned char *macaddr;
	struct net_device *ndev;
	struct blaze_priv *priv;
	struct resource *tx_fifo, *rx_fifo;

	/* Allocate network device */
	ndev = alloc_etherdev(sizeof(struct blaze_priv));
	if (!ndev) {
		PDEBUG("unabled to allocate network device\n");
		return ret;
	}

	/* Set device custom options */
	ndev->watchdog_timeo = timeout;
	ndev->netdev_ops = &blaze_netdev_ops;
	//ndev->flags |= IFF_NOARP;
	ndev->features |= NETIF_F_HW_CSUM;
	priv = netdev_priv(ndev);

	/* Initialize network device */
	macaddr = of_get_mac_address(pdev->dev.of_node);
	if (macaddr)
		memcpy(ndev->dev_addr, macaddr, ETH_ALEN);
	else
		eth_hw_addr_random(ndev);
	memset(priv, 0, sizeof(struct blaze_priv));
	spin_lock_init(&priv->lock);
	rx_ints(ndev, 1);
	setup_pool(ndev);
	SET_NETDEV_DEV(ndev, &pdev->dev);
	priv->device = &pdev->dev;
	priv->dev = ndev;

	/* Get device resources */
	ret = get_resource(pdev, "tx_fifo", &tx_fifo,
			(void __iomem **)&priv->tx_dev);
	if (ret) {
		PDEBUG("unable to get tx_fifo resource\n");
		goto out;
	}
	ret = get_resource(pdev, "rx_fifo", &rx_fifo,
			(void __iomem **)&priv->rx_dev);
	if (ret) {
		PDEBUG("unable to get rx_fifo resource\n");
		goto out;
	}

	/* Register device */
	ret = -ENODEV;
	result = register_netdev(ndev);
	if (result) {
		PDEBUG("unable to register network device\n");
		goto out;
	} else {
		ret = 0;
	}

	platform_set_drvdata(pdev, ndev);

 out:
	if (ret) {
		unregister_netdev(ndev);
		teardown_pool(ndev);
		free_netdev(ndev);
	}

	return ret;
}

/* Remove device and resources */
static int blazepps_ethernet_remove(struct platform_device *pdev)
{
	struct net_device *ndev = platform_get_drvdata(pdev);

	platform_set_drvdata(pdev, NULL);
	unregister_netdev(ndev);
	teardown_pool(ndev);
	free_netdev(ndev);

	return 0;
}

#ifdef CONFIG_OF
static const struct of_device_id blazepps_ethernet_of_match[] = {
	{.compatible = "altr,blazepps_ethernet"},
	{},
};

MODULE_DEVICE_TABLE(of, blazepps_ethernet_of_match);
#endif

static struct platform_driver blazepps_ethernet_driver = {
	.probe = blazepps_ethernet_probe,
	.remove = blazepps_ethernet_remove,
	.suspend = NULL,
	.resume = NULL,
	.driver = {
		   .name = DRIVER_NAME,
		   .of_match_table = of_match_ptr(blazepps_ethernet_of_match),},
};

module_platform_driver(blazepps_ethernet_driver);

MODULE_DESCRIPTION("BlazePPS Ethernet Driver");
MODULE_AUTHOR("BlazePPS Corporation");
MODULE_LICENSE("GPL v2");
