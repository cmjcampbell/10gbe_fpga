/* BlazePPS Software Loopback Driver
 *
 * Authors: 
 *	Valeh Valiollahpour Amiri (vv2252@columbia.edu)
 *	Christopher Campbell (cc3769@columbia.edu)
 *
 * Last Modified:
 *	April 23rd, 2015
 */

/* Network and Host Address Scheme
 *
 * Networks:
 *     blazenet0    192.168.2.0
 *     blazenet1    192.168.3.0
 *
 * Hosts:
 *     local0       192.168.2.1
 *     remote0      192.168.2.2
 *     local1       192.168.3.2
 *     remote1      192.168.3.1
 *
 * The network addresses have been added to /etc/networks and the host
 * addresses have been added to /etc/hosts so all of these addresses can
 * be referrenced by name
 *
 * Once the driver is running, you should see the bz0 and bz1 interfaces
 * using the command 'ifconfig'.
 * 
 * Assigning addresses to the interfaces (bz0 and bz1):
 *     $ ifconfig bz0 local0
 *     $ ifconfig bz1 local1
 *
 * Run load.sh script which loads the module and then runs the two commandes above.
 *
 * Sending packets from bz0 to bz1:
 *     $ ping -c 2 remote0
 *     
 * Sending packets from bz1 to bz0:
 *     $ ping -c 2 remote1
 */

/* References
 * Network drivers
 *   http://www.oreilly.com/openbook/linuxdrive3/book/ch17.pdf
 *   http://www.cubrid.org/blog/dev-platform/
		understanding-tcp-ip-network-stack/
 * blaze.c
 *   http://www.xml.com/ldd/chapter/book/ch14.html
 *
 * loopback.c
 *   http://lxr.free-electrons.com/source/drivers/net/loopback.c?v=3.8
 *
 * NAPI:
 *   http://en.wikipedia.org/wiki/New_API#Compliant_drivers
 *   http://www.linuxfoundation.org/collaborate/workgroups/networking/napi
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/version.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/interrupt.h>

#include <linux/in.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/skbuff.h>
#include <linux/in6.h>
#include <asm/checksum.h>
#include <linux/of.h>
#include <linux/of_net.h>

#include "blazepps_loopback.h"

MODULE_DESCRIPTION("BlazePPS Ethernet Driver");
MODULE_AUTHOR("Valeh Valiollahpour Amiri, Christopher Campbell");
MODULE_LICENSE("GPL");

/* static int use_napi = 0;
module_param(use_napi, int, S_IRUGO);

int pool_size = 8;
module_param(pool_size, int, S_IRUGO); 

static int lockup = 0;
module_param(lockup, int, S_IRUGO); 

static int timeout = BLAZE_TIMEOUT;
module_param(timeout, int, S_IRUGO); */

struct net_device *blaze_devs[NUM_DEVS];

/* In-flight packet */
struct blaze_packet {
	struct blaze_packet *next;
	struct net_device *dev;
	int datalen;
	/* ETH_DATA_LEN := Max octets in payload (1500 bytes)*/
	u8 data[ETH_DATA_LEN];
};

/* Private device data */
struct blaze_priv {
	struct net_device_stats stats;	/* device stats */
	int status;			/* device status (soft emu) */
	struct blaze_packet *ppool;	/* packet pool */ 
	struct blaze_packet *rx_queue;  /* incoming packets */
	int rx_int_enabled;		/* receive enabled */
	struct sk_buff *skb;		/* socket buff for snt/recv pckt */
	int tx_packetlen;		/* transmission packet length */
	u8 *tx_packetdata;		/* transmission packet data */
	spinlock_t lock;		/* blaze_priv lock */
	struct net_device *dev;		/* net device */
	struct napi_struct napi;	/* used for NAPI compliance */
};

static void blaze_tx_timeout(struct net_device *dev);
static void (*blaze_interrupt)(int, void *, struct pt_regs *);


/* Set up device packet pool */
void blaze_setup_pool(struct net_device *dev)
{
	int i;
	struct blaze_packet *pkt;
	struct blaze_priv *priv = netdev_priv(dev);

	priv->ppool = NULL;
	for (i = 0; i < POOL_SZ; i++) {
		pkt = kmalloc (sizeof (struct blaze_packet), GFP_KERNEL);
		if (pkt == NULL) {
			printk(KERN_NOTICE "Ran out of memory" \
					"allocating packet pool\n");
			return;
		}

		pkt->dev = dev;
		pkt->next = priv->ppool;
		priv->ppool = pkt;
	}
}

/* Teardown device pool */
void blaze_teardown_pool(struct net_device *dev)
{
	struct blaze_packet *pkt;
	struct blaze_priv *priv = netdev_priv(dev);
    
	while ((pkt = priv->ppool)) {
		priv->ppool = pkt->next;
		kfree(pkt);
		/* FIXME - in-flight packets ? */
	}
}

/* Get buffer from the head of the packet pool to use for xmission. */
struct blaze_packet *blaze_get_tx_buffer(struct net_device *dev)
{
	unsigned long flags;
	struct blaze_packet *pkt;

	struct blaze_priv *priv = netdev_priv(dev);
    
	spin_lock_irqsave(&priv->lock, flags);
	pkt = priv->ppool;
	priv->ppool = pkt->next;
	if (priv->ppool == NULL) {
		printk (KERN_INFO "Pool empty\n");
		netif_stop_queue(dev);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	return pkt;
}

/* Append a new packet at the head of the packet pool. */
void blaze_release_buffer(struct blaze_packet *pkt)
{
	unsigned long flags;
	struct blaze_priv *priv = netdev_priv(pkt->dev);
        
	spin_lock_irqsave(&priv->lock, flags);
	pkt->next = priv->ppool;
	priv->ppool = pkt;
	spin_unlock_irqrestore(&priv->lock, flags);
	if (netif_queue_stopped(pkt->dev) && pkt->next == NULL)
		netif_wake_queue(pkt->dev);
}

/* Enqueue a received packet to the queue of received packets. */
void blaze_enqueue_buf(struct net_device *dev, struct blaze_packet *pkt)
{
	unsigned long flags;
	struct blaze_priv *priv = netdev_priv(dev);

	spin_lock_irqsave(&priv->lock, flags);
	pkt->next = priv->rx_queue;  /* FIXME - misorders packets */
	priv->rx_queue = pkt;
	spin_unlock_irqrestore(&priv->lock, flags);
}

/* Dequeue a packet from the queue of received packets. */
struct blaze_packet *blaze_dequeue_buf(struct net_device *dev)
{
	struct blaze_priv *priv = netdev_priv(dev);
	struct blaze_packet *pkt;
	unsigned long flags;

	spin_lock_irqsave(&priv->lock, flags);
	pkt = priv->rx_queue;
	if (pkt != NULL)
		priv->rx_queue = pkt->next;
	spin_unlock_irqrestore(&priv->lock, flags);
	return pkt;
}

/* Enable and disable receive interrupts */
static void blaze_rx_ints(struct net_device *dev, int enable)
{
	struct blaze_priv *priv = netdev_priv(dev);
	priv->rx_int_enabled = enable;	//.ndo_change_mtu      = blaze_change_mtu,

}

/* Open device */
int blaze_open(struct net_device *dev)
{
	const unsigned char *macaddr;

	macaddr = of_get_mac_address(pdev->dev.of_node);
	if (macaddr)
		ether_addr_copy(ndev->dev_addr, macaddr);
	else
		eth_hw_addr_random(ndev);

	netif_start_queue(dev);
	return 0;
}  

/* Stop transmit queue */
int blaze_release(struct net_device *dev)
{
	netif_stop_queue(dev); /* can't transmit anymore */
	
	/* may need to free skbufs here */	
	
	return 0;
}

/* Configuration via ifconfig */
int blaze_config(struct net_device *dev, struct ifmap *map)
{
	if (dev->flags & IFF_UP) /* can't act on a running interface */
		return -EBUSY;

	/* Don't allow changing the I/O address */
	if (map->base_addr != dev->base_addr) {
		printk(KERN_WARNING "blaze: Can't change I/O address\n");
		return -EOPNOTSUPP;
	}

	/* Allow changing the IRQ */
	if (map->irq != dev->irq) {
		dev->irq = map->irq;
        	/* request_irq() is delayed to open-time */
	}

	/* Ignore other fields */
	return 0;
}

/* Receive a packet (retrieve, encapsulate and pass over to upper levels) */
void blaze_rx(struct net_device *dev, struct blaze_packet *pkt)
{
	struct sk_buff *skb;
	struct blaze_priv *priv = netdev_priv(dev);

	/*
	 * Packet received from transmission medium
         * (build an skb around it, so upper layers can handle it)
	 */
	skb = dev_alloc_skb(pkt->datalen + 2);
	if (!skb) {
		if (printk_ratelimit())
			printk(KERN_NOTICE "blaze rx: low on mem - " \
							"packet dropped\n");
		priv->stats.rx_dropped++;
		goto out;
	}
	skb_reserve(skb, 2); /* align IP on 16B boundary */  
	memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);

	/* Write metadata, and then pass to the receive level */
	skb->dev = dev; 
	skb->protocol = eth_type_trans(skb, dev);
	skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
	priv->stats.rx_packets++;
	priv->stats.rx_bytes += pkt->datalen;
	netif_rx(skb);
out:
	return;
}

/* Polling function */
static int blaze_poll(struct napi_struct *napi, int budget)
{
	int npackets = 0;
	struct sk_buff *skb;
	struct blaze_priv *priv = container_of(napi, struct blaze_priv,
								napi);
	struct net_device *dev = priv->dev;
	struct blaze_packet *pkt;
    
	while (npackets < budget && priv->rx_queue) {
		pkt = blaze_dequeue_buf(dev);
		skb = dev_alloc_skb(pkt->datalen + 2);
		if (! skb) {
			if (printk_ratelimit())
				printk(KERN_NOTICE "blaze: packet " \
							"dropped\n");
			priv->stats.rx_dropped++;
			blaze_release_buffer(pkt);
			continue;
		}
		skb_reserve(skb, 2); /* align IP on 16B boundary */  
		memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);
		skb->dev = dev;
		skb->protocol = eth_type_trans(skb, dev);
		skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
		netif_receive_skb(skb);
		
        	/* Maintain stats */
		npackets++;
		priv->stats.rx_packets++;
		priv->stats.rx_bytes += pkt->datalen;
		blaze_release_buffer(pkt);
	}
	/* If we processed all packets, tell the kernel and reenable ints */
	if (! priv->rx_queue) {
		napi_complete(napi);
		blaze_rx_ints(dev, 1);
		return 0;
	}

	/* We couldn't process everything. */
	return npackets;
}

/* Typical interrupt handler */
static void blaze_regular_interrupt(int irq, void *dev_id,
				    struct pt_regs *regs)
{
	int statusword;
	struct blaze_priv *priv;
	struct blaze_packet *pkt = NULL;

	/*
	 * As usual, check the "device" pointer to be sure it is
	 * really interrupting.
	 * Then assign "struct device *dev"
	 */
	struct net_device *dev = (struct net_device *)dev_id;

	/* Paranoid */
	if (!dev)
		return;

	/* Lock the device */
	priv = netdev_priv(dev);
	spin_lock(&priv->lock);

	/* Retrieve statusword: real netdevices use I/O instructions */
	statusword = priv->status;
	priv->status = 0;
	/* rx interrupt */
	if (statusword & BLAZE_RX_INTR) {
		/* A new packet has been added and enqueued tio the receive queue. Take it and send it to blaze_rx for handling */
		pkt = priv->rx_queue;
		if (pkt) {
			priv->rx_queue = pkt->next;
			blaze_rx(dev, pkt);
		}
	}
	/* tx interrupt */
	if (statusword & BLAZE_TX_INTR) {
		/* a transmission is over: update the tx stats and free the associated skb */
		priv->stats.tx_packets++;
		priv->stats.tx_bytes += priv->tx_packetlen;
		dev_kfree_skb(priv->skb);
	}

	/* Unlock the device and we are done */
	spin_unlock(&priv->lock);
	if (pkt) blaze_release_buffer(pkt); /* Do this outside the lock! */
	return;
}

/* NAPI Interrupt handler */
static void blaze_napi_interrupt(int irq, void *dev_id,
				struct pt_regs *regs)
{
	int statusword;
	struct blaze_priv *priv;

	/* Check the "device" pointer for shared handlers */
	struct net_device *dev = (struct net_device *) dev_id;

	/* 
	 * Hardware related code goes here: check with hw if it's
	 * really ours
	 */

	/* Paranoid */
	if (!dev)
		return;

	priv = netdev_priv(dev);
	spin_lock(&priv->lock);

	/* Retrieve statusword */
	/* 
	 * Hardware related code goes here (real netdevices use
	 * I/O instructions) 
	 */
	statusword = priv->status;
	priv->status = 0;
	if (statusword & BLAZE_RX_INTR) {
		blaze_rx_ints(dev, 0);  /* Disable further interrupts */
		napi_schedule(&priv->napi);
	}
	if (statusword & BLAZE_TX_INTR) {
        	/* Transmission is over, free the skb */
		priv->stats.tx_packets++;
		priv->stats.tx_bytes += priv->tx_packetlen;
		dev_kfree_skb(priv->skb);
	}

	spin_unlock(&priv->lock);
	return;
}

/* Transmit a packet (low level interface) */
static void blaze_hw_tx(char *buf, int len, struct net_device *dev)
{	
	/* This function deals with hw details of transmission. */ 
	struct iphdr *ih;
	struct net_device *dest;
	struct blaze_priv *priv;
	u32 *saddr, *daddr;
	struct blaze_packet *tx_buffer;    

	/* Paranoid! */
	if (len < sizeof(struct ethhdr) + sizeof(struct iphdr)) {
		printk("blaze: Hmm... packet too short (%i octets)\n",
				len);
		return;
	}

	/* Enable this conditional to print out the payload data */
	if (0) {
		int i;
		PDEBUG("len is %i\n" KERN_DEBUG "data:",len);
		for (i=0; i<len; i++)
			printk(" %02x",buf[i]);
		printk("\n");
	}

	/*
	 * Ethhdr is 14 bytes, but the kernel arranges for iphdr
	 * to be aligned (i.e., ethhdr is unaligned)
	 */
	ih = (struct iphdr *)(buf+sizeof(struct ethhdr));
	saddr = &ih->saddr;
	daddr = &ih->daddr;

	/* Why??? */
	((u8 *)saddr)[2] ^= 1; /* change the third octet (class C) */
	((u8 *)daddr)[2] ^= 1;

	ih->check = 0;         /* and rebuild the checksum (ip needs it) */
	ih->check = ip_fast_csum((unsigned char *)ih, ih->ihl);

	/* Print out IP addr of source and destination */
	if (dev == blaze_devs[0])
		PDEBUG("%08x:%05i --> %08x:%05i\n",
			ntohl(ih->saddr),ntohs(((struct tcphdr *)(ih+1))->source),
			ntohl(ih->daddr),ntohs(((struct tcphdr *)(ih+1))->dest));
	else
		PDEBUG("%08x:%05i <-- %08x:%05i\n",
			ntohl(ih->daddr),ntohs(((struct tcphdr *)(ih+1))->dest),
			ntohl(ih->saddr),ntohs(((struct tcphdr *)(ih+1))->source));
	
	/*
	 * Ok, now the packet is ready for transmission: first simulate a
	 * receive interrupt on the destination device, then a
	 * transmission-done interrupt on the transmitting device
	 */
	dest = blaze_devs[dev == blaze_devs[0] ? 1 : 0];
	priv = netdev_priv(dest);
		priv = netdev_priv(dest);
	tx_buffer = blaze_get_tx_buffer(dev);
	tx_buffer->datalen = len;
	memcpy(tx_buffer->data, buf, len);
	/* Enqueue received packet on the destination's receive queue */
	blaze_enqueue_buf(dest, tx_buffer);
	if (priv->rx_int_enabled) {
		priv->status |= BLAZE_RX_INTR;
		blaze_interrupt(0, dest, NULL);
	}

	priv = netdev_priv(dev);
	priv->tx_packetlen = len;
	priv->tx_packetdata = buf;
	priv->status |= BLAZE_TX_INTR;
	/* Drop packet if too many stats.tx_packets */
	if (LOCKUP && ((priv->stats.tx_packets + 1) % LOCKUP) == 0) {
		/* Simulate a dropped transmit interrupt */
		netif_stop_queue(dev);
		PDEBUG("Simulate lockup at %ld, txp %ld\n", jiffies,
		       (unsigned long) priv->stats.tx_packets);
	}
	blaze_interrupt(0, dev, NULL);
}

/* Transmit a packet (called by the kernel) */
int blaze_tx(struct sk_buff *skb, struct net_device *dev)
{
	int len;
	/* ETH_ZLEN := Min. octets in frame sans FCS (Frame Checking Sequence) (60 bytes) */
	char *data, shortpkt[ETH_ZLEN];
	struct blaze_priv *priv = netdev_priv(dev);
        
	data = skb->data;
	len = skb->len;
	if (len < ETH_ZLEN) {
		memset(shortpkt, 0, ETH_ZLEN);
		memcpy(shortpkt, skb->data, skb->len);
		len = ETH_ZLEN;
		data = shortpkt;
	}

	dev->trans_start = jiffies; /* save the timestamp */

	/* Remember the skb, so we can free it at interrupt time */
	priv->skb = skb;

	/* Actual delivery of the data (low level xmit) - device-specific */
	blaze_hw_tx(data, len, dev);

	return 0; /* our simple device cannot fail */
}

/* Handle a transmit timeout */
void blaze_tx_timeout(struct net_device *dev)
{
	struct blaze_priv *priv = netdev_priv(dev);

	PDEBUG("Transmit timeout at %ld, latency %ld\n", jiffies,
			jiffies - dev->trans_start);

        /* Simulate a transmission interrupt to get things moving */
	priv->status = BLAZE_TX_INTR;
	blaze_interrupt(0, dev, NULL);
	priv->stats.tx_errors++;
	netif_wake_queue(dev);
	return;
}

/* ioctl commands */
int blaze_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	/* ioctl commands go here */ 
	PDEBUG("ioctl\n");
	return 0;
}

/* Return stats */
struct net_device_stats *blaze_stats(struct net_device *dev)
{
	struct blaze_priv *priv = netdev_priv(dev);
	return &priv->stats;
}

/* 
 * Manually fills up ethernet header because ARP is not used
 * (probably won't be necessary in our actual driver)
 */
int blaze_rebuild_header(struct sk_buff *skb)
{
	struct ethhdr *eth = (struct ethhdr *) skb->data;
	struct net_device *dev = skb->dev;
    
	memcpy(eth->h_source, dev->dev_addr, dev->addr_len);
	memcpy(eth->h_dest, dev->dev_addr, dev->addr_len);
	/* ETH_ALEN := Octets in one ethernet addr (6 bytes) */
	eth->h_dest[ETH_ALEN-1] ^= 0x01;   /* dest is us xor 1 */
	return 0;
}

/* Creates header (probably won't be necessary in our actual driver) */
int blaze_header(struct sk_buff *skb, struct net_device *dev,
		unsigned short type, const void *daddr, const void *saddr,								unsigned len)
{
	struct ethhdr *eth = (struct ethhdr *) skb_push(skb, ETH_HLEN);

	eth->h_proto = htons(type);
	memcpy(eth->h_source, saddr ? saddr : dev->dev_addr, dev->addr_len);
	memcpy(eth->h_dest,   daddr ? daddr : dev->dev_addr, dev->addr_len);
	eth->h_dest[ETH_ALEN-1] ^= 0x01;   /* dest is us xor 1 */
	return (dev->hard_header_len);
}

/*
 * The "change_mtu" method is usually not needed.
 * If you need it, it must be like this.
 */
int blaze_change_mtu(struct net_device *dev, int new_mtu)
{
	unsigned long flags;
	struct blaze_priv *priv = netdev_priv(dev);
	spinlock_t *lock = &priv->lock;
    
	/* check ranges */
	if ((new_mtu < 68) || (new_mtu > 1500))
		return -EINVAL;
	/*
	 * Do anything you need, and the accept the value
	 */
	spin_lock_irqsave(lock, flags);
	dev->mtu = new_mtu;
	spin_unlock_irqrestore(lock, flags);
	return 0; /* success */
}

static const struct header_ops blaze_header_ops = {
        .create  = blaze_header,
	.rebuild = blaze_rebuild_header
};

static const struct net_device_ops blaze_netdev_ops = {
	.ndo_open            = blaze_open,
	.ndo_stop            = blaze_release,
	.ndo_start_xmit      = blaze_tx,
	.ndo_do_ioctl        = blaze_ioctl,
	.ndo_set_config      = blaze_config,
	.ndo_get_stats       = blaze_stats,
	.ndo_change_mtu      = blaze_change_mtu,
	.ndo_tx_timeout      = blaze_tx_timeout
};

/* Init/Probe function (invoked by register_netdev()) */
void blaze_init(struct net_device *dev)
{
	struct blaze_priv *priv;

	/* 
	 * Hardware specific code will go here: check_region(),
	 * probe irq, ... Return -ENODEV if no device is found.
	 * No resource should be grabbed (this is done on open())
	 */

    	/* Setup dev fields */
	ether_setup(dev); /* set some of these manually? */
	dev->watchdog_timeo = BLAZE_TIMEOUT;
	dev->netdev_ops = &blaze_netdev_ops;
	dev->header_ops = &blaze_header_ops;
	dev->flags           |= IFF_NOARP; /* use ARP? */
	dev->features        |= NETIF_F_HW_CSUM;

	/* Intialize priv field */
	priv = netdev_priv(dev);
	if (USE_NAPI) {
		/* weight = 2 (> weight = interface more important) */
		netif_napi_add(dev, &priv->napi, blaze_poll, 2);
	}
	memset(priv, 0, sizeof(struct blaze_priv));

	spin_lock_init(&priv->lock);
	/* Enable receive interrupts */
	blaze_rx_ints(dev, 1);
	blaze_setup_pool(dev);
}

/* Cleanup */
void blaze_cleanup(void)
{
	int i;

	for (i = 0; i < NUM_DEVS; i++) {
		if (blaze_devs[i]) {
			unregister_netdev(blaze_devs[i]);
			blaze_teardown_pool(blaze_devs[i]);
			free_netdev(blaze_devs[i]);
		}
	}

	return;
}

/* Initializes device */
int blaze_init_module(void)
{
	int result, i, ret = -ENOMEM;

	blaze_interrupt = USE_NAPI ? blaze_napi_interrupt : blaze_regular_interrupt;
 
	/* Allocate the devices */
	printk("begin alloc\n");
	blaze_devs[0] = alloc_netdev(sizeof(struct blaze_priv), "bz%d",
				     blaze_init);
	blaze_devs[1] = alloc_netdev(sizeof(struct blaze_priv), "bz%d",
				     blaze_init);
	if (blaze_devs[0] == NULL || blaze_devs[1] == NULL)
		goto out;
	printk("end alloc\n");
 
	ret = -ENODEV;
	for (i = 0; i < 2;  i++)
		if ((result = register_netdev(blaze_devs[i])))
			printk("blaze: error %i registering device \"%s\"\n",
			       result, blaze_devs[i]->name);
		else
			ret = 0;
 out:
	if (ret) 
		blaze_cleanup();
	return ret;
}

module_init(blaze_init_module);
module_exit(blaze_cleanup);
