/* Blaze XGE mSGDMA Driver
 *
 * Contributors:
 *  Christopher Campbell (cc3769@columbia.edu)
 *
 * Original driver contributed by Altera.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __BLAZE_MSGDMA_H__
#define __BLAZE_MSGDMA_H__

void msgdma_reset(struct blaze_xge_private *);
void msgdma_enable_txirq(struct blaze_xge_private *);
void msgdma_enable_rxirq(struct blaze_xge_private *);
void msgdma_disable_rxirq(struct blaze_xge_private *);
void msgdma_disable_txirq(struct blaze_xge_private *);
void msgdma_clear_rxirq(struct blaze_xge_private *);
void msgdma_clear_txirq(struct blaze_xge_private *);
u32 msgdma_tx_completions(struct blaze_xge_private *);
void msgdma_add_rx_desc(struct blaze_xge_private *, struct xge_buffer *);
int msgdma_tx_buffer(struct blaze_xge_private *, struct xge_buffer *);
u32 msgdma_rx_status(struct blaze_xge_private *);
int msgdma_initialize(struct blaze_xge_private *);
void msgdma_uninitialize(struct blaze_xge_private *);
void msgdma_start_rxdma(struct blaze_xge_private *);

#endif /*  __BLAZE_MSGDMA_H__ */

