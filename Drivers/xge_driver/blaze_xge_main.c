/* Blaze 10 Gigabit Ethernet Driver
 *
 * Contributors:
 *  Christopher Campbell (cc3769@columbia.edu)
 *
 * Original driver contributed by Altera.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/atomic.h>
#include <linux/delay.h>
#include <linux/etherdevice.h>
#include <linux/if_vlan.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/of_device.h>
#include <linux/of_mdio.h>
#include <linux/of_net.h>
#include <linux/of_platform.h>
#include <linux/phy.h>
#include <linux/platform_device.h>
#include <linux/skbuff.h>
#include <linux/netdev_features.h>
#include <asm/cacheflush.h>

#include "blaze_utils.h"
#include "blaze_xge_main.h"
#include "blaze_msgdma.h"

#define DEBUG
#ifdef DEBUG
#define PDEBUG(fmt, args...) printk( KERN_DEBUG "blaze_xge: " fmt, ## args)
#else
#define PDEBUG(fmt, args...)
#endif

#define NAPI_POLL_WEIGHT 64
#define BLAZE_RXDMABUFFER_SIZE 2048

static int debug = -1;
module_param(debug, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(debug, "Message Level (-1: default, 0: no output, 16: all)");

static const u32 default_msg_level = (NETIF_MSG_DRV | NETIF_MSG_PROBE |
                                      NETIF_MSG_LINK | NETIF_MSG_IFUP |
                                      NETIF_MSG_IFDOWN);

#define RX_DESCRIPTORS 64
static int dma_rx_num = RX_DESCRIPTORS;
module_param(dma_rx_num, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(dma_rx_num, "Number of descriptors in the RX list");

#define TX_DESCRIPTORS 64
static int dma_tx_num = TX_DESCRIPTORS;
module_param(dma_tx_num, int, S_IRUGO | S_IWUSR);
MODULE_PARM_DESC(dma_tx_num, "Number of descriptors in the TX list");

#define POLL_PHY (-1)
#define BLAZE_RXDMABUFFER_SIZE 2048
#define XGE_TX_THRESH(x)    (x->tx_ring_size / 4)
#define TXQUEUESTOP_THRESHHOLD  2

static const struct of_device_id blaze_xge_ids[];

static void dump_sk_buff(struct sk_buff *skb)
{
    int i;
    unsigned int mac_header_len;
    unsigned int data_len;
    char *mac_header;
    char *data;

    mac_header_len = skb->mac_len;
    mac_header = skb->mac_header;
    printk("mac_header_len: %u\n", mac_header_len);
    printk("mac_header: ");
    for (i = 0; i < mac_header_len; i++) {
        printk("%02x ", (unsigned char) *mac_header++);
    }
    printk("\n");
    printk("\n");

    data_len = skb->len;
    data = skb->data;
    printk("data_len: %u\n", data_len);
    printk("data: ");
    for (i = 0; i < data_len; i++) {
        printk("%02x ", (unsigned char) *data++);
    }
    printk("\n");
    printk("\n");
}



static inline u32 xge_tx_avail(struct blaze_xge_private *priv)
{
    return priv->tx_cons + priv->tx_ring_size - priv->tx_prod - 1;
}

static int xge_init_rx_buffer(struct blaze_xge_private *priv,
                  struct xge_buffer *rxbuffer, int len)
{
    rxbuffer->skb = netdev_alloc_skb_ip_align(priv->dev, len);
    if (!rxbuffer->skb)
        return -ENOMEM;

    rxbuffer->dma_addr = dma_map_single(priv->device, rxbuffer->skb->data,
                        len,
                        DMA_FROM_DEVICE);

    if (dma_mapping_error(priv->device, rxbuffer->dma_addr)) {
        netdev_err(priv->dev, "%s: DMA mapping error\n", __func__);
        dev_kfree_skb_any(rxbuffer->skb);
        return -EINVAL;
    }
    rxbuffer->dma_addr &= (dma_addr_t)~3;
    rxbuffer->len = len;
    return 0;
}

static void xge_free_rx_buffer(struct blaze_xge_private *priv,
                   struct xge_buffer *rxbuffer)
{
    struct sk_buff *skb = rxbuffer->skb;
    dma_addr_t dma_addr = rxbuffer->dma_addr;
    
    if (skb != NULL) {
        if (dma_addr)
            dma_unmap_single(priv->device, dma_addr,
                     rxbuffer->len,
                     DMA_FROM_DEVICE);
        dev_kfree_skb_any(skb);
        rxbuffer->skb = NULL;
        rxbuffer->dma_addr = 0;
    }
}

static void xge_free_tx_buffer(struct blaze_xge_private *priv,
                   struct xge_buffer *buffer)
{
    PDEBUG("*xge_free_tx_buffer*\n");

    if (buffer->dma_addr) {
        if (buffer->mapped_as_page)
            dma_unmap_page(priv->device, buffer->dma_addr,
                       buffer->len, DMA_TO_DEVICE);
        else
            dma_unmap_single(priv->device, buffer->dma_addr,
                     buffer->len, DMA_TO_DEVICE);
        buffer->dma_addr = 0;
    }
    if (buffer->skb) {
        dev_kfree_skb_any(buffer->skb);
        buffer->skb = NULL;
    }
}


static int alloc_init_skbufs(struct blaze_xge_private *priv)
{

    unsigned int rx_descs = priv->rx_ring_size;
    unsigned int tx_descs = priv->tx_ring_size;
    int i;
    int ret = -ENOMEM;
    
    priv->rx_ring = kcalloc(rx_descs, sizeof(struct xge_buffer),
                GFP_KERNEL);
    if (!priv->rx_ring)
        goto err_rx_ring;

    priv->tx_ring = kcalloc(tx_descs, sizeof(struct xge_buffer),
                GFP_KERNEL);
    if (!priv->tx_ring)
        goto err_tx_ring;

    priv->tx_cons = 0;
    priv->tx_prod = 0;

    for (i = 0; i < rx_descs; i++) {
        ret = xge_init_rx_buffer(priv, &priv->rx_ring[i],
                     priv->rx_dma_buf_sz);
        if (ret)
            goto err_init_rx_buffers;
    }

    priv->rx_cons = 0;
    priv->rx_prod = 0;

    return 0;
err_init_rx_buffers:
    while (--i >= 0)
        xge_free_rx_buffer(priv, &priv->rx_ring[i]);
    kfree(priv->tx_ring);
err_tx_ring:
    kfree(priv->rx_ring);
err_rx_ring:
    return ret;
}

static void free_skbufs(struct net_device *dev)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    unsigned int rx_descs = priv->rx_ring_size;
    unsigned int tx_descs = priv->tx_ring_size;
    int i;
    
    for (i = 0; i < rx_descs; i++)
        xge_free_rx_buffer(priv, &priv->rx_ring[i]);
    for (i = 0; i < tx_descs; i++)
        xge_free_tx_buffer(priv, &priv->tx_ring[i]);


    kfree(priv->tx_ring);
}

static inline void xge_rx_refill(struct blaze_xge_private *priv)
{
    unsigned int rxsize = priv->rx_ring_size;
    unsigned int entry;
    int ret;
    
    for (; priv->rx_cons - priv->rx_prod > 0;
            priv->rx_prod++) {
        entry = priv->rx_prod % rxsize;
        if (likely(priv->rx_ring[entry].skb == NULL)) {
            ret = xge_init_rx_buffer(priv, &priv->rx_ring[entry],
                priv->rx_dma_buf_sz);
            if (unlikely(ret != 0))
                break;
            priv->dmaops->add_rx_desc(priv, &priv->rx_ring[entry]);
        }
    }
}

static inline void xge_rx_vlan(struct net_device *dev, struct sk_buff *skb)
{
    struct ethhdr *eth_hdr;
    u16 vid;

    if ((dev->features & NETIF_F_HW_VLAN_RX) &&
        !__vlan_get_tag(skb, &vid)) {
        PDEBUG("*pull_out_vlan*\n");
        eth_hdr = (struct ethhdr *)skb->data;
        memmove(skb->data + VLAN_HLEN, eth_hdr, ETH_ALEN * 2);
        skb_pull(skb, VLAN_HLEN);
        __vlan_hwaccel_put_tag(skb, vid);
    }
}

static int xge_rx(struct blaze_xge_private *priv, int limit)
{
    int res;

    unsigned int count = 0;
    unsigned int next_entry;
    struct sk_buff *skb;
    unsigned int entry = priv->rx_cons % priv->rx_ring_size;
    u32 rxstatus;
    u16 pktlength;
    u16 pktstatus;
    
    while ((count < limit) &&
           (rxstatus = priv->dmaops->get_rx_status(priv)) != 0) {
        pktstatus = rxstatus >> 16;
        pktlength = rxstatus & 0xffff;

        if ((pktstatus & 0xFF) || (pktlength == 0)) {
            netdev_err(priv->dev,
                   "RCV pktstatus %08X pktlength %08X\n",
                   pktstatus, pktlength);
        }

       pktlength -= 2;

        count++;
        next_entry = (++priv->rx_cons) % priv->rx_ring_size;

        skb = priv->rx_ring[entry].skb;
        if (unlikely(!skb)) {
            netdev_err(priv->dev,
                   "%s: Inconsistent Rx descriptor chain\n",
                   __func__);
            priv->dev->stats.rx_dropped++;
            break;
        }
        priv->rx_ring[entry].skb = NULL;

        skb_put(skb, pktlength);

        dma_sync_single_for_cpu(priv->device,
                    priv->rx_ring[entry].dma_addr,
                    priv->rx_ring[entry].len,
                    DMA_FROM_DEVICE);

        dma_unmap_single(priv->device, priv->rx_ring[entry].dma_addr,
                 priv->rx_ring[entry].len, DMA_FROM_DEVICE);

        if (netif_msg_pktdata(priv)) {
            netdev_info(priv->dev, "frame received %d bytes\n",
                    pktlength);
            print_hex_dump(KERN_ERR, "data: ", DUMP_PREFIX_OFFSET,
                       16, 1, skb->data, pktlength, true);
        }

        xge_rx_vlan(priv->dev, skb);

        skb->data -= 2;
        skb->protocol = eth_type_trans(skb, priv->dev);
        skb_checksum_none_assert(skb);
        
        res = napi_gro_receive(&priv->napi, skb);

        priv->dev->stats.rx_packets++;
        priv->dev->stats.rx_bytes += pktlength;

        entry = next_entry;

        dump_sk_buff(skb);

        xge_rx_refill(priv);
   }

    return count;
}

static int xge_tx_complete(struct blaze_xge_private *priv)
{
    unsigned int txsize = priv->tx_ring_size;
    u32 ready;
    unsigned int entry;
    struct xge_buffer *tx_buff;
    int txcomplete = 0;

    PDEBUG("*xge_tx_complete*\n");

    spin_lock(&priv->tx_lock);

    ready = priv->dmaops->tx_completions(priv);

    while (ready && (priv->tx_cons != priv->tx_prod)) {
        entry = priv->tx_cons % txsize;
        tx_buff = &priv->tx_ring[entry];

        if (netif_msg_tx_done(priv))
            netdev_dbg(priv->dev, "%s: curr %d, dirty %d\n",
                   __func__, priv->tx_prod, priv->tx_cons);

        if (likely(tx_buff->skb))
            priv->dev->stats.tx_packets++;

        xge_free_tx_buffer(priv, tx_buff);
        priv->tx_cons++;

        txcomplete++;
        ready--;
    }

    if (unlikely(netif_queue_stopped(priv->dev) &&
             xge_tx_avail(priv) > XGE_TX_THRESH(priv))) {
        netif_tx_lock(priv->dev);
        if (netif_queue_stopped(priv->dev) &&
            xge_tx_avail(priv) > XGE_TX_THRESH(priv)) {
            if (netif_msg_tx_done(priv))
                netdev_dbg(priv->dev, "%s: restart transmit\n",
                       __func__);
            netif_wake_queue(priv->dev);
        }
        netif_tx_unlock(priv->dev);
    }

    spin_unlock(&priv->tx_lock);
    return txcomplete;
}

static int xge_poll(struct napi_struct *napi, int budget)
{
    struct blaze_xge_private *priv =
            container_of(napi, struct blaze_xge_private, napi);
    int rxcomplete = 0;
    int txcomplete = 0;
    unsigned long int flags;
    
    PDEBUG("*xge_poll*\n");

    txcomplete = xge_tx_complete(priv);

    rxcomplete = xge_rx(priv, budget);

    if (rxcomplete >= budget || txcomplete > 0)
        return rxcomplete;

    napi_gro_flush(napi, false);
    __napi_complete(napi);

    netdev_dbg(priv->dev,
           "NAPI Complete, did %d packets with budget %d\n",
           txcomplete+rxcomplete, budget);

    spin_lock_irqsave(&priv->rxdma_irq_lock, flags);
    priv->dmaops->enable_rxirq(priv);
    priv->dmaops->enable_txirq(priv);
    spin_unlock_irqrestore(&priv->rxdma_irq_lock, flags);
    return rxcomplete + txcomplete;
    return 0;
}

static irqreturn_t blaze_isr(int irq, void *dev_id)
{
    struct net_device *dev = dev_id;
    struct blaze_xge_private *priv;
    unsigned long int flags;
    
    PDEBUG("*blaze_isr*\n");

    if (unlikely(!dev)) {
        pr_err("%s: invalid dev pointer\n", __func__);
        return IRQ_NONE;
    }
    priv = netdev_priv(dev);

    spin_lock_irqsave(&priv->rxdma_irq_lock, flags);

    if (likely(napi_schedule_prep(&priv->napi))) {
        priv->dmaops->disable_rxirq(priv);
        priv->dmaops->disable_txirq(priv);
        __napi_schedule(&priv->napi);
    }

    priv->dmaops->clear_rxirq(priv);
    priv->dmaops->clear_txirq(priv);

    spin_unlock_irqrestore(&priv->rxdma_irq_lock, flags);

    return IRQ_HANDLED;
}


static int xge_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    unsigned int txsize = priv->tx_ring_size;
    unsigned int entry;
    struct xge_buffer *buffer = NULL;
    int nfrags = skb_shinfo(skb)->nr_frags;
    unsigned int nopaged_len = skb_headlen(skb);
    enum netdev_tx ret = NETDEV_TX_OK;
    dma_addr_t dma_addr;

    PDEBUG("*xge_start_xmit*\n");

    spin_lock_bh(&priv->tx_lock);

    if (unlikely(xge_tx_avail(priv) < nfrags + 1)) {
        if (!netif_queue_stopped(dev)) {
            PDEBUG("tx queue not stopped\n");
            netif_stop_queue(dev);
            netdev_err(priv->dev,
                   "%s: Tx list full when queue awake\n",
                   __func__);
        }
        PDEBUG("error: tx busy\n");
        ret = NETDEV_TX_BUSY;
        goto out;
    }

    entry = priv->tx_prod % txsize;
    buffer = &priv->tx_ring[entry];

    dma_addr = dma_map_single(priv->device, skb->data, nopaged_len,
                  DMA_TO_DEVICE);
    if (dma_mapping_error(priv->device, dma_addr)) {
        PDEBUG("error: dma_mapping\n");
        netdev_err(priv->dev, "%s: DMA mapping error\n", __func__);
        ret = NETDEV_TX_OK;
        goto out;
    }

    buffer->skb = skb;
    buffer->dma_addr = dma_addr;
    buffer->len = nopaged_len;

    dma_sync_single_for_device(priv->device, buffer->dma_addr,
                   buffer->len, DMA_TO_DEVICE);

    priv->dmaops->tx_buffer(priv, buffer);

    skb_tx_timestamp(skb);

    priv->tx_prod++;
    dev->stats.tx_bytes += skb->len;

    if (unlikely(xge_tx_avail(priv) <= TXQUEUESTOP_THRESHHOLD)) {
        if (netif_msg_hw(priv))
            netdev_dbg(priv->dev, "%s: stop transmitted packets\n",
                   __func__);
        netif_stop_queue(dev);
    }

out:
    spin_unlock_bh(&priv->tx_lock);

    return ret;


    return 0;
}

/*
static void blaze_xge_adjust_link(struct net_device *dev)
{
    PDEBUG("*blaze_xge_adjust_link*\n");
}
*/

/*
static void xge_update_mac_addr(struct blaze_xge_private *priv, u8 *addr)
{
    PDEBUG("*xge_update_mac_addr*\n");
}
*/

/*
static int reset_mac(struct blaze_xge_private *priv)
{
    return 0;
}
*/

/* Initialize MAC core registers
static int init_mac(struct blaze_xge_private *priv)
{
    PDEBUG("*init_mac*\n");
    return 0;
}
*/

static int xge_change_mtu(struct net_device *dev, int new_mtu)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    unsigned int max_mtu = priv->max_mtu;
    unsigned int min_mtu = ETH_ZLEN + ETH_FCS_LEN;
    
    if (netif_running(dev)) {
        netdev_err(dev, "must be stopped to change its MTU\n");
        return -EBUSY;
    }

    if ((new_mtu < min_mtu) || (new_mtu > max_mtu)) {
        netdev_err(dev, "invalid MTU, max MTU is: %u\n", max_mtu);
        return -EINVAL;
    }

    dev->mtu = new_mtu;
    netdev_update_features(dev);

    return 0;
}

static int xge_open(struct net_device *dev)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    unsigned long int flags;
    int ret = 0;
    int i;
    
    PDEBUG("*xge_open*\n");

    /* Reset and configure XGE MAC and probe associated PHY */
    if (netif_msg_ifup(priv))
        netdev_warn(dev, "device MAC address %pM\n",
                dev->dev_addr);
    /*
    if ((priv->revision < 0xd00) || (priv->revision > 0xe00))
        netdev_warn(dev, "XGE revision %x\n", priv->revision);
    spin_lock(&priv->mac_cfg_lock);

    ret = reset_mac(priv);
    if (ret)
        netdev_err(dev, "Cannot reset MAC core (error: %d)\n", ret);

    ret = init_mac(priv);
    spin_unlock(&priv->mac_cfg_lock);
    if (ret) {
        netdev_err(dev, "Cannot init MAC core (error: %d)\n", ret);
        goto alloc_skbuf_error;
    }
    */

    priv->dmaops->reset_dma(priv);

    priv->rx_ring_size = dma_rx_num;
    priv->tx_ring_size = dma_tx_num;
    ret = alloc_init_skbufs(priv);
    if (ret) {
        netdev_err(dev, "DMA descriptors initialization failed\n");
        goto alloc_skbuf_error;
    }

    ret = request_irq(priv->rx_irq, blaze_isr, IRQF_SHARED,
              dev->name, dev);
    if (ret) {
        netdev_err(dev, "Unable to register RX interrupt %d\n",
               priv->rx_irq);
        goto init_error;
    }

    ret = request_irq(priv->tx_irq, blaze_isr, IRQF_SHARED,
              dev->name, dev);
    if (ret) {
        netdev_err(dev, "Unable to register TX interrupt %d\n",
               priv->tx_irq);
        goto tx_request_irq_error;
    }

    spin_lock_irqsave(&priv->rxdma_irq_lock, flags);
    priv->dmaops->enable_rxirq(priv);
    priv->dmaops->enable_txirq(priv);

    for (i = 0; i < priv->rx_ring_size; i++)
        priv->dmaops->add_rx_desc(priv, &priv->rx_ring[i]);

    spin_unlock_irqrestore(&priv->rxdma_irq_lock, flags);

    napi_enable(&priv->napi);
    netif_start_queue(dev);

    /* Start MAC Rx/Tx
    spin_lock(&priv->mac_cfg_lock);
    xge_set_mac(priv, true);
    spin_unlock(&priv->mac_cfg_lock);
    */

    return 0;

tx_request_irq_error:
    free_irq(priv->rx_irq, dev);
init_error:
    free_skbufs(dev);
alloc_skbuf_error:
    return ret;
}


static int xge_shutdown(struct net_device *dev)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    unsigned long int flags;

    netif_stop_queue(dev);
    napi_disable(&priv->napi);

    spin_lock_irqsave(&priv->rxdma_irq_lock, flags);
    priv->dmaops->disable_rxirq(priv);
    priv->dmaops->disable_txirq(priv);
    spin_unlock_irqrestore(&priv->rxdma_irq_lock, flags);

    free_irq(priv->rx_irq, dev);
    free_irq(priv->tx_irq, dev);

    /* spin_lock(&priv->mac_cfg_lock); */
    spin_lock(&priv->tx_lock);
    /*
    ret = reset_mac(priv);
    if (ret)
        netdev_err(dev, "Cannot reset MAC core (error: %d)\n", ret);
    */
    priv->dmaops->reset_dma(priv);
    free_skbufs(dev);

    spin_unlock(&priv->tx_lock);
    /* spin_unlock(&priv->mac_cfg_lock); */

    return 0;
}

static struct net_device_ops blaze_xge_netdev_ops = {
    .ndo_open       = xge_open,
    .ndo_stop       = xge_shutdown,
    .ndo_start_xmit     = xge_start_xmit,
    .ndo_set_mac_address    = eth_mac_addr,
    .ndo_change_mtu     = xge_change_mtu,
    .ndo_validate_addr  = eth_validate_addr,
    /*.ndo_set_rx_mode    = xge_set_rx_mode,*/
};


static int request_and_map(struct platform_device *pdev, const char *name,
               struct resource **res, void __iomem **ptr)
{
    struct resource *region;
    struct device *device = &pdev->dev;

    *res = platform_get_resource_byname(pdev, IORESOURCE_MEM, name);
    if (*res == NULL) {
        dev_err(device, "resource %s not defined\n", name);
        return -ENODEV;
    }

    region = devm_request_mem_region(device, (*res)->start,
                     resource_size(*res), dev_name(device));
    if (region == NULL) {
        dev_err(device, "unable to request %s\n", name);
        return -EBUSY;
    }

    *ptr = devm_ioremap_nocache(device, region->start,
                    resource_size(region));
    if (*ptr == NULL) {
        dev_err(device, "ioremap_nocache of %s failed!", name);
        return -ENOMEM;
    }

    return 0;
}

static int blaze_xge_probe(struct platform_device *pdev)
{
    struct net_device *ndev;
    int ret = -ENODEV;
    struct resource *dma_res;
    struct blaze_xge_private *priv;
    const unsigned char *macaddr;
    const struct of_device_id *of_id = NULL;
    /* struct resource *control_port; */

    PDEBUG("*blaze_xge_probe*\n");

    ndev = alloc_etherdev(sizeof(struct blaze_xge_private));
    if (!ndev) {
        dev_err(&pdev->dev, "Could not allocate network device\n");
        PDEBUG("error: alloc_etherdev\n");
        return -ENODEV;
    }

    SET_NETDEV_DEV(ndev, &pdev->dev);

    priv = netdev_priv(ndev);
    priv->device = &pdev->dev;
    priv->dev = ndev;
    priv->msg_enable = netif_msg_init(debug, default_msg_level);

    of_id = of_match_device(blaze_xge_ids, &pdev->dev);

    if (of_id)
        priv->dmaops = (struct blaze_dmaops *)of_id->data;

    if (priv->dmaops ) {
        ret = request_and_map(pdev, "rx_resp", &dma_res,
                      &priv->rx_dma_resp);
        if (ret) {
            PDEBUG("error: rx_resp\n");
            goto err_free_netdev;
        }
        PDEBUG("rx_resp: %p\n", priv->rx_dma_resp);

        ret = request_and_map(pdev, "tx_desc", &dma_res,
                      &priv->tx_dma_desc);
        if (ret) {
            PDEBUG("error: tx_desc\n");
            goto err_free_netdev;
        }
        PDEBUG("tx_desc: %p\n", priv->tx_dma_desc);

        priv->txdescmem = resource_size(dma_res);
        priv->txdescmem_busaddr = dma_res->start;

        ret = request_and_map(pdev, "rx_desc", &dma_res,
                      &priv->rx_dma_desc);
        if (ret) {
            PDEBUG("error: rx_desc\n");
            goto err_free_netdev;
        }
        PDEBUG("rx_desc: %p\n", priv->rx_dma_desc);

        priv->rxdescmem = resource_size(dma_res);
        priv->rxdescmem_busaddr = dma_res->start;

    } else {
        PDEBUG("error: priv->dmaops\n");
        goto err_free_netdev;
    }

    /* 
    ret = request_and_map(pdev, "control_port", &control_port,
                  (void __iomem **)&priv->mac_dev);
    if (ret)
        goto err_free_netdev;
    */

    ret = request_and_map(pdev, "rx_csr", &dma_res,
                  &priv->rx_dma_csr);
    if (ret) {
        PDEBUG("error: rx_csr\n");
        goto err_free_netdev;
    }
    PDEBUG("rx_dma_csr: %p\n", priv->rx_dma_csr);


    ret = request_and_map(pdev, "tx_csr", &dma_res,
                  &priv->tx_dma_csr);
    if (ret) {
        PDEBUG("error: tx_csr\n");
        goto err_free_netdev;
    }
    PDEBUG("tx_dma_csr: %p\n", priv->tx_dma_csr);


    priv->rx_irq = platform_get_irq_byname(pdev, "rx_irq");
    if (priv->rx_irq == -ENXIO) {
        dev_err(&pdev->dev, "cannot obtain Rx IRQ\n");
        ret = -ENXIO;
        PDEBUG("error: rx_irq\n");
        goto err_free_netdev;
    }
    PDEBUG("rx_irq: %u\n", priv->rx_irq);

    priv->tx_irq = platform_get_irq_byname(pdev, "tx_irq");
    if (priv->tx_irq == -ENXIO) {
        dev_err(&pdev->dev, "cannot obtain Tx IRQ\n");
        ret = -ENXIO;
        PDEBUG("error: tx_irq\n");
        goto err_free_netdev;
    }
    PDEBUG("tx_irq: %u\n", priv->tx_irq);

    if (of_property_read_u32(pdev->dev.of_node, "rx-fifo-depth",
                 &priv->rx_fifo_depth)) {
        dev_err(&pdev->dev, "cannot obtain rx-fifo-depth\n");
        ret = -ENXIO;
        PDEBUG("error: rx-fifo-depth\n");
        goto err_free_netdev;
    }
    PDEBUG("rx_fifo_depth: %u\n", priv->rx_fifo_depth);

    if (of_property_read_u32(pdev->dev.of_node, "tx-fifo-depth",
                 &priv->tx_fifo_depth)) {
        dev_err(&pdev->dev, "cannot obtain tx-fifo-depth\n");
        ret = -ENXIO;
        PDEBUG("error: tx-fifo-depth\n");
        goto err_free_netdev;
    }
    PDEBUG("tx_fifo_depth: %u\n", priv->tx_fifo_depth);

    priv->max_mtu = ETH_DATA_LEN;

    of_property_read_u32(pdev->dev.of_node, "max-frame-size",
                 &priv->max_mtu);
    PDEBUG("max-frame-size: %u\n", priv->max_mtu);

    priv->rx_dma_buf_sz = BLAZE_RXDMABUFFER_SIZE;

    macaddr = of_get_mac_address(pdev->dev.of_node);
    if (macaddr) {
        memcpy(ndev->dev_addr, macaddr, ETH_ALEN);
    } else {
        eth_hw_addr_random(ndev);
    } 
    PDEBUG("eth addr: %#010x\n", *((int *)ndev->dev_addr));

    if (ret)
        goto err_free_netdev;

    ether_setup(ndev);

    /* ndev->mem_start = control_port->start;
    ndev->mem_end = control_port->end;
    */

    ndev->netdev_ops = &blaze_xge_netdev_ops;
    blaze_xge_set_ethtool_ops(ndev);

    /*blaze_xge_netdev_ops.ndo_set_rx_mode = xge_set_rx_mode;*/

    ndev->hw_features &= ~NETIF_F_SG;
    ndev->features |= ndev->hw_features | NETIF_F_HIGHDMA | NETIF_F_HW_VLAN_RX;

    netif_napi_add(ndev, &priv->napi, xge_poll, NAPI_POLL_WEIGHT);

    spin_lock_init(&priv->mac_cfg_lock);
    spin_lock_init(&priv->tx_lock);
    spin_lock_init(&priv->rxdma_irq_lock);

    ret = register_netdev(ndev);
    if (ret) {
        dev_err(&pdev->dev, "failed to register XGE net device\n");
        goto err_register_netdev;
    }

    platform_set_drvdata(pdev, ndev);

    return 0;

err_register_netdev:
    netif_napi_del(&priv->napi);
err_free_netdev:
    free_netdev(ndev);
    return ret;
}

static int blaze_xge_remove(struct platform_device *pdev)
{
	struct net_device *ndev = platform_get_drvdata(pdev);

    PDEBUG("*blaze_xge_remove*");

    platform_set_drvdata(pdev, NULL);

    unregister_netdev(ndev);
    free_netdev(ndev);
    
    return 0;
}

static const struct blaze_dmaops blaze_dtype_msgdma = {
    .dmamask = 64,
    .reset_dma = msgdma_reset,
    .enable_txirq = msgdma_enable_txirq,
    .enable_rxirq = msgdma_enable_rxirq,
    .disable_txirq = msgdma_disable_txirq,
    .disable_rxirq = msgdma_disable_rxirq,
    .clear_txirq = msgdma_clear_txirq,
    .clear_rxirq = msgdma_clear_rxirq,
    .tx_buffer = msgdma_tx_buffer,
    .tx_completions = msgdma_tx_completions,
    .add_rx_desc = msgdma_add_rx_desc,
    .get_rx_status = msgdma_rx_status,
};

static const struct of_device_id blaze_xge_ids[] = {
    { .compatible = "altr,xge-ethernet", .data = &blaze_dtype_msgdma, },
    {},
};

MODULE_DEVICE_TABLE(of, blaze_xge_ids);

static struct platform_driver blaze_xge_ethernet_driver = {
	.probe = blaze_xge_probe,
	.remove = blaze_xge_remove,
	.suspend = NULL,
	.resume = NULL,
	.driver = {
		   .name = BLAZE_XGE_RESOURCE_NAME,
		   .of_match_table = blaze_xge_ids,
    },
};

module_platform_driver(blaze_xge_ethernet_driver);

MODULE_AUTHOR("Christopher Campbell");
MODULE_DESCRIPTION("Blaze 10 GbE Ethernet Driver");
MODULE_LICENSE("GPL v2");
