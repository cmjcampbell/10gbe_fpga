#include <linux/ethtool.h>
#include <linux/kernel.h>
#include <linux/netdevice.h>
#include <linux/phy.h>

#include "blaze_xge_main.h"

#define DEBUG

#ifdef DEBUG
#define PDEBUG(fmt, args...) printk( KERN_DEBUG "blaze_ethtool: " fmt, ## args)
#else
#define PDEBUG(fmt, args...)
#endif

#define XGE_STATS_LEN   2
#define XGE_NUM_REGS    128

static char const stat_gstrings[][ETH_GSTRING_LEN] = {
    "tx_packets",
    "rx_packets",
};

static void xge_get_drvinfo(struct net_device *dev,
                struct ethtool_drvinfo *info)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    u32 rev = ioread32(&priv->mac_dev->megacore_revision);

    strcpy(info->driver, "blaze_xge");
    strcpy(info->version, "v8.0");
    snprintf(info->fw_version, ETHTOOL_FWVERS_LEN, "v%d.%d",
         rev & 0xFFFF, (rev & 0xFFFF0000) >> 16);
    sprintf(info->bus_info, "platform");
}

static void xge_gstrings(struct net_device *dev, u32 stringset, u8 *buf)
{
    memcpy(buf, stat_gstrings, XGE_STATS_LEN * ETH_GSTRING_LEN);
}

static void xge_fill_stats(struct net_device *dev, struct ethtool_stats *dummy,
               u64 *buf)
{
/*
    u64 ext;
    struct blaze_xge_private *priv = netdev_priv(dev);
    buf[0] = csrrd32(priv->mac_dev,
             xge_csroffs(frames_transmitted_ok));
    buf[1] = csrrd32(priv->mac_dev,
             xge_csroffs(frames_received_ok));
*/
    buf[0] = 0;
    buf[1] = 0;
   }

static int xge_sset_count(struct net_device *dev, int sset)
{
    switch (sset) {
    case ETH_SS_STATS:
        return XGE_STATS_LEN;
    default:
        return -EOPNOTSUPP;
    }
}

static u32 xge_get_msglevel(struct net_device *dev)
{
    struct blaze_xge_private *priv = netdev_priv(dev);

    return priv->msg_enable;
}

static void xge_set_msglevel(struct net_device *dev, uint32_t data)
{
    struct blaze_xge_private *priv = netdev_priv(dev);

    priv->msg_enable = data;
}

static int xge_reglen(struct net_device *dev)
{
    return XGE_NUM_REGS * sizeof(u32);
}

static void xge_get_regs(struct net_device *dev, struct ethtool_regs *regs,
             void *regbuf)
{
    int i;
    struct blaze_xge_private *priv = netdev_priv(dev);
    u32 *buf = regbuf;

    regs->version = 1;

    for (i = 0; i < XGE_NUM_REGS; i++)
        buf[i] = csrrd32(priv->mac_dev, i * 4);
}

static int xge_get_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    struct phy_device *phydev = priv->phydev;

    if (phydev == NULL) {
        return -ENODEV;
    }

    return phy_ethtool_gset(phydev, cmd);
}

static int xge_set_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
    struct blaze_xge_private *priv = netdev_priv(dev);
    struct phy_device *phydev = priv->phydev;

    if (phydev == NULL) {
        return -ENODEV;
    }

    return phy_ethtool_sset(phydev, cmd);
}

static const struct ethtool_ops xge_ethtool_ops = {
    .get_drvinfo = xge_get_drvinfo,
    .get_regs_len = xge_reglen,
    .get_regs = xge_get_regs,
    .get_link = ethtool_op_get_link,
    .get_settings = xge_get_settings,
    .set_settings = xge_set_settings,
    .get_strings = xge_gstrings,
    .get_sset_count = xge_sset_count,
    .get_ethtool_stats = xge_fill_stats,
    .get_msglevel = xge_get_msglevel,
    .set_msglevel = xge_set_msglevel,
};

void blaze_xge_set_ethtool_ops(struct net_device *netdev)
{
    netdev->ethtool_ops = &xge_ethtool_ops;
}

