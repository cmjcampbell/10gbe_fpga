/* Blaze 10 Gigabit Ethernet Driver
 *
 * Contributors:
 *  Christopher Campbell (cc3769@columbia.edu)
 *
 * Original driver contributed by Altera.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _BLAZE_XGE_MAIN_H
#define _BLAZE_XGE_MAIN_H

#define BLAZE_XGE_RESOURCE_NAME        "blaze_xge"

#include <linux/bitops.h>
#include <linux/if_vlan.h>
#include <linux/list.h>
#include <linux/netdevice.h>
#include <linux/phy.h>

#define BLAZE_XGE_SW_RESET_WATCHDOG_CNTR    10000
#define BLAZE_XGE_MAC_FIFO_WIDTH            4

#define BLAZE_XGE_RX_SECTION_EMPTY          16
#define BLAZE_XGE_RX_SECTION_FULL           0
#define BLAZE_XGE_RX_ALMOST_EMPTY           8
#define BLAZE_XGE_RX_ALMOST_FULL            8

#define BLAZE_XGE_TX_SECTION_EMPTY          16
#define BLAZE_XGE_TX_SECTION_FULL           0
#define BLAZE_XGE_TX_ALMOST_EMPTY           8
#define BLAZE_XGE_TX_ALMOST_FULL            3
 
#define BLAZE_XGE_TX_IPG_LENGTH             12 
#define BLAZE_XGE_PAUSE_QUANTA              0xffff
#define GET_BIT_VALUE(v, bit)               (((v) >> (bit)) & 0x1)
 
#define MAC_CMDCFG_TX_ENA                   BIT(0)

struct blaze_xge_mac {
    u32 megacore_revision;
};

#define xge_csroffs(a) (offsetof(struct blaze_xge_mac, a))
 
#define BALZE_XGE_TX_CMD_STAT_OMIT_CRC         BIT(17)
#define BALZE_XGE_TX_CMD_STAT_TX_SHIFT16       BIT(18)
#define BALZE_XGE_RX_CMD_STAT_RX_SHIFT16       BIT(25)

struct xge_buffer {
    struct list_head lh;
    struct sk_buff *skb;
    dma_addr_t dma_addr;
    u32 len;
    int mapped_as_page;
};

struct blaze_xge_private;

struct blaze_dmaops {
    int dmamask;
    void (*reset_dma)(struct blaze_xge_private *);
    void (*enable_txirq)(struct blaze_xge_private *);
    void (*enable_rxirq)(struct blaze_xge_private *);
    void (*disable_txirq)(struct blaze_xge_private *);
    void (*disable_rxirq)(struct blaze_xge_private *);
    void (*clear_txirq)(struct blaze_xge_private *);
    void (*clear_rxirq)(struct blaze_xge_private *);
    int (*tx_buffer)(struct blaze_xge_private *, struct xge_buffer *);
    u32 (*tx_completions)(struct blaze_xge_private *);
    void (*add_rx_desc)(struct blaze_xge_private *, struct xge_buffer *);
    u32 (*get_rx_status)(struct blaze_xge_private *);
    int (*init_dma)(struct blaze_xge_private *);
    void (*uninit_dma)(struct blaze_xge_private *);
    void (*start_rxdma)(struct blaze_xge_private *);
};

struct blaze_xge_private {
    struct net_device *dev;
    struct device *device;
    struct napi_struct napi;

    struct blaze_xge_mac __iomem *mac_dev;

    u32     revision;

    void __iomem *rx_dma_csr;
    void __iomem *rx_dma_desc;
    void __iomem *rx_dma_resp;

    void __iomem *tx_dma_csr;
    void __iomem *tx_dma_desc;

    struct xge_buffer *rx_ring;
    u32 rx_cons;
    u32 rx_prod;
    u32 rx_ring_size;
    u32 rx_dma_buf_sz;

    struct xge_buffer *tx_ring;
    u32 tx_prod;
    u32 tx_cons;
    u32 tx_ring_size;

    u32 tx_irq;
    u32 rx_irq;

    u32 tx_fifo_depth;
    u32 rx_fifo_depth;
    u32 max_mtu;

    u32 txdescmem;
    u32 rxdescmem;
    dma_addr_t rxdescmem_busaddr;
    dma_addr_t txdescmem_busaddr;
    u32 txctrlreg;
    u32 rxctrlreg;
    dma_addr_t rxdescphys;
    dma_addr_t txdescphys;

    struct list_head txlisthd;
    struct list_head rxlisthd;

    spinlock_t mac_cfg_lock;
    spinlock_t tx_lock;
    spinlock_t rxdma_irq_lock;

    u32 msg_enable;

    struct phy_device *phydev;

    struct blaze_dmaops *dmaops;

    /*struct net_device_stats stats;*/
};

void blaze_xge_set_ethtool_ops(struct net_device *);

static inline
u32 csrrd32(void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    return readl(paddr);
}

static inline
u16 csrrd16(void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    return readw(paddr);
}

static inline
u8 csrrd8(void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    return readb(paddr);
}

static inline
void csrwr32(u32 val, void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    writel(val, paddr);
}

static inline
void csrwr16(u16 val, void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    writew(val, paddr);
}

static inline
void csrwr8(u8 val, void __iomem *mac, size_t offs)
{
    void __iomem *paddr = (void __iomem *)((uintptr_t)mac + offs);
    writeb(val, paddr);
}


#endif /* _BLAZE_XGE_MAIN_H_ */
