#! /bin/bash

if [ "$1" = "-b" ]
then
    ping -I eth1 -c $2 -b 192.168.2.255
else
    ping -I eth1 -c $1 192.168.2.1
fi
