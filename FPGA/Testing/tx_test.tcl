source common.tcl

open_jtag

set src_base 0x208
set src_base_control 0x20C

puts "Begin writing packet"
master_write_32 $jtag_master $src_base_control 0x00000001
master_write_32 $jtag_master $src_base 0x01005e00
master_write_32 $jtag_master $src_base 0x00010017
master_write_32 $jtag_master $src_base 0xf293c2df
master_write_32 $jtag_master $src_base 0x08004500
master_write_32 $jtag_master $src_base 0x0046417c
master_write_32 $jtag_master $src_base 0x00000111
master_write_32 $jtag_master $src_base 0x032d803b
master_write_32 $jtag_master $src_base 0x14c2e000
master_write_32 $jtag_master $src_base 0x00010272
master_write_32 $jtag_master $src_base 0x02720032
master_write_32 $jtag_master $src_base 0x2fd6534e
master_write_32 $jtag_master $src_base 0x51554552
master_write_32 $jtag_master $src_base 0x593a6468
master_write_32 $jtag_master $src_base 0x63703437
master_write_32 $jtag_master $src_base 0x2e63732e
master_write_32 $jtag_master $src_base 0x636f6c75
master_write_32 $jtag_master $src_base 0x6d626961
master_write_32 $jtag_master $src_base 0x2e656475
master_write_32 $jtag_master $src_base 0x3a725a77
master_write_32 $jtag_master $src_base 0x6472533a
master_write_32 $jtag_master $src_base 0x78737672
master_write_32 $jtag_master $src_base_control 0x00000002
master_write_32 $jtag_master $src_base 0xA9ffffff
puts "Done writing packet"

close_jtag
