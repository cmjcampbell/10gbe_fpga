#==============================================================================
#                       Address Map                
#==============================================================================
set MAC_BASE_ADDR 		0x00008000

#==============================================================================
#                       MAC Address Map                
#==============================================================================
# RX path
set RX_PACKET_TRANSFER_BASE_ADDR  0x00000000
set RX_PAD_REMOVER_BASE_ADDR      0x00000100
set RX_CRC_CHECKER_BASE_ADDR      0x00000200 
set RX_OVERFLOW_CTRL_BASE_ADDR    0x00000300
set RX_FRAME_DECODER_BASE_ADDR    0x00002000
set RX_STATISTICS_BASE_ADDR       0x00003000

# TX path
set TX_PACKET_TRANSFER_BASE_ADDR   0x00004000
set TX_PAD_INSERTER_BASE_ADDR      0x00004100
set TX_CRC_INSERTER_BASE_ADDR      0x00004200
set TX_PAUSE_GEN_CTRL_BASE_ADDR    0x00004500
set TX_ADDRESS_INSERTER_BASE_ADDR  0x00004800
set TX_FRAME_DECODER_BASE_ADDR     0x00006000 
set TX_STATISTICS_BASE_ADDR        0x00007000

#==============================================================================
#                       APIs for MAC              
#==============================================================================
# Read all registers of TX address inserter
proc read_tx_address_inserter {} {
    global MAC_BASE_ADDR
    global TX_ADDRESS_INSERTER_BASE_ADDR
    global add_inserter

    puts "=============================================================================="
    puts "                      Reading Address Inserter CSR			        "
    puts "==============================================================================\n\n"
    puts "control status : [rd32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR 0x0]\n"
    puts "MAC src addr 0 : [rd32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR 0x4]\n"
    puts "MAC src addr 1 : [rd32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR 0x8]\n"
}

# Write to TX address inserter register
proc set_tx_address_inserter {value} {
    global MAC_BASE_ADDR
    global TX_ADDRESS_INSERTER_BASE_ADDR
    set offset 0x0

    puts "=============================================================================="
    puts "                      Write to Control register of Address Inserter  	        "
    puts "==============================================================================\n\n"
    puts "Write to offset $offset of Address inserter with value of $value\n"
    wr32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR $offset $value
}

# Write to TX address register with the given value
proc write_tx_address {value} {
    global MAC_BASE_ADDR
    global TX_ADDRESS_INSERTER_BASE_ADDR

    puts "=============================================================================="
    puts "                      Write MAC Source Address to Address Inserter 		"
    puts "==============================================================================\n\n"
    set offset 0x4
    set lowerMAC 0x[string range $value 6 13]
    puts "Write to offset $offset of Address Inserter with value of $lowerMAC\n"
    wr32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR $offset $lowerMAC

    set offset 0x8
    set higherMAC [string range $value 0 5]
    puts "Write to offset $offset of Address Inserter with value of $higherMAC\n"
    wr32 $MAC_BASE_ADDR $TX_ADDRESS_INSERTER_BASE_ADDR $offset $higherMAC
}

# Read transfer register
proc read_transfer {path} {
    global MAC_BASE_ADDR
    global TX_PACKET_TRANSFER_BASE_ADDR
    global RX_PACKET_TRANSFER_BASE_ADDR

    if {$path == "tx"} {
        puts "=============================================================================="
        puts "                      Reading TX Transfer CSR		             	        "
        puts "==============================================================================\n\n"
        puts "control : [rd32 $MAC_BASE_ADDR $TX_PACKET_TRANSFER_BASE_ADDR 0x0]\n" 
        puts "status  : [rd32 $MAC_BASE_ADDR $TX_PACKET_TRANSFER_BASE_ADDR 0x4]\n" 
    } elseif {$path == "rx"} {
        puts "=============================================================================="
        puts "                      Reading RX Transfer CSR		             	        "
        puts "==============================================================================\n\n"
        puts "control : [rd32 $MAC_BASE_ADDR $RX_PACKET_TRANSFER_BASE_ADDR 0x0]\n" 
        puts "status : [rd32 $MAC_BASE_ADDR $RX_PACKET_TRANSFER_BASE_ADDR 0x4]\n" 
    } else {
        puts "Wrong argument for read_backpressure"
        set 
    }
}

# Read RX frame control register
proc read_rx_frame_control {} {
    global MAC_BASE_ADDR
    global RX_FRAME_DECODER_BASE_ADDR

    puts "=============================================================================="
    puts "                      Reading RX Frame Control CSR		             	        "
    puts "==============================================================================\n\n"
    puts "control/status : [rd32 $MAC_BASE_ADDR $RX_FRAME_DECODER_BASE_ADDR 0x0]\n" 
}

# Read all statistics registers of the given path (rx or tx)
proc read_stats {path} {
    global MAC_BASE_ADDR
    global TX_STATISTICS_BASE_ADDR
    global RX_STATISTICS_BASE_ADDR

    set base_address 0x0
    set read_error 0x0
    
    if {$path ==  "tx"} {
        puts "=============================================================================="
        puts "                      Reading TX Statistics			            "
        puts "==============================================================================\n\n"
        set base_address $TX_STATISTICS_BASE_ADDR
    } elseif {$path == "rx"} {
        puts "=============================================================================="
        puts "                      Reading RX Statistics			            "
        puts "==============================================================================\n\n"
        set base_address $RX_STATISTICS_BASE_ADDR
    } else {
        puts "Wrong argument for read_stats\n"
        set read_error 0x1
    }

    if {$read_error == 0x0} {
        puts "clr : [rd32 $MAC_BASE_ADDR $base_address 0x00]\n"
        puts "framesOK : [rd64 $MAC_BASE_ADDR $base_address 0x08]\n"
        puts "framesErr : [rd64 $MAC_BASE_ADDR $base_address 0x10]\n"
        puts "framesCRCErr : [rd64 $MAC_BASE_ADDR $base_address 0x18]\n"
        puts "octetsOK : [rd64 $MAC_BASE_ADDR $base_address 0x20]\n"
        puts "pauseMACCtrlFrames : [rd64 $MAC_BASE_ADDR $base_address 0x28]\n"
        puts "ifErrors : [rd64 $MAC_BASE_ADDR $base_address 0x30]\n"
        puts "unicastFramesOK : [rd64 $MAC_BASE_ADDR $base_address 0x38]\n"
        puts "unicastFramesErr : [rd64 $MAC_BASE_ADDR $base_address 0x40]\n"
        puts "multicastFramesOK : [rd64 $MAC_BASE_ADDR $base_address 0x48]\n"
        puts "multicastFramesErr : [rd64 $MAC_BASE_ADDR $base_address 0x50]\n"
        puts "broadcastFramesOK : [rd64 $MAC_BASE_ADDR $base_address 0x58]\n"
        puts "broadcastFramesErr : [rd64 $MAC_BASE_ADDR $base_address 0x60]\n"
        puts "etherStatsOctets : [rd64 $MAC_BASE_ADDR $base_address 0x68]\n"
        puts "etherStatsPkts : [rd64 $MAC_BASE_ADDR $base_address 0x70]\n"
        puts "etherStatsUnderSizePkts : [rd64 $MAC_BASE_ADDR $base_address 0x78]\n"
        puts "etherStatsOversizePkts : [rd64 $MAC_BASE_ADDR $base_address 0x80]\n"
        puts "etherStatsPkts64Octets : [rd64 $MAC_BASE_ADDR $base_address 0x88]\n"
        puts "etherStatsPkts65to127Octets : [rd64 $MAC_BASE_ADDR $base_address 0x90]\n"
        puts "etherStatsPkts128to255Octets : [rd64 $MAC_BASE_ADDR $base_address 0x98]\n"
        puts "etherStatsPkts256to511Octet : [rd64 $MAC_BASE_ADDR $base_address 0xA0]\n"
        puts "etherStatsPkts512to1023Octets : [rd64 $MAC_BASE_ADDR $base_address 0xA8]\n"
        puts "etherStatsPkts1024to1518Octets : [rd64 $MAC_BASE_ADDR $base_address 0xB0]\n"
        puts "etherStatsPkts1518toXOctets : [rd64 $MAC_BASE_ADDR $base_address 0xB8]\n"
        puts "etherStatsFragments : [rd64 $MAC_BASE_ADDR $base_address 0xC0]\n"
        puts "etherStatsJabbers : [rd64 $MAC_BASE_ADDR $base_address 0xC8]\n"
        puts "etherStatsCRCErr : [rd64 $MAC_BASE_ADDR $base_address 0xD0]\n"
        puts "unicastMACCtrlFrames : [rd64 $MAC_BASE_ADDR $base_address 0xD8]\n"
        puts "multicastMACCtrlFrames : [rd64 $MAC_BASE_ADDR $base_address 0xE0]\n"
        puts "broadcastMACCtrlFrames : [rd64 $MAC_BASE_ADDR $base_address 0xE8]\n" 
   }      
}

# Clear all statistics registers both on the TX and on the RX path
proc clear_stats {} {
    global MAC_BASE_ADDR
    global MacStatistics
    global RX_STATISTICS_BASE_ADDR
    global TX_STATISTICS_BASE_ADDR

    puts "Clear RX statistics registers\n"
    wr32 $MAC_BASE_ADDR $RX_STATISTICS_BASE_ADDR 0x0 0x1
    
    puts "Clear TX statistics registers\n"
    wr32 $MAC_BASE_ADDR $TX_STATISTICS_BASE_ADDR 0x0 0x1

}
