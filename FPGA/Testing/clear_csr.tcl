source common.tcl
source csr_pkg.tcl

open_jtag

puts "=============================================================================="
puts "                      Clearing Ethernet 10G MAC CSR			    "
puts "==============================================================================\n\n"

clear_stats

close_jtag
