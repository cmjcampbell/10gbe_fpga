source common.tcl
source csr_pkg.tcl

open_jtag

puts "=============================================================================="
puts "                      Accessing Ethernet 10G MAC CSR			    "
puts "==============================================================================\n\n"
read_transfer tx
read_transfer rx
read_rx_frame_control
read_stats tx
read_stats rx

close_jtag
