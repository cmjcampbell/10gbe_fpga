import sys
from socket import *
def sendeth(src,dst,eth_type,payload,interface = "eth1"):
        s = socket(AF_PACKET, SOCK_RAW)
        s.bind((interface, 0))
        return s.send(src+dst+eth_type+payload)

if __name__=="__main__":
        print("Sending raw packets on eth1")
        s = socket(AF_PACKET, SOCK_RAW)
        s.bind((sys.argv[1],0))
        content = ''
        for i in range(int(sys.argv[2])):
            content = content + chr(0x00+i)
        s.send(content)
