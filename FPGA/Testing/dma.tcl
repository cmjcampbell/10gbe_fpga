source common.tcl

set DMA_READ_OFF    0x00000000
set DMA_WRITE_OFF   0x00000004
set DMA_LENGTH_OFF  0x00000008
set DMA_CONTROL_OFF 0x0000000C

set DMA_CSR         0x00000000

set CONTROL_GO      0x80000000
set CONTROL_IRQ     0x00004000

#==============================================================================
#                       Transfer Request
#==============================================================================
proc dma_transfer {desc_addr source_addr dest_addr length} {

    global DMA_READ_OFF
    global DMA_WRITE_OFF
    global DMA_LENGTH_OFF
    global DMA_CONTROL_OFF
    global CONTROL_GO
    global CONTROL_IRQ

    wr32 $desc_addr $DMA_READ_OFF 0 $source_addr;
    wr32 $desc_addr $DMA_WRITE_OFF 0 $dest_addr;
    wr32 $desc_addr $DMA_LENGTH_OFF 0 $length;
    wr32 $desc_addr $DMA_CONTROL_OFF 0 [$CONTROL_GO & $CONTROL_IRQ];
     
}
