set JTAG_OPEN 0

#==============================================================================
#                       Open JTAG               
#==============================================================================
proc open_jtag {} {
    global jtag_master
    global JTAG_OPEN

    if {$JTAG_OPEN == 1} {
        close_service master $jtag_master;
    } 

    set jtag_master [lindex [get_service_paths master] 0];
    open_service master $jtag_master;
    set $JTAG_OPEN 1
    puts "\nInfo: Opened JTAG Master Service\n"
}

#==============================================================================
#                       Open JTAG               
#==============================================================================
proc open_jtag_num {num} {
    global jtag_master
    global JTAG_OPEN

    if {$JTAG_OPEN == 1} {
        close_service master $jtag_master;
    }
    
    set jtag_master [lindex [get_service_paths master] $num];
    open_service master $jtag_master;
    set $JTAG_OPEN 1
    puts "\nInfo: Opened JTAG Master Service\n"
}


#==============================================================================
#                       Close JTAG               
#==============================================================================
proc close_jtag {} {
    global jtag_master
    global JTAG_OPEN

    if {$JTAG_OPEN == 1} {
        close_service master $jtag_master;
    }

    set $JTAG_OPEN 0
    puts "\nInfo: Closed JTAG Master Service\n\n";
}

#==============================================================================
# Write 32                
#==============================================================================
proc wr32 {base_address sub_base_address offset value} {
    global jtag_master
	
    set address [expr $base_address + $sub_base_address + $offset];
    set address_hex [format "%#x" $address];
    master_write_32 $jtag_master $address_hex $value;
}

#==============================================================================
# Read 32                
#==============================================================================
proc rd32 {base_address sub_base_address offset} {
    global jtag_master
	
    set address [expr $base_address + $sub_base_address + $offset];
    set address_hex [format "%#x" $address];
    set data [master_read_32 $jtag_master $address_hex 1] 
    return $data
}

#==============================================================================
# Read 64                
#==============================================================================
proc rd64 {base_address sub_base_address offset} {
    global jtag_master

    set address [expr $base_address + $sub_base_address + $offset];
    set address_hex [format "%#x" $address];
    set data1 [master_read_32 $jtag_master $address_hex 1]
    set data2 [master_read_32 $jtag_master [expr $address_hex + 0x04] 1]
    regsub {0x} $data1 {} data1  
    return $data2$data1
}

