source common.tcl
source csr_pkg.tcl

open_jtag

puts "=============================================================================="
puts "                      Accessing Ethernet 10G MAC CSR			    "
puts "==============================================================================\n\n"

#==============================================================================
#                       Configuring MAC Source Address               
#==============================================================================
read_tx_address_inserter
set_tx_address_inserter 1
write_tx_address 0xFFFFFFFFFFFF
#write_tx_address 0x000F5308CFFD
#write_tx_address 0xEECC88CCAAEE
read_tx_address_inserter

close_jtag
