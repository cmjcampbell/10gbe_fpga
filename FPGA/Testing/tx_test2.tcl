source common.tcl

open_jtag

set src_base 0x208
set src_base_control 0x20C

puts "Begin writing packet"
master_write_32 $jtag_master $src_base_control 0x00000001
master_write_32 $jtag_master $src_base 0xffffffff
master_write_32 $jtag_master $src_base_control 0x00000002
master_write_32 $jtag_master $src_base 0xffffffff
puts "Done writing packet"

close_jtag
