/*-----------MAC_RX module-------------
Mealy FSM for rx module

*/

module mac_rx(
	input 					clk, 			//clock for mac module
	input 					rst,			//reset signal, active high
	output reg [63:0]		rx_data,		//data input of format Avalon-ST for transmition path
	output reg				rx_sop,			//Avalon ST interface for transmition
	output reg				rx_eop,
	output reg				rx_valid,
	output reg [2:0]		rx_empty,
	input 					rx_ready,
	input [71:0]			rx_xgmii);

	//------------Internal Constants-------
	parameter SIZE = 5;
	parameter IDLE = 5'd1, START = 5'd2, SOP = 5'd3, RECEIVE = 5'd4, EOP = 5'd5, CRC = 5'd6, ERR = 5'd7;
	//------------Internal Variables-------
	reg [SIZE-1:0]		state;
	reg [SIZE-1:0]		next_state;
	reg [31:0]			crc_current;
	reg [31:0]			crc_next;
	//---------------------------------------
	reg					crc_rst;
	reg					crc_en;
	reg 					crc_flag;
	//----------Buffers--------------------
	reg [63:0]			data_buf;
	reg [63:0]			data_locked;
	reg [7:0]			ctrl_locked;
	reg [7:0]			ctrl_buf;
	reg 					sop, eop, valid;
	reg [63:0]			data;
	reg [2:0]			empty;
	//------------Internal Wires-----------
	wire [31:0]			crc_out;
	wire [63:0]			data_in;
	wire [7:0]			xgmii_control;
	//-----------Instantiated Module-------
	//-----------Packet CRC calc module----
	crc	crc_rx(data_buf, crc_en, crc_out, empty, crc_rst, clk);
	//-----------IPG counter---------------
	//for data_in 10 Gbps, Minimum IPG 5 Bytes
	//For maxium throughput, should be able to start packet from 5th data_in
	//Will implement later if have time
	//-----------Initial Value-------------
	initial begin
		data = 0;
		sop = 0;
		eop = 0;
		valid = 0;
		empty = 0;
		crc_en = 0;
	end
	//-----------Next State----------------	
	//xgmii_control from LSB to MSB
	assign xgmii_control = {rx_xgmii[44], rx_xgmii[53], rx_xgmii[62], rx_xgmii[71], 
									rx_xgmii[8],rx_xgmii[17], rx_xgmii[26], rx_xgmii[35]};
	assign data_in[7:0]	 = rx_xgmii[43:36];
	assign data_in[15:8]  = rx_xgmii[52:45]; 
	assign data_in[23:16] = rx_xgmii[61:54];
	assign data_in[31:24] = rx_xgmii[70:63];
	assign data_in[39:32] = rx_xgmii[7:0];
	assign data_in[47:40] = rx_xgmii[16:9];
	assign data_in[55:48] = rx_xgmii[25:18];
	assign data_in[63:56] = rx_xgmii[34:27];
  
	always @ (state or rx_ready or ctrl_locked) begin: FSM_COMBO
		case(state)
			IDLE: begin
				if(ctrl_locked == 1 && rx_ready) begin
					next_state = START;
				end else begin
					next_state = IDLE;
				end
			end
			START: begin			//to wait for data buffered 1 clk cycle
				if(!rx_ready) begin
					next_state = IDLE;
				end else begin
					next_state = SOP;
				end
			end
			SOP: begin
				if(!rx_ready) begin
					next_state = IDLE;
				end else begin
					next_state = RECEIVE;
				end
			end
			RECEIVE: begin
				if(!rx_ready) begin
					next_state = IDLE;
				end else begin
					case(ctrl_locked)
						8'b00000000: next_state = RECEIVE;
						8'b10000000: next_state = EOP;
						8'b11000000: next_state = EOP;
						8'b11100000: next_state = EOP;
						8'b11110000: next_state = CRC;
						8'b11111000: next_state = CRC;
						8'b11111100: next_state = CRC;
						8'b11111110: next_state = CRC;
						8'b11111111: next_state = CRC;
						default: next_state = ERR;			//wired issue with data from solarflare
					endcase
				end
			end
			EOP: begin
				next_state = CRC;
			end
			CRC: begin
				next_state = IDLE;
			end
			ERR: begin
				next_state = IDLE;
			end
		endcase
	end

	//-----------Seq Logic----------------
	always @ (posedge clk or posedge rst) begin: FSM_SEQ
		if(rst == 1'b1) begin
			state <= IDLE;
		end else begin
			state <= next_state;
			data_locked <= data_in;
			data_buf <= data_locked;
			ctrl_locked <= xgmii_control;
			ctrl_buf <= ctrl_locked;
			rx_eop <= eop;
			rx_sop <= sop;
			rx_data <= data;
			rx_valid <= valid;
			rx_empty <= empty;
			crc_current <= crc_next;
		end
	end
	//--------FSM output logic----------------
	always @* begin: OUTPUT_LOGIC
		case(state) 
			IDLE: begin
				data = 0;
				eop = 0;
				sop = 0;
				valid = 0;
				empty = 0;
				crc_en = 0;
				crc_rst = 1;
				crc_next = 0;
			end
			START: begin
				data = 0;
				eop = 0;
				sop = 0;
				valid = 0;
				empty = 0;
				crc_en = 0;
				crc_rst = 0;
				crc_next = 0;
			end
			SOP: begin
				data = data_buf;
				eop = 0;
				sop = 1;
				valid = 1;
				empty = 0;
				crc_en = 1;
				crc_rst = 0;
				crc_next = 0;
			end
			RECEIVE: begin
				data = data_buf;
				sop = 0;
				valid = 1;
				crc_en = 1;
				crc_rst = 0;
				case(ctrl_locked)
					8'b11110000: begin
						eop = 1;
						empty = 0;
						crc_next = data_locked[63:32];
					end
					8'b11111000: begin
						eop = 1;
						empty = 1;
						crc_next = {data_buf[7:0], data_locked[63:40]};
					end
					8'b11111100: begin
						eop = 1;
						empty = 2;
						crc_next = {data_buf[15:0], data_locked[63:48]};
					end
					8'b11111110: begin
						eop = 1;
						empty = 3;
						crc_next = {data_buf[23:0], data_locked[63:56]};
					end
					8'b11111111: begin
						eop = 1;
						empty = 4;
						crc_next = data_buf[31:0];
					end
					default: begin
						eop = 0;
						empty = 0;
						crc_next = 0;
					end		
				endcase
			end
			EOP: begin
				eop = 1;
				sop = 0;
				valid = 1;
				data = data_buf;
				case(ctrl_buf)
					8'b10000000: begin
						empty = 5; 
						crc_next = data_buf[39:8];
					end
					8'b11000000: begin
						empty = 6; 
						crc_next = data_buf[47:16];
					end
					8'b11100000: begin
						empty = 7; 
						crc_next = data_buf[55:24];
					end
				endcase
				crc_en = 1;
				crc_rst = 0;
			end
			CRC: begin
				data = 0;
				eop = 0;
				sop = 0;
				valid = 0;
				empty = 0;
				crc_en = 0;
				crc_rst = 0;
				crc_next = 0;
				crc_flag = ({crc_out[7:0], crc_out[15:8], crc_out[23:16], crc_out[31:24]} == crc_current);
			end
			ERR: begin
				data = data_buf;
				eop = 1;
				sop = 0;
				valid = 1;
				empty = 0;
				crc_en = 0;
				crc_rst = 1;
				crc_next = 0;
			end
		endcase
	end
endmodule


