`timescale 1ns / 1ns
module test;
	reg reset = 1;
	reg clk = 1;
	reg [3:0] ptr = 0;
	reg [63:0] datain = 0;
	reg [63:0] data [0:12];
	reg en = 0;
	initial $readmemh ("out.txt",data);
	initial begin
		$dumpfile("mac_test.vcd");
		$dumpvars(0, test);
		# 10 reset = 0;
			 en = 1;
		# 150 
			 $stop;
	end
	always #5 begin
		clk = !clk;
	end
	always #10 begin
		datain = data[ptr];
		ptr = ptr + 1;
	end
	wire [31:0] crc_out;
	crc crc(datain, en, crc_out, reset, clk);
	initial 
		$monitor("@%t, datain= %h, crcout = %h", $time, datain, crc_out);
endmodule
