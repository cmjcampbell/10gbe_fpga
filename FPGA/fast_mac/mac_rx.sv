

module mac_rx(

						input logic clk, 
						input logic [71:0] xgmiidata,// input data
						input logic reset,
						input logic [31:0]c,//inputcrc
						
						output logic [63:0] data,//output data
						output logic [2:0] empty,
						output logic sop,
						output logic eop,
						output logic valid,
						output logic ready,
						output logic [31:0] newcrc,//outputcrc
						output logic checksum_err,
						output logic [31:0] cc); ///cc is for debugging, inverted
						
						
		logic [63:0] xgmii;
		logic [63:0] xgmiibuf;
		logic [31:0] newcrc8;
		logic [63:0]xgmiirev;
		logic [63:0]swapxgmiirev;		
		logic [7:0] xgmiicontrol;
		logic valid_flag;
		logic valid_flag_buff;
		logic sop_flag;
		logic sop_flag_buff;
		logic sop_flag_buff2;
		logic eop_flag=0;
		logic eop_flag_buff =0;
		logic crc_flag;
		logic checksum_buff =0;
		logic checksum_flag=0;
		logic [2:0] empty_flag;
	  
		logic [63:0]swapxgmiidata;
		
		logic[7:0] xgmiirev8;
		logic[15:0] xgmiirev16;
		logic[23:0] xgmiirev24;
		logic[31:0] xgmiirev32;
		logic[39:0] xgmiirev40;
		logic[47:0] xgmiirev48;
		logic[55:0] xgmiirev56;

		assign ready = 1;		
		
		assign xgmiicontrol = {xgmiidata[44], xgmiidata[53],xgmiidata[62], xgmiidata[71],xgmiidata[8], xgmiidata[17], xgmiidata[26], xgmiidata[35]};

		
		assign xgmiirev8=swapxgmiirev[63:56];
		assign xgmiirev16=swapxgmiirev[63:48];
		assign xgmiirev24=swapxgmiirev[63:40];
		assign xgmiirev32=swapxgmiirev[63:32];
		
		assign xgmiirev40=xgmiirev[63:24];
		assign xgmiirev48=xgmiirev[63:16];
		assign xgmiirev56=xgmiirev[63:8];
		
		assign swapxgmiidata[7:0] = xgmiidata[43:36];
		assign swapxgmiidata[15:8] =  xgmiidata[52:45]; 
		assign swapxgmiidata[23:16] = xgmiidata[61:54];
		assign swapxgmiidata[31:24] = xgmiidata[70:63];
		assign swapxgmiidata[39:32] = xgmiidata[7:0];
		assign swapxgmiidata[47:40] = xgmiidata[16:9];
		assign swapxgmiidata[55:48] = xgmiidata[25:18];
		assign swapxgmiidata[63:56] = xgmiidata[34:27];
		          
    integer i;
    integer g;
    integer k;
	 
    always@* begin
        for (i=0;i<8;i++)begin
          for(g=0;g<8;g++)begin
             xgmiirev[g+i*8]=swapxgmiidata[(7-g)+i*8]; 
			   swapxgmiirev[g+i*8]=swapxgmiidata[(7-g)+i*8]; 
          end
        end
    

        for (k=0;k<32;k++)begin
           cc[k] = ~newcrc[31-k];
        end
     end      
      
             
          
          
            
		always @(posedge clk) begin

						if (reset == 1)begin
								xgmii[63:0]<= 64'b0;
								sop_flag <= 0;
								sop_flag_buff <= 0;
								eop <= 0;
								sop <=0;
								valid_flag <= 0;
								crc_flag <=0;
								valid <= 0;
								newcrc<=32'hFFFFFFFF;
								valid_flag_buff <= 0;
								checksum_err <=0;
								empty <=3'b0;
						end 
						else begin
								empty <= empty_flag;
								eop_flag <= eop_flag_buff;
								///detect sop///
								if(xgmiicontrol == 1) begin 
											sop_flag<=1;
											crc_flag <= 1;
											
								end 
								if(sop_flag) begin
											sop_flag_buff<= 1;
											sop_flag<=0;
								end
								if(sop_flag_buff) begin
											sop_flag_buff<= 0;
											sop_flag_buff2<=1; 
								end
								if(sop_flag_buff2)begin
											sop <=1;
											valid <= 1;
											sop_flag_buff2 <=0;
								end
								else begin
											sop <= 0;
								end
								/////////////////////
								
								///when in middle of packet///
								data[63:0] <= xgmiibuf[63:0];
								xgmiibuf[63:0]<=xgmii[63:0];
								
								if(crc_flag)begin
									newcrc[0]   <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[16] ^ xgmiirev[12] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[0] ^ c[0] ^ c[2] ^ c[5] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
									newcrc[1]   <= xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
									newcrc[2]   <= xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[44] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[2] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[12] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[27];
									newcrc[3]   <= xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[45] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[1] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[13] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[28];
									newcrc[4]   <= xgmiirev[63] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[15] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[1] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[18] ^ c[25] ^ c[26] ^ c[27] ^ c[31];
									newcrc[5]   <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[14] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[27] ^ c[29] ^ c[31];
									newcrc[6]   <= xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[25] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[14] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[28] ^ c[30];
									newcrc[7]   <= xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[0] ^ c[2] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[28];
									newcrc[8]   <= xgmiirev[63] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[17] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
									newcrc[9]   <= xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[18] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[26] ^ c[28] ^ c[29];
									newcrc[10] <= xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[50] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[19] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[18] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
									newcrc[11] <= xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[20] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[1] ^ c[4] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[27];
									newcrc[12] <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[31];
									newcrc[13] <= xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[25] ^ xgmiirev[22] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[30];
									newcrc[14] <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[26] ^ xgmiirev[23] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[2] ^ c[0] ^ c[1] ^ c[11] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
									newcrc[15] <= xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[3] ^ c[1] ^ c[2] ^ c[12] ^ c[13] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
									newcrc[16] <= xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[51] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[8] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[0] ^ c[0] ^ c[3] ^ c[5] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[24] ^ c[25];
									newcrc[17] <= xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[1] ^ c[1] ^ c[4] ^ c[6] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[25] ^ c[26];
									newcrc[18] <= xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[46] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[2] ^ c[0] ^ c[2] ^ c[5] ^ c[7] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[21] ^ c[26] ^ c[27];
									newcrc[19] <= xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[35] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[3] ^ c[0] ^ c[1] ^ c[3] ^ c[6] ^ c[8] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[22] ^ c[27] ^ c[28];
									newcrc[20] <= xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[36] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[4] ^ c[1] ^ c[2] ^ c[4] ^ c[7] ^ c[9] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[28] ^ c[29];
									newcrc[21] <= xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[5] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[10] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[29] ^ c[30];
									newcrc[22] <= xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[20] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
									newcrc[23] <= xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[42] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[10] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
									newcrc[24] <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[43] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[11] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
									newcrc[25] <= xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[44] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[3] ^ xgmiirev[2] ^ c[1] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[24] ^ c[25] ^ c[26] ^ c[29] ^ c[30];
									newcrc[26] <= xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[10] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[0] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[12] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
									newcrc[27] <= xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[11] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[1] ^ c[0] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[16] ^ c[17] ^ c[18] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
									newcrc[28] <= xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[12] ^ xgmiirev[8] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[2] ^ c[1] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[17] ^ c[18] ^ c[19] ^ c[22] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
									newcrc[29] <= xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[3] ^ c[2] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[30] ^ c[31];
									newcrc[30] <= xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[48] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[4] ^ c[0] ^ c[3] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
								   newcrc[31] <= xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[5] ^ c[1] ^ c[4] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
								end


								///
								if(xgmiicontrol==8'h00)begin
									xgmii[7:0] <= xgmiidata[43:36];
									xgmii[15:8] <=  xgmiidata[52:45]; 
									xgmii[23:16] <= xgmiidata[61:54];
									xgmii[31:24] <= xgmiidata[70:63];
									xgmii[39:32] <= xgmiidata[7:0];
									xgmii[47:40] <= xgmiidata[16:9];
									xgmii[55:48] <= xgmiidata[25:18];
									xgmii[63:56] <= xgmiidata[34:27];
									xgmiibuf[63:0]<=xgmii[63:0];
								end								
								///end of packet detect through terminating FD
								//Buff data is eop
								else if (xgmiidata[34:27]==8'hFD) begin 
											xgmii[63:0] <= 0;
											eop_flag <= 1;
											empty<=4;
											empty_flag<=4;
											checksum_flag <=1;
											newcrc8<=c;
											crc_flag<=0;
								end
								else if (xgmiidata[25:18]==8'hFD) begin 
											xgmii[63:56]<=xgmiidata[34:27];
											xgmii[55:0]<=0;
											empty<=3;
											empty_flag<=3;
											eop_flag <= 1;
											newcrc8[0] 	<= xgmiirev8[6] ^ xgmiirev8[0] ^ c[24] ^ c[30];
											newcrc8[1] 	<= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[24] ^ c[25] ^ c[30] ^ c[31];
											newcrc8[2] 	<= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[24] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[3] 	<= xgmiirev8[7] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ c[25] ^ c[26] ^ c[27] ^ c[31];
											newcrc8[4] 	<= xgmiirev8[6] ^ xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ xgmiirev8[0] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[5] 	<= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[6] 	<= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[7] 	<= xgmiirev8[7] ^ xgmiirev8[5] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ xgmiirev8[0] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[8] 	<= xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[0] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
											newcrc8[9] 	<= xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ c[1] ^ c[25] ^ c[26] ^ c[28] ^ c[29];
											newcrc8[10] <= xgmiirev8[5] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ xgmiirev8[0] ^ c[2] ^ c[24] ^ c[26] ^ c[27] ^ c[29];
											newcrc8[11] <= xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[3] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
											newcrc8[12] <= xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[4] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[13] <= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ c[5] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[14] <= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[2] ^ c[6] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[15] <= xgmiirev8[7] ^ xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[3] ^ c[7] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[16] <= xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[0] ^ c[8] ^ c[24] ^ c[28] ^ c[29];
											newcrc8[17] <= xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[1] ^ c[9] ^ c[25] ^ c[29] ^ c[30];
											newcrc8[18] <= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[2] ^ c[10] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[19] <= xgmiirev8[7] ^ xgmiirev8[3] ^ c[11] ^ c[27] ^ c[31];
											newcrc8[20] <= xgmiirev8[4] ^ c[12] ^ c[28];
											newcrc8[21] <= xgmiirev8[5] ^ c[13] ^ c[29];
											newcrc8[22] <= xgmiirev8[0] ^ c[14] ^ c[24];
											newcrc8[23] <= xgmiirev8[6] ^ xgmiirev8[1] ^ xgmiirev8[0] ^ c[15] ^ c[24] ^ c[25] ^ c[30];
											newcrc8[24] <= xgmiirev8[7] ^ xgmiirev8[2] ^ xgmiirev8[1] ^ c[16] ^ c[25] ^ c[26] ^ c[31];
											newcrc8[25] <= xgmiirev8[3] ^ xgmiirev8[2] ^ c[17] ^ c[26] ^ c[27];
											newcrc8[26] <= xgmiirev8[6] ^ xgmiirev8[4] ^ xgmiirev8[3] ^ xgmiirev8[0] ^ c[18] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[27] <= xgmiirev8[7] ^ xgmiirev8[5] ^ xgmiirev8[4] ^ xgmiirev8[1] ^ c[19] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[28] <= xgmiirev8[6] ^ xgmiirev8[5] ^ xgmiirev8[2] ^ c[20] ^ c[26] ^ c[29] ^ c[30];
											newcrc8[29] <= xgmiirev8[7] ^ xgmiirev8[6] ^ xgmiirev8[3] ^ c[21] ^ c[27] ^ c[30] ^ c[31];
											newcrc8[30] <= xgmiirev8[7] ^ xgmiirev8[4] ^ c[22] ^ c[28] ^ c[31];
											newcrc8[31] <= xgmiirev8[5] ^ c[23] ^ c[29];
											checksum_flag<=1;
											crc_flag<=0;
								end 
								else if (xgmiidata[16:9]==8'hFD) begin 
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[47:0]<=0;
											empty<=2;
											empty_flag <= 2;
											eop_flag <= 1;	
											newcrc8[0]  <= xgmiirev16[12] ^ xgmiirev16[10] ^ xgmiirev16[9] ^ xgmiirev16[6] ^ xgmiirev16[0] ^ c[16] ^ c[22] ^ c[25] ^ c[26] ^ c[28];
											newcrc8[1]  <= xgmiirev16[13] ^ xgmiirev16[12] ^ xgmiirev16[11] ^ xgmiirev16[9] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[29];
											newcrc8[2]  <= xgmiirev16[14] ^ xgmiirev16[13] ^ xgmiirev16[9] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[29] ^ c[30];
											newcrc8[3]  <= xgmiirev16[15] ^ xgmiirev16[14] ^ xgmiirev16[10] ^ xgmiirev16[9] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ c[17] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[4]  <= xgmiirev16[15] ^ xgmiirev16[12] ^ xgmiirev16[11] ^ xgmiirev16[8] ^ xgmiirev16[6] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ xgmiirev16[0] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[5]  <= xgmiirev16[13] ^ xgmiirev16[10] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[29];
											newcrc8[6]  <= xgmiirev16[14] ^ xgmiirev16[11] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[30];
											newcrc8[7]  <= xgmiirev16[15] ^ xgmiirev16[10] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[5] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ xgmiirev16[0] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[31];
											newcrc8[8]  <= xgmiirev16[12] ^ xgmiirev16[11] ^ xgmiirev16[10] ^ xgmiirev16[8] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[24] ^ c[26] ^ c[27] ^ c[28];
											newcrc8[9]  <= xgmiirev16[13] ^ xgmiirev16[12] ^ xgmiirev16[11] ^ xgmiirev16[9] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[25] ^ c[27] ^ c[28] ^ c[29];
											newcrc8[10] <= xgmiirev16[14] ^ xgmiirev16[13] ^ xgmiirev16[9] ^ xgmiirev16[5] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ xgmiirev16[0] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[25] ^ c[29] ^ c[30];
											newcrc8[11] <= xgmiirev16[15] ^ xgmiirev16[14] ^ xgmiirev16[12] ^ xgmiirev16[9] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[25] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[12] <= xgmiirev16[15] ^ xgmiirev16[13] ^ xgmiirev16[12] ^ xgmiirev16[9] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[13] <= xgmiirev16[14] ^ xgmiirev16[13] ^ xgmiirev16[10] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[29] ^ c[30];
											newcrc8[14] <= xgmiirev16[15] ^ xgmiirev16[14] ^ xgmiirev16[11] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[30] ^ c[31];
											newcrc8[15] <= xgmiirev16[15] ^ xgmiirev16[12] ^ xgmiirev16[9] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[31];
											newcrc8[16] <= xgmiirev16[13] ^ xgmiirev16[12] ^ xgmiirev16[8] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[0] ^ c[0] ^ c[16] ^ c[20] ^ c[21] ^ c[24] ^ c[28] ^ c[29];
											newcrc8[17] <= xgmiirev16[14] ^ xgmiirev16[13] ^ xgmiirev16[9] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[1] ^ c[1] ^ c[17] ^ c[21] ^ c[22] ^ c[25] ^ c[29] ^ c[30];
											newcrc8[18] <= xgmiirev16[15] ^ xgmiirev16[14] ^ xgmiirev16[10] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[2] ^ c[2] ^ c[18] ^ c[22] ^ c[23] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[19] <= xgmiirev16[15] ^ xgmiirev16[11] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[3] ^ c[3] ^ c[19] ^ c[23] ^ c[24] ^ c[27] ^ c[31];
											newcrc8[20] <= xgmiirev16[12] ^ xgmiirev16[9] ^ xgmiirev16[8] ^ xgmiirev16[4] ^ c[4] ^ c[20] ^ c[24] ^ c[25] ^ c[28];
											newcrc8[21] <= xgmiirev16[13] ^ xgmiirev16[10] ^ xgmiirev16[9] ^ xgmiirev16[5] ^ c[5] ^ c[21] ^ c[25] ^ c[26] ^ c[29];
											newcrc8[22] <= xgmiirev16[14] ^ xgmiirev16[12] ^ xgmiirev16[11] ^ xgmiirev16[9] ^ xgmiirev16[0] ^ c[6] ^ c[16] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[23] <= xgmiirev16[15] ^ xgmiirev16[13] ^ xgmiirev16[9] ^ xgmiirev16[6] ^ xgmiirev16[1] ^ xgmiirev16[0] ^ c[7] ^ c[16] ^ c[17] ^ c[22] ^ c[25] ^ c[29] ^ c[31];
											newcrc8[24] <= xgmiirev16[14] ^ xgmiirev16[10] ^ xgmiirev16[7] ^ xgmiirev16[2] ^ xgmiirev16[1] ^ c[8] ^ c[17] ^ c[18] ^ c[23] ^ c[26] ^ c[30];
											newcrc8[25] <= xgmiirev16[15] ^ xgmiirev16[11] ^ xgmiirev16[8] ^ xgmiirev16[3] ^ xgmiirev16[2] ^ c[9] ^ c[18] ^ c[19] ^ c[24] ^ c[27] ^ c[31];
											newcrc8[26] <= xgmiirev16[10] ^ xgmiirev16[6] ^ xgmiirev16[4] ^ xgmiirev16[3] ^ xgmiirev16[0] ^ c[10] ^ c[16] ^ c[19] ^ c[20] ^ c[22] ^ c[26];
											newcrc8[27] <= xgmiirev16[11] ^ xgmiirev16[7] ^ xgmiirev16[5] ^ xgmiirev16[4] ^ xgmiirev16[1] ^ c[11] ^ c[17] ^ c[20] ^ c[21] ^ c[23] ^ c[27];
											newcrc8[28] <= xgmiirev16[12] ^ xgmiirev16[8] ^ xgmiirev16[6] ^ xgmiirev16[5] ^ xgmiirev16[2] ^ c[12] ^ c[18] ^ c[21] ^ c[22] ^ c[24] ^ c[28];
											newcrc8[29] <= xgmiirev16[13] ^ xgmiirev16[9] ^ xgmiirev16[7] ^ xgmiirev16[6] ^ xgmiirev16[3] ^ c[13] ^ c[19] ^ c[22] ^ c[23] ^ c[25] ^ c[29];
											newcrc8[30] <= xgmiirev16[14] ^ xgmiirev16[10] ^ xgmiirev16[8] ^ xgmiirev16[7] ^ xgmiirev16[4] ^ c[14] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[30];
											newcrc8[31] <= xgmiirev16[15] ^ xgmiirev16[11] ^ xgmiirev16[9] ^ xgmiirev16[8] ^ xgmiirev16[5] ^ c[15] ^ c[21] ^ c[24] ^ c[25] ^ c[27] ^ c[31];
											checksum_flag<=1;
											crc_flag<=0;
								end
								else if (xgmiidata[7:0]==8'hFD) begin 
											xgmii[47:40] <= xgmiidata[16:9];
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[39:0]<=0;
											empty<=1;
											empty_flag<=1;
											eop_flag <= 1;	
											newcrc8[0] 	<= xgmiirev24[16] ^ xgmiirev24[12] ^ xgmiirev24[10] ^ xgmiirev24[9] ^ xgmiirev24[6] ^ xgmiirev24[0] ^ c[8] ^ c[14] ^ c[17] ^ c[18] ^ c[20] ^ c[24];
											newcrc8[1] 	<= xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[13] ^ xgmiirev24[12] ^ xgmiirev24[11] ^ xgmiirev24[9] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[25];
											newcrc8[2] 	<= xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[14] ^ xgmiirev24[13] ^ xgmiirev24[9] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26];
											newcrc8[3] 	<= xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[15] ^ xgmiirev24[14] ^ xgmiirev24[10] ^ xgmiirev24[9] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ c[9] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27];
											newcrc8[4] 	<= xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[15] ^ xgmiirev24[12] ^ xgmiirev24[11] ^ xgmiirev24[8] ^ xgmiirev24[6] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ xgmiirev24[0] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[23] ^ c[26] ^ c[27] ^ c[28];
											newcrc8[5] 	<= xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[13] ^ xgmiirev24[10] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[21] ^ c[27] ^ c[28] ^ c[29];
											newcrc8[6] 	<= xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[14] ^ xgmiirev24[11] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[22] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[7] 	<= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[16] ^ xgmiirev24[15] ^ xgmiirev24[10] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[5] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ xgmiirev24[0] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[23] ^ c[24] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[8] 	<= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[17] ^ xgmiirev24[12] ^ xgmiirev24[11] ^ xgmiirev24[10] ^ xgmiirev24[8] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[25] ^ c[30] ^ c[31];
											newcrc8[9] 	<= xgmiirev24[23] ^ xgmiirev24[18] ^ xgmiirev24[13] ^ xgmiirev24[12] ^ xgmiirev24[11] ^ xgmiirev24[9] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[26] ^ c[31];
											newcrc8[10] <= xgmiirev24[19] ^ xgmiirev24[16] ^ xgmiirev24[14] ^ xgmiirev24[13] ^ xgmiirev24[9] ^ xgmiirev24[5] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ xgmiirev24[0] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[17] ^ c[21] ^ c[22] ^ c[24] ^ c[27];
											newcrc8[11] <= xgmiirev24[20] ^ xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[15] ^ xgmiirev24[14] ^ xgmiirev24[12] ^ xgmiirev24[9] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[17] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[28];
											newcrc8[12] <= xgmiirev24[21] ^ xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[15] ^ xgmiirev24[13] ^ xgmiirev24[12] ^ xgmiirev24[9] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[17] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[29];
											newcrc8[13] <= xgmiirev24[22] ^ xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[16] ^ xgmiirev24[14] ^ xgmiirev24[13] ^ xgmiirev24[10] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[30];
											newcrc8[14] <= xgmiirev24[23] ^ xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[17] ^ xgmiirev24[15] ^ xgmiirev24[14] ^ xgmiirev24[11] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[15] <= xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[18] ^ xgmiirev24[16] ^ xgmiirev24[15] ^ xgmiirev24[12] ^ xgmiirev24[9] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[29];
											newcrc8[16] <= xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[19] ^ xgmiirev24[17] ^ xgmiirev24[13] ^ xgmiirev24[12] ^ xgmiirev24[8] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[0] ^ c[8] ^ c[12] ^ c[13] ^ c[16] ^ c[20] ^ c[21] ^ c[25] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[17] <= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[20] ^ xgmiirev24[18] ^ xgmiirev24[14] ^ xgmiirev24[13] ^ xgmiirev24[9] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[1] ^ c[9] ^ c[13] ^ c[14] ^ c[17] ^ c[21] ^ c[22] ^ c[26] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[18] <= xgmiirev24[23] ^ xgmiirev24[21] ^ xgmiirev24[19] ^ xgmiirev24[15] ^ xgmiirev24[14] ^ xgmiirev24[10] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[2] ^ c[10] ^ c[14] ^ c[15] ^ c[18] ^ c[22] ^ c[23] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[19] <= xgmiirev24[22] ^ xgmiirev24[20] ^ xgmiirev24[16] ^ xgmiirev24[15] ^ xgmiirev24[11] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[3] ^ c[11] ^ c[15] ^ c[16] ^ c[19] ^ c[23] ^ c[24] ^ c[28] ^ c[30];
											newcrc8[20] <= xgmiirev24[23] ^ xgmiirev24[21] ^ xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[12] ^ xgmiirev24[9] ^ xgmiirev24[8] ^ xgmiirev24[4] ^ c[12] ^ c[16] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[29] ^ c[31];
											newcrc8[21] <= xgmiirev24[22] ^ xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[13] ^ xgmiirev24[10] ^ xgmiirev24[9] ^ xgmiirev24[5] ^ c[13] ^ c[17] ^ c[18] ^ c[21] ^ c[25] ^ c[26] ^ c[30];
											newcrc8[22] <= xgmiirev24[23] ^ xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[16] ^ xgmiirev24[14] ^ xgmiirev24[12] ^ xgmiirev24[11] ^ xgmiirev24[9] ^ xgmiirev24[0] ^ c[8] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[31];
											newcrc8[23] <= xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[15] ^ xgmiirev24[13] ^ xgmiirev24[9] ^ xgmiirev24[6] ^ xgmiirev24[1] ^ xgmiirev24[0] ^ c[8] ^ c[9] ^ c[14] ^ c[17] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
											newcrc8[24] <= xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[16] ^ xgmiirev24[14] ^ xgmiirev24[10] ^ xgmiirev24[7] ^ xgmiirev24[2] ^ xgmiirev24[1] ^ c[0] ^ c[9] ^ c[10] ^ c[15] ^ c[18] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29];
											newcrc8[25] <= xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[17] ^ xgmiirev24[15] ^ xgmiirev24[11] ^ xgmiirev24[8] ^ xgmiirev24[3] ^ xgmiirev24[2] ^ c[1] ^ c[10] ^ c[11] ^ c[16] ^ c[19] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[26] <= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[18] ^ xgmiirev24[10] ^ xgmiirev24[6] ^ xgmiirev24[4] ^ xgmiirev24[3] ^ xgmiirev24[0] ^ c[2] ^ c[8] ^ c[11] ^ c[12] ^ c[14] ^ c[18] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[27] <= xgmiirev24[23] ^ xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[19] ^ xgmiirev24[11] ^ xgmiirev24[7] ^ xgmiirev24[5] ^ xgmiirev24[4] ^ xgmiirev24[1] ^ c[3] ^ c[9] ^ c[12] ^ c[13] ^ c[15] ^ c[19] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[28] <= xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[20] ^ xgmiirev24[12] ^ xgmiirev24[8] ^ xgmiirev24[6] ^ xgmiirev24[5] ^ xgmiirev24[2] ^ c[4] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[20] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[29] <= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[21] ^ xgmiirev24[13] ^ xgmiirev24[9] ^ xgmiirev24[7] ^ xgmiirev24[6] ^ xgmiirev24[3] ^ c[5] ^ c[11] ^ c[14] ^ c[15] ^ c[17] ^ c[21] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[30] <= xgmiirev24[23] ^ xgmiirev24[22] ^ xgmiirev24[14] ^ xgmiirev24[10] ^ xgmiirev24[8] ^ xgmiirev24[7] ^ xgmiirev24[4] ^ c[6] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[22] ^ c[30] ^ c[31];
											newcrc8[31] <= xgmiirev24[23] ^ xgmiirev24[15] ^ xgmiirev24[11] ^ xgmiirev24[9] ^ xgmiirev24[8] ^ xgmiirev24[5] ^ c[7] ^ c[13] ^ c[16] ^ c[17] ^ c[19] ^ c[23] ^ c[31];
											checksum_flag<=1;
											crc_flag<=0;
								end
								else if (xgmiidata[70:63]==8'hFD) begin 
											xgmii[39:32] <= xgmiidata[7:0];
											xgmii[47:40] <= xgmiidata[16:9];
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[32:0]<=0;
											empty<=0;
											empty_flag<=0;
											eop_flag <= 1;	
											newcrc8[0]  <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[16] ^ xgmiirev32[12] ^ xgmiirev32[10] ^ xgmiirev32[9] ^ xgmiirev32[6] ^ xgmiirev32[0] ^ c[0] ^ c[6] ^ c[9] ^ c[10] ^ c[12] ^ c[16] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[1]  <= xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[24] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[13] ^ xgmiirev32[12] ^ xgmiirev32[11] ^ xgmiirev32[9] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[16] ^ c[17] ^ c[24] ^ c[27] ^ c[28];
											newcrc8[2]  <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[14] ^ xgmiirev32[13] ^ xgmiirev32[9] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[2] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[24] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[3]  <= xgmiirev32[31] ^ xgmiirev32[27] ^ xgmiirev32[25] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[15] ^ xgmiirev32[14] ^ xgmiirev32[10] ^ xgmiirev32[9] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ c[1] ^ c[2] ^ c[3] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[25] ^ c[27] ^ c[31];
											newcrc8[4]  <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[15] ^ xgmiirev32[12] ^ xgmiirev32[11] ^ xgmiirev32[8] ^ xgmiirev32[6] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ xgmiirev32[0] ^ c[0] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[11] ^ c[12] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[24] ^ c[25] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[5]  <= xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[24] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[13] ^ xgmiirev32[10] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[10] ^ c[13] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[28] ^ c[29];
											newcrc8[6]  <= xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[25] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[14] ^ xgmiirev32[11] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[11] ^ c[14] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[29] ^ c[30];
											newcrc8[7]  <= xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[16] ^ xgmiirev32[15] ^ xgmiirev32[10] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[5] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ xgmiirev32[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[15] ^ c[16] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29];
											newcrc8[8]  <= xgmiirev32[31] ^ xgmiirev32[28] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[17] ^ xgmiirev32[12] ^ xgmiirev32[11] ^ xgmiirev32[10] ^ xgmiirev32[8] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[17] ^ c[22] ^ c[23] ^ c[28] ^ c[31];
											newcrc8[9]  <= xgmiirev32[29] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[18] ^ xgmiirev32[13] ^ xgmiirev32[12] ^ xgmiirev32[11] ^ xgmiirev32[9] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[18] ^ c[23] ^ c[24] ^ c[29];
											newcrc8[10] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[26] ^ xgmiirev32[19] ^ xgmiirev32[16] ^ xgmiirev32[14] ^ xgmiirev32[13] ^ xgmiirev32[9] ^ xgmiirev32[5] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ xgmiirev32[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[9] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[11] <= xgmiirev32[31] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[20] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[15] ^ xgmiirev32[14] ^ xgmiirev32[12] ^ xgmiirev32[9] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[9] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[12] <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[27] ^ xgmiirev32[24] ^ xgmiirev32[21] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[15] ^ xgmiirev32[13] ^ xgmiirev32[12] ^ xgmiirev32[9] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[9] ^ c[12] ^ c[13] ^ c[15] ^ c[17] ^ c[18] ^ c[21] ^ c[24] ^ c[27] ^ c[30] ^ c[31];
											newcrc8[13] <= xgmiirev32[31] ^ xgmiirev32[28] ^ xgmiirev32[25] ^ xgmiirev32[22] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[16] ^ xgmiirev32[14] ^ xgmiirev32[13] ^ xgmiirev32[10] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[7] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[18] ^ c[19] ^ c[22] ^ c[25] ^ c[28] ^ c[31];
											newcrc8[14] <= xgmiirev32[29] ^ xgmiirev32[26] ^ xgmiirev32[23] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[17] ^ xgmiirev32[15] ^ xgmiirev32[14] ^ xgmiirev32[11] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[11] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[23] ^ c[26] ^ c[29];
											newcrc8[15] <= xgmiirev32[30] ^ xgmiirev32[27] ^ xgmiirev32[24] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[18] ^ xgmiirev32[16] ^ xgmiirev32[15] ^ xgmiirev32[12] ^ xgmiirev32[9] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[20] ^ c[21] ^ c[24] ^ c[27] ^ c[30];
											newcrc8[16] <= xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[19] ^ xgmiirev32[17] ^ xgmiirev32[13] ^ xgmiirev32[12] ^ xgmiirev32[8] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[0] ^ c[0] ^ c[4] ^ c[5] ^ c[8] ^ c[12] ^ c[13] ^ c[17] ^ c[19] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[29] ^ c[30];
											newcrc8[17] <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[27] ^ xgmiirev32[25] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[20] ^ xgmiirev32[18] ^ xgmiirev32[14] ^ xgmiirev32[13] ^ xgmiirev32[9] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[1] ^ c[1] ^ c[5] ^ c[6] ^ c[9] ^ c[13] ^ c[14] ^ c[18] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[30] ^ c[31];
											newcrc8[18] <= xgmiirev32[31] ^ xgmiirev32[28] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[21] ^ xgmiirev32[19] ^ xgmiirev32[15] ^ xgmiirev32[14] ^ xgmiirev32[10] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[2] ^ c[2] ^ c[6] ^ c[7] ^ c[10] ^ c[14] ^ c[15] ^ c[19] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[31];
											newcrc8[19] <= xgmiirev32[29] ^ xgmiirev32[27] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[22] ^ xgmiirev32[20] ^ xgmiirev32[16] ^ xgmiirev32[15] ^ xgmiirev32[11] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[3] ^ c[3] ^ c[7] ^ c[8] ^ c[11] ^ c[15] ^ c[16] ^ c[20] ^ c[22] ^ c[24] ^ c[25] ^ c[27] ^ c[29];
											newcrc8[20] <= xgmiirev32[30] ^ xgmiirev32[28] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[23] ^ xgmiirev32[21] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[12] ^ xgmiirev32[9] ^ xgmiirev32[8] ^ xgmiirev32[4] ^ c[4] ^ c[8] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[30];
											newcrc8[21] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[22] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[13] ^ xgmiirev32[10] ^ xgmiirev32[9] ^ xgmiirev32[5] ^ c[5] ^ c[9] ^ c[10] ^ c[13] ^ c[17] ^ c[18] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[22] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[16] ^ xgmiirev32[14] ^ xgmiirev32[12] ^ xgmiirev32[11] ^ xgmiirev32[9] ^ xgmiirev32[0] ^ c[0] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[16] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[23] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[15] ^ xgmiirev32[13] ^ xgmiirev32[9] ^ xgmiirev32[6] ^ xgmiirev32[1] ^ xgmiirev32[0] ^ c[0] ^ c[1] ^ c[6] ^ c[9] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[24] <= xgmiirev32[30] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[16] ^ xgmiirev32[14] ^ xgmiirev32[10] ^ xgmiirev32[7] ^ xgmiirev32[2] ^ xgmiirev32[1] ^ c[1] ^ c[2] ^ c[7] ^ c[10] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[25] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[17] ^ xgmiirev32[15] ^ xgmiirev32[11] ^ xgmiirev32[8] ^ xgmiirev32[3] ^ xgmiirev32[2] ^ c[2] ^ c[3] ^ c[8] ^ c[11] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[26] <= xgmiirev32[31] ^ xgmiirev32[28] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[18] ^ xgmiirev32[10] ^ xgmiirev32[6] ^ xgmiirev32[4] ^ xgmiirev32[3] ^ xgmiirev32[0] ^ c[0] ^ c[3] ^ c[4] ^ c[6] ^ c[10] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[31];
											newcrc8[27] <= xgmiirev32[29] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[19] ^ xgmiirev32[11] ^ xgmiirev32[7] ^ xgmiirev32[5] ^ xgmiirev32[4] ^ xgmiirev32[1] ^ c[1] ^ c[4] ^ c[5] ^ c[7] ^ c[11] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[29];
											newcrc8[28] <= xgmiirev32[30] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[20] ^ xgmiirev32[12] ^ xgmiirev32[8] ^ xgmiirev32[6] ^ xgmiirev32[5] ^ xgmiirev32[2] ^ c[2] ^ c[5] ^ c[6] ^ c[8] ^ c[12] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[29] <= xgmiirev32[31] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[25] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[21] ^ xgmiirev32[13] ^ xgmiirev32[9] ^ xgmiirev32[7] ^ xgmiirev32[6] ^ xgmiirev32[3] ^ c[3] ^ c[6] ^ c[7] ^ c[9] ^ c[13] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[30] <= xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[26] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[22] ^ xgmiirev32[14] ^ xgmiirev32[10] ^ xgmiirev32[8] ^ xgmiirev32[7] ^ xgmiirev32[4] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[14] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[31] <= xgmiirev32[31] ^ xgmiirev32[30] ^ xgmiirev32[29] ^ xgmiirev32[28] ^ xgmiirev32[27] ^ xgmiirev32[25] ^ xgmiirev32[24] ^ xgmiirev32[23] ^ xgmiirev32[15] ^ xgmiirev32[11] ^ xgmiirev32[9] ^ xgmiirev32[8] ^ xgmiirev32[5] ^ c[5] ^ c[8] ^ c[9] ^ c[11] ^ c[15] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											checksum_flag<=1;
											crc_flag<=0;
								end
								//
								//input data is eop
								else if (xgmiidata[61:54]==8'hFD) begin 
											xgmii[31:24] <= xgmiidata[70:63];
											xgmii[39:32] <= xgmiidata[7:0];
											xgmii[47:40] <= xgmiidata[16:9];
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[24:0]<=0;
											empty_flag<=7;
											eop_flag_buff <= 1;
		
											newcrc8[0]  <= xgmiirev40[37] ^ xgmiirev40[34] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[16] ^ xgmiirev40[12] ^ xgmiirev40[10] ^ xgmiirev40[9] ^ xgmiirev40[6] ^ xgmiirev40[0] ^ c[1] ^ c[2] ^ c[4] ^ c[8] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[29];
											newcrc8[1]  <= xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[33] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[24] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[13] ^ xgmiirev40[12] ^ xgmiirev40[11] ^ xgmiirev40[9] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[8] ^ c[9] ^ c[16] ^ c[19] ^ c[20] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[2]  <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[14] ^ xgmiirev40[13] ^ xgmiirev40[9] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[0] ^ c[1] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[16] ^ c[18] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[3]  <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[36] ^ xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[27] ^ xgmiirev40[25] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[15] ^ xgmiirev40[14] ^ xgmiirev40[10] ^ xgmiirev40[9] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ c[0] ^ c[1] ^ c[2] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[17] ^ c[19] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[4]  <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[33] ^ xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[15] ^ xgmiirev40[12] ^ xgmiirev40[11] ^ xgmiirev40[8] ^ xgmiirev40[6] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ xgmiirev40[0] ^ c[0] ^ c[3] ^ c[4] ^ c[7] ^ c[10] ^ c[11] ^ c[12] ^ c[16] ^ c[17] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[30] ^ c[31];
											newcrc8[5]  <= xgmiirev40[39] ^ xgmiirev40[37] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[24] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[13] ^ xgmiirev40[10] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[2] ^ c[5] ^ c[11] ^ c[12] ^ c[13] ^ c[16] ^ c[20] ^ c[21] ^ c[29] ^ c[31];
											newcrc8[6]  <= xgmiirev40[38] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[25] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[14] ^ xgmiirev40[11] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ c[0] ^ c[3] ^ c[6] ^ c[12] ^ c[13] ^ c[14] ^ c[17] ^ c[21] ^ c[22] ^ c[30];
											newcrc8[7]  <= xgmiirev40[39] ^ xgmiirev40[37] ^ xgmiirev40[34] ^ xgmiirev40[32] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[16] ^ xgmiirev40[15] ^ xgmiirev40[10] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[5] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ xgmiirev40[0] ^ c[0] ^ c[2] ^ c[7] ^ c[8] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[21] ^ c[24] ^ c[26] ^ c[29] ^ c[31];
											newcrc8[8]  <= xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[28] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[17] ^ xgmiirev40[12] ^ xgmiirev40[11] ^ xgmiirev40[10] ^ xgmiirev40[8] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[0] ^ c[2] ^ c[3] ^ c[4] ^ c[9] ^ c[14] ^ c[15] ^ c[20] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[9]  <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[29] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[18] ^ xgmiirev40[13] ^ xgmiirev40[12] ^ xgmiirev40[11] ^ xgmiirev40[9] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[10] ^ c[15] ^ c[16] ^ c[21] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[10] <= xgmiirev40[39] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[26] ^ xgmiirev40[19] ^ xgmiirev40[16] ^ xgmiirev40[14] ^ xgmiirev40[13] ^ xgmiirev40[9] ^ xgmiirev40[5] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ xgmiirev40[0] ^ c[1] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[18] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[11] <= xgmiirev40[36] ^ xgmiirev40[33] ^ xgmiirev40[31] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[20] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[15] ^ xgmiirev40[14] ^ xgmiirev40[12] ^ xgmiirev40[9] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[1] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[25] ^ c[28];
											newcrc8[12] <= xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[27] ^ xgmiirev40[24] ^ xgmiirev40[21] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[15] ^ xgmiirev40[13] ^ xgmiirev40[12] ^ xgmiirev40[9] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[1] ^ c[4] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[13] ^ c[16] ^ c[19] ^ c[22] ^ c[23];
											newcrc8[13] <= xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[28] ^ xgmiirev40[25] ^ xgmiirev40[22] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[16] ^ xgmiirev40[14] ^ xgmiirev40[13] ^ xgmiirev40[10] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ c[2] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[14] ^ c[17] ^ c[20] ^ c[23] ^ c[24];
											newcrc8[14] <= xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[29] ^ xgmiirev40[26] ^ xgmiirev40[23] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[17] ^ xgmiirev40[15] ^ xgmiirev40[14] ^ xgmiirev40[11] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ c[0] ^ c[3] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[12] ^ c[15] ^ c[18] ^ c[21] ^ c[24] ^ c[25];
											newcrc8[15] <= xgmiirev40[34] ^ xgmiirev40[33] ^ xgmiirev40[30] ^ xgmiirev40[27] ^ xgmiirev40[24] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[18] ^ xgmiirev40[16] ^ xgmiirev40[15] ^ xgmiirev40[12] ^ xgmiirev40[9] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ c[0] ^ c[1] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[12] ^ c[13] ^ c[16] ^ c[19] ^ c[22] ^ c[25] ^ c[26];
											newcrc8[16] <= xgmiirev40[37] ^ xgmiirev40[35] ^ xgmiirev40[32] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[19] ^ xgmiirev40[17] ^ xgmiirev40[13] ^ xgmiirev40[12] ^ xgmiirev40[8] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[0] ^ c[0] ^ c[4] ^ c[5] ^ c[9] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[18] ^ c[21] ^ c[22] ^ c[24] ^ c[27] ^ c[29];
											newcrc8[17] <= xgmiirev40[38] ^ xgmiirev40[36] ^ xgmiirev40[33] ^ xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[27] ^ xgmiirev40[25] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[20] ^ xgmiirev40[18] ^ xgmiirev40[14] ^ xgmiirev40[13] ^ xgmiirev40[9] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[1] ^ c[1] ^ c[5] ^ c[6] ^ c[10] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[22] ^ c[23] ^ c[25] ^ c[28] ^ c[30];
											newcrc8[18] <= xgmiirev40[39] ^ xgmiirev40[37] ^ xgmiirev40[34] ^ xgmiirev40[32] ^ xgmiirev40[31] ^ xgmiirev40[28] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[21] ^ xgmiirev40[19] ^ xgmiirev40[15] ^ xgmiirev40[14] ^ xgmiirev40[10] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[2] ^ c[2] ^ c[6] ^ c[7] ^ c[11] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[29] ^ c[31];
											newcrc8[19] <= xgmiirev40[38] ^ xgmiirev40[35] ^ xgmiirev40[33] ^ xgmiirev40[32] ^ xgmiirev40[29] ^ xgmiirev40[27] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[22] ^ xgmiirev40[20] ^ xgmiirev40[16] ^ xgmiirev40[15] ^ xgmiirev40[11] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[3] ^ c[0] ^ c[3] ^ c[7] ^ c[8] ^ c[12] ^ c[14] ^ c[16] ^ c[17] ^ c[19] ^ c[21] ^ c[24] ^ c[25] ^ c[27] ^ c[30];
											newcrc8[20] <= xgmiirev40[39] ^ xgmiirev40[36] ^ xgmiirev40[34] ^ xgmiirev40[33] ^ xgmiirev40[30] ^ xgmiirev40[28] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[23] ^ xgmiirev40[21] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[12] ^ xgmiirev40[9] ^ xgmiirev40[8] ^ xgmiirev40[4] ^ c[0] ^ c[1] ^ c[4] ^ c[8] ^ c[9] ^ c[13] ^ c[15] ^ c[17] ^ c[18] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[28] ^ c[31];
											newcrc8[21] <= xgmiirev40[37] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[22] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[13] ^ xgmiirev40[10] ^ xgmiirev40[9] ^ xgmiirev40[5] ^ c[1] ^ c[2] ^ c[5] ^ c[9] ^ c[10] ^ c[14] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[29];
											newcrc8[22] <= xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[16] ^ xgmiirev40[14] ^ xgmiirev40[12] ^ xgmiirev40[11] ^ xgmiirev40[9] ^ xgmiirev40[0] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[23] <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[34] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[15] ^ xgmiirev40[13] ^ xgmiirev40[9] ^ xgmiirev40[6] ^ xgmiirev40[1] ^ xgmiirev40[0] ^ c[1] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[24] <= xgmiirev40[39] ^ xgmiirev40[37] ^ xgmiirev40[36] ^ xgmiirev40[35] ^ xgmiirev40[32] ^ xgmiirev40[30] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[16] ^ xgmiirev40[14] ^ xgmiirev40[10] ^ xgmiirev40[7] ^ xgmiirev40[2] ^ xgmiirev40[1] ^ c[2] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[25] <= xgmiirev40[38] ^ xgmiirev40[37] ^ xgmiirev40[36] ^ xgmiirev40[33] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[17] ^ xgmiirev40[15] ^ xgmiirev40[11] ^ xgmiirev40[8] ^ xgmiirev40[3] ^ xgmiirev40[2] ^ c[0] ^ c[3] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[26] <= xgmiirev40[39] ^ xgmiirev40[38] ^ xgmiirev40[31] ^ xgmiirev40[28] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[18] ^ xgmiirev40[10] ^ xgmiirev40[6] ^ xgmiirev40[4] ^ xgmiirev40[3] ^ xgmiirev40[0] ^ c[2] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[23] ^ c[30] ^ c[31];
											newcrc8[27] <= xgmiirev40[39] ^ xgmiirev40[32] ^ xgmiirev40[29] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[19] ^ xgmiirev40[11] ^ xgmiirev40[7] ^ xgmiirev40[5] ^ xgmiirev40[4] ^ xgmiirev40[1] ^ c[3] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[31];
											newcrc8[28] <= xgmiirev40[33] ^ xgmiirev40[30] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[20] ^ xgmiirev40[12] ^ xgmiirev40[8] ^ xgmiirev40[6] ^ xgmiirev40[5] ^ xgmiirev40[2] ^ c[0] ^ c[4] ^ c[12] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[25];
											newcrc8[29] <= xgmiirev40[34] ^ xgmiirev40[31] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[25] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[21] ^ xgmiirev40[13] ^ xgmiirev40[9] ^ xgmiirev40[7] ^ xgmiirev40[6] ^ xgmiirev40[3] ^ c[1] ^ c[5] ^ c[13] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[26];
											newcrc8[30] <= xgmiirev40[35] ^ xgmiirev40[32] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[26] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[22] ^ xgmiirev40[14] ^ xgmiirev40[10] ^ xgmiirev40[8] ^ xgmiirev40[7] ^ xgmiirev40[4] ^ c[0] ^ c[2] ^ c[6] ^ c[14] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[27];
											newcrc8[31] <= xgmiirev40[36] ^ xgmiirev40[33] ^ xgmiirev40[31] ^ xgmiirev40[30] ^ xgmiirev40[29] ^ xgmiirev40[28] ^ xgmiirev40[27] ^ xgmiirev40[25] ^ xgmiirev40[24] ^ xgmiirev40[23] ^ xgmiirev40[15] ^ xgmiirev40[11] ^ xgmiirev40[9] ^ xgmiirev40[8] ^ xgmiirev40[5] ^ c[0] ^ c[1] ^ c[3] ^ c[7] ^ c[15] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[28];
												
											checksum_buff<=1;
								end
								else if (xgmiidata[52:45]==8'hFD) begin 
											xgmii[23:16] <= xgmiidata[61:54];
											xgmii[31:24] <= xgmiidata[70:63];
											xgmii[39:32] <= xgmiidata[7:0];
											xgmii[47:40] <= xgmiidata[16:9];
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[16:0]<=0;
											empty_flag<=6;
											eop_flag_buff <= 1;

											newcrc8[0]  <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[37] ^ xgmiirev48[34] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[16] ^ xgmiirev48[12] ^ xgmiirev48[10] ^ xgmiirev48[9] ^ xgmiirev48[6] ^ xgmiirev48[0] ^ c[0] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[18] ^ c[21] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[1]  <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[33] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[24] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[13] ^ xgmiirev48[12] ^ xgmiirev48[11] ^ xgmiirev48[9] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[0] ^ c[1] ^ c[8] ^ c[11] ^ c[12] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[2]  <= xgmiirev48[44] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[14] ^ xgmiirev48[13] ^ xgmiirev48[9] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[0] ^ c[1] ^ c[2] ^ c[8] ^ c[10] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[28];
											newcrc8[3]  <= xgmiirev48[45] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[36] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[27] ^ xgmiirev48[25] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[15] ^ xgmiirev48[14] ^ xgmiirev48[10] ^ xgmiirev48[9] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ c[1] ^ c[2] ^ c[3] ^ c[9] ^ c[11] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[29];
											newcrc8[4]  <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[33] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[15] ^ xgmiirev48[12] ^ xgmiirev48[11] ^ xgmiirev48[8] ^ xgmiirev48[6] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ xgmiirev48[0] ^ c[2] ^ c[3] ^ c[4] ^ c[8] ^ c[9] ^ c[13] ^ c[14] ^ c[15] ^ c[17] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[5]  <= xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[37] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[24] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[13] ^ xgmiirev48[10] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[3] ^ c[4] ^ c[5] ^ c[8] ^ c[12] ^ c[13] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[30];
											newcrc8[6]  <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[38] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[25] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[14] ^ xgmiirev48[11] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ c[4] ^ c[5] ^ c[6] ^ c[9] ^ c[13] ^ c[14] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc8[7]  <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[45] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[39] ^ xgmiirev48[37] ^ xgmiirev48[34] ^ xgmiirev48[32] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[16] ^ xgmiirev48[15] ^ xgmiirev48[10] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[5] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ xgmiirev48[0] ^ c[0] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[16] ^ c[18] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[8]  <= xgmiirev48[46] ^ xgmiirev48[45] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[40] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[28] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[17] ^ xgmiirev48[12] ^ xgmiirev48[11] ^ xgmiirev48[10] ^ xgmiirev48[8] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[1] ^ c[6] ^ c[7] ^ c[12] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[9]  <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[41] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[29] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[18] ^ xgmiirev48[13] ^ xgmiirev48[12] ^ xgmiirev48[11] ^ xgmiirev48[9] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ c[2] ^ c[7] ^ c[8] ^ c[13] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[10] <= xgmiirev48[42] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[26] ^ xgmiirev48[19] ^ xgmiirev48[16] ^ xgmiirev48[14] ^ xgmiirev48[13] ^ xgmiirev48[9] ^ xgmiirev48[5] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ xgmiirev48[0] ^ c[0] ^ c[3] ^ c[10] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[23] ^ c[24] ^ c[26];
											newcrc8[11] <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[36] ^ xgmiirev48[33] ^ xgmiirev48[31] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[20] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[15] ^ xgmiirev48[14] ^ xgmiirev48[12] ^ xgmiirev48[9] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[0] ^ c[1] ^ c[4] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[15] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[12] <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[27] ^ xgmiirev48[24] ^ xgmiirev48[21] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[15] ^ xgmiirev48[13] ^ xgmiirev48[12] ^ xgmiirev48[9] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[1] ^ c[2] ^ c[5] ^ c[8] ^ c[11] ^ c[14] ^ c[15] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[13] <= xgmiirev48[47] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[28] ^ xgmiirev48[25] ^ xgmiirev48[22] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[16] ^ xgmiirev48[14] ^ xgmiirev48[13] ^ xgmiirev48[10] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ c[0] ^ c[2] ^ c[3] ^ c[6] ^ c[9] ^ c[12] ^ c[15] ^ c[16] ^ c[26] ^ c[27] ^ c[31];
											newcrc8[14] <= xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[29] ^ xgmiirev48[26] ^ xgmiirev48[23] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[17] ^ xgmiirev48[15] ^ xgmiirev48[14] ^ xgmiirev48[11] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ c[1] ^ c[3] ^ c[4] ^ c[7] ^ c[10] ^ c[13] ^ c[16] ^ c[17] ^ c[27] ^ c[28];
											newcrc8[15] <= xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[34] ^ xgmiirev48[33] ^ xgmiirev48[30] ^ xgmiirev48[27] ^ xgmiirev48[24] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[18] ^ xgmiirev48[16] ^ xgmiirev48[15] ^ xgmiirev48[12] ^ xgmiirev48[9] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ c[0] ^ c[2] ^ c[4] ^ c[5] ^ c[8] ^ c[11] ^ c[14] ^ c[17] ^ c[18] ^ c[28] ^ c[29];
											newcrc8[16] <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[37] ^ xgmiirev48[35] ^ xgmiirev48[32] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[19] ^ xgmiirev48[17] ^ xgmiirev48[13] ^ xgmiirev48[12] ^ xgmiirev48[8] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[0] ^ c[1] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[21] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[17] <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[38] ^ xgmiirev48[36] ^ xgmiirev48[33] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[27] ^ xgmiirev48[25] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[20] ^ xgmiirev48[18] ^ xgmiirev48[14] ^ xgmiirev48[13] ^ xgmiirev48[9] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[1] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[14] ^ c[15] ^ c[17] ^ c[20] ^ c[22] ^ c[29] ^ c[31];
											newcrc8[18] <= xgmiirev48[46] ^ xgmiirev48[39] ^ xgmiirev48[37] ^ xgmiirev48[34] ^ xgmiirev48[32] ^ xgmiirev48[31] ^ xgmiirev48[28] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[21] ^ xgmiirev48[19] ^ xgmiirev48[15] ^ xgmiirev48[14] ^ xgmiirev48[10] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[2] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[21] ^ c[23] ^ c[30];
											newcrc8[19] <= xgmiirev48[47] ^ xgmiirev48[40] ^ xgmiirev48[38] ^ xgmiirev48[35] ^ xgmiirev48[33] ^ xgmiirev48[32] ^ xgmiirev48[29] ^ xgmiirev48[27] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[22] ^ xgmiirev48[20] ^ xgmiirev48[16] ^ xgmiirev48[15] ^ xgmiirev48[11] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[3] ^ c[0] ^ c[4] ^ c[6] ^ c[8] ^ c[9] ^ c[11] ^ c[13] ^ c[16] ^ c[17] ^ c[19] ^ c[22] ^ c[24] ^ c[31];
											newcrc8[20] <= xgmiirev48[41] ^ xgmiirev48[39] ^ xgmiirev48[36] ^ xgmiirev48[34] ^ xgmiirev48[33] ^ xgmiirev48[30] ^ xgmiirev48[28] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[23] ^ xgmiirev48[21] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[12] ^ xgmiirev48[9] ^ xgmiirev48[8] ^ xgmiirev48[4] ^ c[0] ^ c[1] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[12] ^ c[14] ^ c[17] ^ c[18] ^ c[20] ^ c[23] ^ c[25];
											newcrc8[21] <= xgmiirev48[42] ^ xgmiirev48[40] ^ xgmiirev48[37] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[22] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[13] ^ xgmiirev48[10] ^ xgmiirev48[9] ^ xgmiirev48[5] ^ c[1] ^ c[2] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[26];
											newcrc8[22] <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[41] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[16] ^ xgmiirev48[14] ^ xgmiirev48[12] ^ xgmiirev48[11] ^ xgmiirev48[9] ^ xgmiirev48[0] ^ c[0] ^ c[2] ^ c[3] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[23] <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[42] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[34] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[15] ^ xgmiirev48[13] ^ xgmiirev48[9] ^ xgmiirev48[6] ^ xgmiirev48[1] ^ xgmiirev48[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[24] <= xgmiirev48[47] ^ xgmiirev48[43] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[37] ^ xgmiirev48[36] ^ xgmiirev48[35] ^ xgmiirev48[32] ^ xgmiirev48[30] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[16] ^ xgmiirev48[14] ^ xgmiirev48[10] ^ xgmiirev48[7] ^ xgmiirev48[2] ^ xgmiirev48[1] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[11] ^ c[12] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[27] ^ c[31];
											newcrc8[25] <= xgmiirev48[44] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[38] ^ xgmiirev48[37] ^ xgmiirev48[36] ^ xgmiirev48[33] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[17] ^ xgmiirev48[15] ^ xgmiirev48[11] ^ xgmiirev48[8] ^ xgmiirev48[3] ^ xgmiirev48[2] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[12] ^ c[13] ^ c[15] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[28];
											newcrc8[26] <= xgmiirev48[47] ^ xgmiirev48[44] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[39] ^ xgmiirev48[38] ^ xgmiirev48[31] ^ xgmiirev48[28] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[18] ^ xgmiirev48[10] ^ xgmiirev48[6] ^ xgmiirev48[4] ^ xgmiirev48[3] ^ xgmiirev48[0] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[15] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[31];
											newcrc8[27] <= xgmiirev48[45] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[40] ^ xgmiirev48[39] ^ xgmiirev48[32] ^ xgmiirev48[29] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[19] ^ xgmiirev48[11] ^ xgmiirev48[7] ^ xgmiirev48[5] ^ xgmiirev48[4] ^ xgmiirev48[1] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[16] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[29];
											newcrc8[28] <= xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[41] ^ xgmiirev48[40] ^ xgmiirev48[33] ^ xgmiirev48[30] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[20] ^ xgmiirev48[12] ^ xgmiirev48[8] ^ xgmiirev48[6] ^ xgmiirev48[5] ^ xgmiirev48[2] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[17] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[29] <= xgmiirev48[47] ^ xgmiirev48[45] ^ xgmiirev48[44] ^ xgmiirev48[42] ^ xgmiirev48[41] ^ xgmiirev48[34] ^ xgmiirev48[31] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[25] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[21] ^ xgmiirev48[13] ^ xgmiirev48[9] ^ xgmiirev48[7] ^ xgmiirev48[6] ^ xgmiirev48[3] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[18] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[30] <= xgmiirev48[46] ^ xgmiirev48[45] ^ xgmiirev48[43] ^ xgmiirev48[42] ^ xgmiirev48[35] ^ xgmiirev48[32] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[26] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[22] ^ xgmiirev48[14] ^ xgmiirev48[10] ^ xgmiirev48[8] ^ xgmiirev48[7] ^ xgmiirev48[4] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[26] ^ c[27] ^ c[29] ^ c[30];
											newcrc8[31] <= xgmiirev48[47] ^ xgmiirev48[46] ^ xgmiirev48[44] ^ xgmiirev48[43] ^ xgmiirev48[36] ^ xgmiirev48[33] ^ xgmiirev48[31] ^ xgmiirev48[30] ^ xgmiirev48[29] ^ xgmiirev48[28] ^ xgmiirev48[27] ^ xgmiirev48[25] ^ xgmiirev48[24] ^ xgmiirev48[23] ^ xgmiirev48[15] ^ xgmiirev48[11] ^ xgmiirev48[9] ^ xgmiirev48[8] ^ xgmiirev48[5] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[17] ^ c[20] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
												
											checksum_buff<=1;
								end
								else if (xgmiidata[43:36]==8'hFD) begin 
											xgmii[15:8] <=  xgmiidata[52:45]; 
											xgmii[23:16] <= xgmiidata[61:54];
											xgmii[31:24] <= xgmiidata[70:63];
											xgmii[39:32] <= xgmiidata[7:0];
											xgmii[47:40] <= xgmiidata[16:9];
											xgmii[55:48] <= xgmiidata[25:18];
											xgmii[63:56] <= xgmiidata[34:27];
											xgmii[7:0]<=0;
											empty_flag<=5;
											eop_flag_buff <= 1;

											newcrc8[0]  <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[37] ^ xgmiirev56[34] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[16] ^ xgmiirev56[12] ^ xgmiirev56[10] ^ xgmiirev56[9] ^ xgmiirev56[6] ^ xgmiirev56[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[13] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[1]  <= xgmiirev56[53] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[33] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[24] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[13] ^ xgmiirev56[12] ^ xgmiirev56[11] ^ xgmiirev56[9] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[0] ^ c[3] ^ c[4] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[29];
											newcrc8[2]  <= xgmiirev56[55] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[44] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[14] ^ xgmiirev56[13] ^ xgmiirev56[9] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[0] ^ c[2] ^ c[6] ^ c[7] ^ c[8] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[20] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[3]  <= xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[45] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[36] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[27] ^ xgmiirev56[25] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[15] ^ xgmiirev56[14] ^ xgmiirev56[10] ^ xgmiirev56[9] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ c[1] ^ c[3] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[21] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[4]  <= xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[33] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[15] ^ xgmiirev56[12] ^ xgmiirev56[11] ^ xgmiirev56[8] ^ xgmiirev56[6] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ xgmiirev56[0] ^ c[0] ^ c[1] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26];
											newcrc8[5]  <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[37] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[24] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[13] ^ xgmiirev56[10] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[0] ^ c[4] ^ c[5] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[6]  <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[38] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[25] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[14] ^ xgmiirev56[11] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ c[1] ^ c[5] ^ c[6] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[7]  <= xgmiirev56[54] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[45] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[39] ^ xgmiirev56[37] ^ xgmiirev56[34] ^ xgmiirev56[32] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[16] ^ xgmiirev56[15] ^ xgmiirev56[10] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[5] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ xgmiirev56[0] ^ c[0] ^ c[1] ^ c[4] ^ c[5] ^ c[8] ^ c[10] ^ c[13] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[8]  <= xgmiirev56[54] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[46] ^ xgmiirev56[45] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[40] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[28] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[17] ^ xgmiirev56[12] ^ xgmiirev56[11] ^ xgmiirev56[10] ^ xgmiirev56[8] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[4] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
											newcrc8[9]  <= xgmiirev56[55] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[41] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[29] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[18] ^ xgmiirev56[13] ^ xgmiirev56[12] ^ xgmiirev56[11] ^ xgmiirev56[9] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ c[0] ^ c[5] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
											newcrc8[10] <= xgmiirev56[55] ^ xgmiirev56[52] ^ xgmiirev56[50] ^ xgmiirev56[42] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[26] ^ xgmiirev56[19] ^ xgmiirev56[16] ^ xgmiirev56[14] ^ xgmiirev56[13] ^ xgmiirev56[9] ^ xgmiirev56[5] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ xgmiirev56[0] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[26] ^ c[28] ^ c[31];
											newcrc8[11] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[36] ^ xgmiirev56[33] ^ xgmiirev56[31] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[20] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[15] ^ xgmiirev56[14] ^ xgmiirev56[12] ^ xgmiirev56[9] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[7] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[30] ^ c[31];
											newcrc8[12] <= xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[27] ^ xgmiirev56[24] ^ xgmiirev56[21] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[15] ^ xgmiirev56[13] ^ xgmiirev56[12] ^ xgmiirev56[9] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[0] ^ c[3] ^ c[6] ^ c[7] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
											newcrc8[13] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[28] ^ xgmiirev56[25] ^ xgmiirev56[22] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[16] ^ xgmiirev56[14] ^ xgmiirev56[13] ^ xgmiirev56[10] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ c[1] ^ c[4] ^ c[7] ^ c[8] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[14] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[29] ^ xgmiirev56[26] ^ xgmiirev56[23] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[17] ^ xgmiirev56[15] ^ xgmiirev56[14] ^ xgmiirev56[11] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ c[2] ^ c[5] ^ c[8] ^ c[9] ^ c[19] ^ c[20] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[15] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[34] ^ xgmiirev56[33] ^ xgmiirev56[30] ^ xgmiirev56[27] ^ xgmiirev56[24] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[18] ^ xgmiirev56[16] ^ xgmiirev56[15] ^ xgmiirev56[12] ^ xgmiirev56[9] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ c[0] ^ c[3] ^ c[6] ^ c[9] ^ c[10] ^ c[20] ^ c[21] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc8[16] <= xgmiirev56[51] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[37] ^ xgmiirev56[35] ^ xgmiirev56[32] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[19] ^ xgmiirev56[17] ^ xgmiirev56[13] ^ xgmiirev56[12] ^ xgmiirev56[8] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[0] ^ c[0] ^ c[2] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[13] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[27];
											newcrc8[17] <= xgmiirev56[52] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[38] ^ xgmiirev56[36] ^ xgmiirev56[33] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[27] ^ xgmiirev56[25] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[20] ^ xgmiirev56[18] ^ xgmiirev56[14] ^ xgmiirev56[13] ^ xgmiirev56[9] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[1] ^ c[1] ^ c[3] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[14] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[28];
											newcrc8[18] <= xgmiirev56[53] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[46] ^ xgmiirev56[39] ^ xgmiirev56[37] ^ xgmiirev56[34] ^ xgmiirev56[32] ^ xgmiirev56[31] ^ xgmiirev56[28] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[21] ^ xgmiirev56[19] ^ xgmiirev56[15] ^ xgmiirev56[14] ^ xgmiirev56[10] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[2] ^ c[0] ^ c[2] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[13] ^ c[15] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[29];
											newcrc8[19] <= xgmiirev56[54] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[47] ^ xgmiirev56[40] ^ xgmiirev56[38] ^ xgmiirev56[35] ^ xgmiirev56[33] ^ xgmiirev56[32] ^ xgmiirev56[29] ^ xgmiirev56[27] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[22] ^ xgmiirev56[20] ^ xgmiirev56[16] ^ xgmiirev56[15] ^ xgmiirev56[11] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[3] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[8] ^ c[9] ^ c[11] ^ c[14] ^ c[16] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
											newcrc8[20] <= xgmiirev56[55] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[41] ^ xgmiirev56[39] ^ xgmiirev56[36] ^ xgmiirev56[34] ^ xgmiirev56[33] ^ xgmiirev56[30] ^ xgmiirev56[28] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[23] ^ xgmiirev56[21] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[12] ^ xgmiirev56[9] ^ xgmiirev56[8] ^ xgmiirev56[4] ^ c[1] ^ c[2] ^ c[4] ^ c[6] ^ c[9] ^ c[10] ^ c[12] ^ c[15] ^ c[17] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[21] <= xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[49] ^ xgmiirev56[42] ^ xgmiirev56[40] ^ xgmiirev56[37] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[22] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[13] ^ xgmiirev56[10] ^ xgmiirev56[9] ^ xgmiirev56[5] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[13] ^ c[16] ^ c[18] ^ c[25] ^ c[27] ^ c[28] ^ c[29];
											newcrc8[22] <= xgmiirev56[55] ^ xgmiirev56[52] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[41] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[16] ^ xgmiirev56[14] ^ xgmiirev56[12] ^ xgmiirev56[11] ^ xgmiirev56[9] ^ xgmiirev56[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[28] ^ c[31];
											newcrc8[23] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[42] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[34] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[15] ^ xgmiirev56[13] ^ xgmiirev56[9] ^ xgmiirev56[6] ^ xgmiirev56[1] ^ xgmiirev56[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
											newcrc8[24] <= xgmiirev56[55] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[43] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[37] ^ xgmiirev56[36] ^ xgmiirev56[35] ^ xgmiirev56[32] ^ xgmiirev56[30] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[16] ^ xgmiirev56[14] ^ xgmiirev56[10] ^ xgmiirev56[7] ^ xgmiirev56[2] ^ xgmiirev56[1] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[19] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[31];
											newcrc8[25] <= xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[44] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[38] ^ xgmiirev56[37] ^ xgmiirev56[36] ^ xgmiirev56[33] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[17] ^ xgmiirev56[15] ^ xgmiirev56[11] ^ xgmiirev56[8] ^ xgmiirev56[3] ^ xgmiirev56[2] ^ c[4] ^ c[5] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
											newcrc8[26] <= xgmiirev56[55] ^ xgmiirev56[54] ^ xgmiirev56[52] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[47] ^ xgmiirev56[44] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[39] ^ xgmiirev56[38] ^ xgmiirev56[31] ^ xgmiirev56[28] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[18] ^ xgmiirev56[10] ^ xgmiirev56[6] ^ xgmiirev56[4] ^ xgmiirev56[3] ^ xgmiirev56[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[7] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[20] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[30] ^ c[31];
											newcrc8[27] <= xgmiirev56[55] ^ xgmiirev56[53] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[48] ^ xgmiirev56[45] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[40] ^ xgmiirev56[39] ^ xgmiirev56[32] ^ xgmiirev56[29] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[19] ^ xgmiirev56[11] ^ xgmiirev56[7] ^ xgmiirev56[5] ^ xgmiirev56[4] ^ xgmiirev56[1] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[25] ^ c[26] ^ c[29] ^ c[31];
											newcrc8[28] <= xgmiirev56[54] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[49] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[41] ^ xgmiirev56[40] ^ xgmiirev56[33] ^ xgmiirev56[30] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[20] ^ xgmiirev56[12] ^ xgmiirev56[8] ^ xgmiirev56[6] ^ xgmiirev56[5] ^ xgmiirev56[2] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[9] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
											newcrc8[29] <= xgmiirev56[55] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[50] ^ xgmiirev56[47] ^ xgmiirev56[45] ^ xgmiirev56[44] ^ xgmiirev56[42] ^ xgmiirev56[41] ^ xgmiirev56[34] ^ xgmiirev56[31] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[25] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[21] ^ xgmiirev56[13] ^ xgmiirev56[9] ^ xgmiirev56[7] ^ xgmiirev56[6] ^ xgmiirev56[3] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[10] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
											newcrc8[30] <= xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[51] ^ xgmiirev56[48] ^ xgmiirev56[46] ^ xgmiirev56[45] ^ xgmiirev56[43] ^ xgmiirev56[42] ^ xgmiirev56[35] ^ xgmiirev56[32] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[26] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[22] ^ xgmiirev56[14] ^ xgmiirev56[10] ^ xgmiirev56[8] ^ xgmiirev56[7] ^ xgmiirev56[4] ^ c[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[24] ^ c[27] ^ c[28] ^ c[29];
											newcrc8[31] <= xgmiirev56[54] ^ xgmiirev56[53] ^ xgmiirev56[52] ^ xgmiirev56[49] ^ xgmiirev56[47] ^ xgmiirev56[46] ^ xgmiirev56[44] ^ xgmiirev56[43] ^ xgmiirev56[36] ^ xgmiirev56[33] ^ xgmiirev56[31] ^ xgmiirev56[30] ^ xgmiirev56[29] ^ xgmiirev56[28] ^ xgmiirev56[27] ^ xgmiirev56[25] ^ xgmiirev56[24] ^ xgmiirev56[23] ^ xgmiirev56[15] ^ xgmiirev56[11] ^ xgmiirev56[9] ^ xgmiirev56[8] ^ xgmiirev56[5] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[28] ^ c[29] ^ c[30];
												
											checksum_buff<=1;
								end
								//
								else if(eop_flag==0 && eop_flag_buff==0) begin
											eop <= 0;
											valid <=0;
											empty   <=0;
											data    <= 63'b0;
							            newcrc  <=32'hFFFFFFFF;
											newcrc8 <= 32'hFFFFFFFF;
								end
								if(eop_flag)begin
											eop<=1;
											eop_flag<=0;
											eop_flag_buff<=0;
											valid_flag<=0;
											valid_flag_buff<=0;	
								end
								///
								if(checksum_flag==1)begin
											checksum_flag<=0;
											checksum_err <= !(newcrc8==32'hC704DD7B);
								end
								else begin
										checksum_err <=0;
								end
								if(checksum_buff != 0)begin
									checksum_flag <= 1;
									checksum_buff<=0;
								end
						end
			end
endmodule


