

module mac_tx(

						input logic clk, 
						input logic [63:0] stdata,// input data
						input logic reset,
						input logic [31:0]c,
						input logic sopin,
						input logic eopin,
						input logic valid,
						input logic [2:0] empty,
						
						output logic ready,
						output logic [63:0]xgmiirev,	//reverse endian for debuggin			
						output logic [71:0] xgmii,
						output logic [31:0] newcrc,
						output logic [31:0] cc); /////cc is for debugging, inverted


		logic [63:0]swapxgmiidata;
		logic eop1;
		logic eop2;
		logic eop3;		
		logic valid1;
		logic [63:0] stdata1;
		logic sop;
		logic eop;
		

		assign sop = sopin && valid;
		assign eop = eopin && valid;
		assign ready = 1;
		
		always @(posedge clk)
			begin
				eop1 <=eop;
				eop2 <=eop1;
				eop3 <=eop2;
				valid1 <= valid;
				stdata1 <= stdata;
			end
		          
    integer i;
    integer g;
    integer k;
	 
	 initial begin
		newcrc<=32'hFFFFFFFF;
		xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
	end
	 
	 
    always@* begin
        for (i=0;i<8;i++)begin
          for(g=0;g<8;g++)begin
             xgmiirev[g+i*8]=stdata[(7-g)+i*8]; 
          end
        end
    

        for (k=0;k<32;k++)begin
           cc[k] = ~newcrc[31-k];
        end
     end      
      
        
		always @(posedge clk) begin

						if (reset == 1)begin
								newcrc<=32'hFFFFFFFF;
						end else if (sop)
							begin
									xgmii <= {1'b0, 8'h55,1'b0, 8'h55,1'b0, 8'h55,1'b0, 8'hD5,1'b1, 8'hFB,1'b0, 8'h55,1'b0, 8'h55, 1'b0, 8'h55};
							end else if (valid1) //middle cases
								begin
									xgmii <= {1'b0,  stdata1[31:24], 1'b0, stdata1[23:16], 1'b0, stdata1[15:8], 1'b0, stdata1[7:0], 1'b0, stdata1[63:56], 1'b0, stdata1[55:48], 1'b0, stdata1[47:40], 1'b0, stdata1[39:32]};	
								end else if (eop2)
									begin
										xgmii <= { 1'b1, 8'hFD,1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,1'b0, cc[7:0], 1'b0, cc[15:8], 1'b0, cc[23:16], 1'b0, cc[31:24]};
									end else if ( eop3)
										begin
											xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
											newcrc <=32'hFFFFFFFF;
											end else 
												begin
														xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
												end

															
								
								if(valid)
									begin
											newcrc[0]    = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[16] ^ xgmiirev[12] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[0] ^ c[0] ^ c[2] ^ c[5] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
											newcrc[1]    = xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc[2]    = xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[44] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[2] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[12] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[27];
											newcrc[3]    = xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[45] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[1] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[13] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[28];
											newcrc[4]    = xgmiirev[63] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[15] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[1] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[18] ^ c[25] ^ c[26] ^ c[27] ^ c[31];
											newcrc[5]    = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[14] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[27] ^ c[29] ^ c[31];
											newcrc[6]    = xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[25] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[14] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[28] ^ c[30];
											newcrc[7]    = xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[0] ^ c[2] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[28];
											newcrc[8]    = xgmiirev[63] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[17] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[18] ^ c[19] ^ c[20] ^ c[22] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
											newcrc[9]    = xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[18] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[26] ^ c[28] ^ c[29];
											newcrc[10]  = xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[50] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[19] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[18] ^ c[20] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
											newcrc[11]  = xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[20] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[1] ^ c[4] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[27];
											newcrc[12]  = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[2] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[31];
											newcrc[13]  = xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[25] ^ xgmiirev[22] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[3] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[30];
											newcrc[14]  = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[26] ^ xgmiirev[23] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[2] ^ c[0] ^ c[1] ^ c[11] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc[15]  = xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[24] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[3] ^ c[1] ^ c[2] ^ c[12] ^ c[13] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
											newcrc[16]  = xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[51] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[13] ^ xgmiirev[12] ^ xgmiirev[8] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[0] ^ c[0] ^ c[3] ^ c[5] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[19] ^ c[24] ^ c[25];
											newcrc[17]  = xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[14] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[1] ^ c[1] ^ c[4] ^ c[6] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[25] ^ c[26];
											newcrc[18]  = xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[46] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[34] ^ xgmiirev[32] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[15] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[2] ^ c[0] ^ c[2] ^ c[5] ^ c[7] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[21] ^ c[26] ^ c[27];
											newcrc[19]  = xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[35] ^ xgmiirev[33] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[3] ^ c[0] ^ c[1] ^ c[3] ^ c[6] ^ c[8] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[22] ^ c[27] ^ c[28];
											newcrc[20]  = xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[36] ^ xgmiirev[34] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[12] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[4] ^ c[1] ^ c[2] ^ c[4] ^ c[7] ^ c[9] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[28] ^ c[29];
											newcrc[21]  = xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[37] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[13] ^ xgmiirev[10] ^ xgmiirev[9] ^ xgmiirev[5] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[10] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[29] ^ c[30];
											newcrc[22]  = xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[12] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[20] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
											newcrc[23]  = xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[42] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[15] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[6] ^ xgmiirev[1] ^ xgmiirev[0] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[10] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
											newcrc[24]  = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[43] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[16] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[7] ^ xgmiirev[2] ^ xgmiirev[1] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[11] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
											newcrc[25]  = xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[44] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[38] ^ xgmiirev[37] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[17] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[8] ^ xgmiirev[3] ^ xgmiirev[2] ^ c[1] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[24] ^ c[25] ^ c[26] ^ c[29] ^ c[30];
											newcrc[26]  = xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[54] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[47] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[39] ^ xgmiirev[38] ^ xgmiirev[31] ^ xgmiirev[28] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[18] ^ xgmiirev[10] ^ xgmiirev[6] ^ xgmiirev[4] ^ xgmiirev[3] ^ xgmiirev[0] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[12] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
											newcrc[27]  = xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[55] ^ xgmiirev[53] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[48] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[40] ^ xgmiirev[39] ^ xgmiirev[32] ^ xgmiirev[29] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[19] ^ xgmiirev[11] ^ xgmiirev[7] ^ xgmiirev[5] ^ xgmiirev[4] ^ xgmiirev[1] ^ c[0] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[16] ^ c[17] ^ c[18] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
											newcrc[28]  = xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[56] ^ xgmiirev[54] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[49] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[41] ^ xgmiirev[40] ^ xgmiirev[33] ^ xgmiirev[30] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[20] ^ xgmiirev[12] ^ xgmiirev[8] ^ xgmiirev[6] ^ xgmiirev[5] ^ xgmiirev[2] ^ c[1] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[17] ^ c[18] ^ c[19] ^ c[22] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
											newcrc[29]  = xgmiirev[63] ^ xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[58] ^ xgmiirev[57] ^ xgmiirev[55] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[50] ^ xgmiirev[47] ^ xgmiirev[45] ^ xgmiirev[44] ^ xgmiirev[42] ^ xgmiirev[41] ^ xgmiirev[34] ^ xgmiirev[31] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[25] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[21] ^ xgmiirev[13] ^ xgmiirev[9] ^ xgmiirev[7] ^ xgmiirev[6] ^ xgmiirev[3] ^ c[2] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[15] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[30] ^ c[31];
											newcrc[30]  = xgmiirev[63] ^ xgmiirev[61] ^ xgmiirev[59] ^ xgmiirev[58] ^ xgmiirev[56] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[51] ^ xgmiirev[48] ^ xgmiirev[46] ^ xgmiirev[45] ^ xgmiirev[43] ^ xgmiirev[42] ^ xgmiirev[35] ^ xgmiirev[32] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[26] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[22] ^ xgmiirev[14] ^ xgmiirev[10] ^ xgmiirev[8] ^ xgmiirev[7] ^ xgmiirev[4] ^ c[0] ^ c[3] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
											newcrc[31]  = xgmiirev[62] ^ xgmiirev[60] ^ xgmiirev[59] ^ xgmiirev[57] ^ xgmiirev[54] ^ xgmiirev[53] ^ xgmiirev[52] ^ xgmiirev[49] ^ xgmiirev[47] ^ xgmiirev[46] ^ xgmiirev[44] ^ xgmiirev[43] ^ xgmiirev[36] ^ xgmiirev[33] ^ xgmiirev[31] ^ xgmiirev[30] ^ xgmiirev[29] ^ xgmiirev[28] ^ xgmiirev[27] ^ xgmiirev[25] ^ xgmiirev[24] ^ xgmiirev[23] ^ xgmiirev[15] ^ xgmiirev[11] ^ xgmiirev[9] ^ xgmiirev[8] ^ xgmiirev[5] ^ c[1] ^ c[4] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
								end
				end
								
endmodule


