// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HZ_=7, %PZ(.4X"XOE3I+"G[(%K8C^V&AF]"A1T[(]0=-N<>HL^Q*"@  
H.1J+)<&6-M.=SHE;!S8@G?O1=M]G(<\WY+H.,1U>HG$9XACPCG**X   
HD5&6*JV$"9J2VFCKQ44/!X,>' 6Z'1J&4A''L7NTLVYBS"\:AN!2F   
H.!/X0%&I=-<UFZ%94S@(CB?H$G.>0[5^MG-%:,YV149#9I9PLPQR!@  
HB" %W\77(29%017K]G++9E,MT\050>*?-&IG15^_! E9;)MG:W<VL   
`pragma protect encoding=(enctype="uuencode",bytes=5760        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@@)(N/:6"S.YCG("Z,E$A@0 6TR]L7>L9/%!V9*=UFEL 
@\CSM*U]*-JDSTMZQNI5/.BY:%G<*A#H">9:DZ8'4E,, 
@HU&DU.P2]2!?!S]9:!0_NM_0U,G2@1,J N1"K0J&;?T 
@A)0 9?>6-(!G_--VN2DYZM1*2CH%KT&GB9633^W^(U0 
@Z7/TSZ@; E#S48VST"@>\4P%,&A=2+&:1+A6"@1,E9D 
@2;RW<U$V9MOH^!2@S6U::W32MI!16[!:DRN3@?$)Y!X 
@&OZW@HKR6KRXH)]VN/E,V,V34ZCJ;K*R5^GJ0).F/P< 
@!&IN1P:'L<5-D&5F0BLHK-/A9HU?]XBZ:*(5AB>";Z  
@1_NT80K>LV@%NGE40TN%7/^K^.GT1LW06Z@HIY@6]M8 
@CW25'Y*ZGX%F0Q2;S,8O3'1)A-^"0.6*0KDEH1$^QF@ 
@&T_N[^^[( AQI&%F\1_*4:NV3-BOA2@#/":O<A,>KLT 
@/J5"GAH:S<T3-YON@(;8/73])&PA9$O?+_S9LV17U<< 
@P6FT>%+_ZA'0,!):KZ#)N$F7/S(>OM+(/^#5 P3ON-0 
@Y4[]//\',4^ $IQ#2 0M"<=3'-UC+$M:E6I+T?VO(9T 
@4X3&V7G7)<*Y"R7N>$ZR&LN-G?IMM3N]0[8T_KQP%DT 
@<.C?BK.VF3G/WEW&!C6B[W+VW0OJI^L1D;BAJI<!2/8 
@B6+2XRMF,W=+*2DR]1/&1KK@S:]+"U3.1GPV)J$Q;D( 
@C>&N\ZP>X!K\2CN@X2 /'HY<O9KQ4AZ32BYKVOA"\'\ 
@B+S-[H%HW RP90>)]&&ZI[<V4$^,\ET>+H:CJS>&!F@ 
@W\.'!HH]P=8LMHT2L+#=0GP;!4HAE%LB3.K(4#M E7D 
@)TUWN>@_CFC Y%I:0/H>,#0LK%B 9$"HFQ[5VOFV_]X 
@&/[7K=[2F%;@C?I)L)M0D^]0X(.J\Z5 B0..X43-GJ@ 
@5[US6C=S*GLTJZ/J3D3/L@Y^]#Y=\R'9O26T?'.5X\H 
@X9B_VBS1(E$Q5<<W,9\D24FX$L9A2!*G$M&.E)OH/18 
@ #-^XSHFQWW[\*1T1)1<5D"5Q0F!93B#W2;K8+J*$BT 
@&@B*#@FO+@PP.D5DJ;-T2J(9F4*\W4Q0B?TB<@&Z;>8 
@K<) ."Q@O'-5&R$LF,P1;FSQ5G-^5/2[1]*']<R^)C8 
@;0MFN(QAMKV:IYBL1*EOAAU\)EE6$3CM?71T1@(^LBD 
@0$!@=P_4;GZD*]$HV:PC%0[0U?9HM\=[)!OWP/,T:Q< 
@S#_?7/?PW!)W[R:!!6V8.&@9U#XKQ-#6]T)WJ<,#TQX 
@#]'4;-FMX@I1GMB$&#F[(YU7L,$Z(I5VT.W31:*.@84 
@_RNHM? NCV"()TGMVZ?VA0>XSU#:"S/6AER+H:?W[Y  
@9HZ^#J<BPQ-Z2]*:S6\7BPOHRK4^$A)=3YNDLH["414 
@]K1>@L_[/##_S6[$O0NHR;WAJI0:A+M<?AH!9QD]>WX 
@3:MI]#ZAPS'F[30\CQHW9B@UTYP+G)[M8NTC/>3L.&$ 
@"RXX/]OG!V@\"@.>-P1#"\YM?8A- <%WWCPE!M!AJO@ 
@P*0^EOP0Z:N^N-$2F/M?-C51[=N8DU7]$9\R9Y5$C+L 
@(;LL>ZUW))D5I8^4VZ6Z6"\13P/^ ?U<X58,DEYRF>8 
@:3ASM:=/ZIW F6^L6KBS%_89C),7(:2*MDN2Y7KET/H 
@X"19WD'X3X[^"^ZI QI%!<\+CC/3=CV(3@:1^M9ME=8 
@IY F=*TD'6DXI%/$K[,*CTEAE_9U[Q/Z0+$)Q>BKFZ( 
@N7018UW\XIV4'[B$"6QK0^)^2/E%MZUZV,7>L@W)/9X 
@+/@/_I7(?QH6YSO\':TN#@@ #][W*LO')U!V;;/AY)4 
@*<DA_BVX E8ZXW@>.8L]&Y\-2>N8+E*XP1\D)Q:KV,  
@>L(%TP9%IZZ-&+%1&:-J9S+,KX 0,-W@[%36M@ZF"^, 
@Q[72UC/Z)42CV'^$+_P#)CZ74LED$^8^9),X65%W -( 
@#A^%J)?WXKF=F!<MUHV;QEQB;Q8R0.EX4$YPKG-W+88 
@]T\Y?USAXL7G#.TO(.0F#SGM2R*F\RJT0"8FDC#+M#T 
@4?@YTF7C\&;X]K8IV 3BK2$,:"U"T5U,H&L4R79>/1, 
@CQ+;*2;0^.?^.((=]XW=I\6UWE2GX_YA;\\!11(I6,H 
@'CP ;(9 Z-7WV%@S5[)/H'A/6D3%XZL,_%&6RM:$R50 
@@BG"W9?09R$5;_\:[P_)/ 0-W(%/7*UINL1%D7_%;!@ 
@Z6"!A.134?^>"QHD3*9Z+Q J*#IQL!F&Z>A& #LM?-< 
@T^(Z<\BLSH@&O1-:=BH% D>B9A?BLGMYM<R?R?S +"@ 
@$6K_T1GF]S%,9/\,11TSV->KQ]XL]+*/X^M%_G?R B  
@D^X^O_2L9S$5CZ-J^6,-O$VQS#-[!+Z&'A],0<SB$=$ 
@YN15A7Y)7ZOZ@-^!DO')0L+ YE%GX=\QD[AP!S$)F>T 
@Z+*34OQ!T>Q^A,S&6S-L+$@LDE/EE!<'$VT!6*L0^E@ 
@UIMPGBD([DMG>;&[>A2Q35HD-Q7L[UWUS\X2RVY(Z-H 
@60QO9 LD?LJ,5)]_\_G K??9U1L+K22\1$8<U,920HH 
@&%T7UO%)T1E/E31 #7OSQK]7Y-:Z$"&4=?PR_Z'F>JL 
@0U3L0$[/FE$;HFJT!0DDF3Q*5/Q_'DS0:9V#X]OM1YD 
@:?C2'S%AX!U]TS-9HKB 2_[M0 *L<8)>^"[0/C7><&( 
@6;,/%557F5+I X"A)KH;\7ILMY_\1SIJ=6#2$55B@Z< 
@TZ\1&,-J8S(8@7F,,B",D]E:D)MHCK,RF=)5;?TA9@D 
@4K.Q'UF;VLOV?#%7-4*05-VU>'R&6S;$8F<)[&]K7"0 
@)@\G;FP]JAS-'*HN P<-9%O?73.$6^3?TVQK\OCEV6< 
@;TV_H!*9N]\EV"<7@?<7NJ>AC!X.QO)L>])'_4$]O'T 
@&DX\*%=389R)87PV5OW_.E7V8\_\2&M&4Q,[0HY:G(T 
@"-\EL,%Y8_J[P795XZ=MJ/UJ_PK:&^4(S^_KK?@@,MX 
@<+V: %15OZX"4$"V^"<J5HQXRS]D;;P$L-VZNNX]KM\ 
@SG859 3&_3GG)04;!LY8\F,/)T[AAW_>-)8=UOW]K)P 
@/^,I&N?H,J:SEMBP,0?7CQK^=L+T5I1!-Q"DM^I'@:X 
@I?LIR@O'-;NX/U6[<CIE%'&QB5 A'<H#ND:ISSB-K8T 
@B]W1^\I11Q?X(X)^.?!85/"!8_Y;C>$X\-F?::X?R4T 
@,L,0\J33R'L5>SV0,5)!6  =9L9(6'UQ5,KNZJ?!S#D 
@C@%3+#72]W?.^I^[1H_N\"! CV:+]X*WA,&(.[S70KL 
@0)K+[>I70[Q901A*W$O+!<BR4AH"![,%Q3WWVDQ6,1T 
@Z- -;^#*]D'7LGI%>LUMH,$OY>U/96Z8V-EPY$>)W&  
@.Y-C;Y-PG"CL5WADOBAL1[^N+)N?/\W4&[3U9^1TCH4 
@]LG!CN[TL50G"++ME]@_\;J7Q2=6?$2KEP;C.!.-@,P 
@H1X,TCLDW*.+=)%RI9,!AALLM"J5WPEYIZIR+TQ2=UL 
@_4$CQ%WC\H(8^D&"IRH'=8R[D:GJD[_H"#*>ME$XH^8 
@*F:G>$JA/8+'GF$.34,?P5MH*>\J=%2\">DXT@-,0"< 
@+,:.IEHP( G /+X$&C*SUN4"7X]IS0E:3MO86=[A'I, 
@OO&&-EZ'^]R6H78#(9=N4O$K:C-_*M4A:?Z0:;YQ#Q\ 
@&I<>A'P\O3_:AEQ=3_8IL^34Z-9V)KK$U?BUGON+WG< 
@$<$N7= V1W1?I'I8EU.'-]$FRHCO (PF\KA*0II;\NH 
@!AXRHV%?+L8'>J?H\OSBFN0[,S3LDRAMW/!FQ\U<4Q@ 
@9%:3;,C?CUD4W49'PCB/A3H#(EZL298TRN/F]^LL614 
@T'#&7'HT3C8),DW6]*7*8/(4'2$%]&, D=\[J2M3YV( 
@'4X<+=WO&V3*PY KD67N^7@5LQO-A_KL,*[AL\.LY-8 
@PDQ5_W.,S"Z:]F8T1#&Q&S_0<HRMK2\$#CCN2T_GC3  
@9P0KB<C@TOBC!P-3#X?CG@;XM/#0U0UMJU@.3X"8A<, 
@M+K68P$:B'1<=DXJ_V ;B'3TPHG!(H@K]^VB5#8R1;D 
@- "WR#M):.!'[UCV'] #_K-*Y\+_R+'(9KV3;M6&W/@ 
@^$P!Y37YV%:859I> CR"+0<FHLWN)AS'DV__.L$L'ID 
@ZSF9IZ9&+XNV,C3OX:\;W_2E!B\-[?J36^(/98H8Y.H 
@\OHQR^Q'R*-$9('&99GWT$)VL;(J6PR3[A<@#+OH4_@ 
@\?E*!2$>'4/I U-/-/WBWN($O-QI, ID T1;%*$:><L 
@NF@7VZN;B28&K/X=S *V<:^7D*5%F(QVB3CA>FNIY#$ 
@YA5-/RLKQ\9?'3B<4P/L:E))"_BKUX?0#9W 7EWY^8, 
@,SX*/9%G6]-HH!2QF^,\EP!52=WR9$*MB>VZV=F?^2X 
@;IP95,4!0>PG65(ZC*:6*C9<Y\.XZ%MJH5CS"?X=+B$ 
@"_0$(^[^G=,V>4T][],/'IU/.C0T5J68Z(S%,SY&EUP 
@Y!G6@$3(SXU7HZYCT%@A][]1Y64? Y#NA0@43L72_7< 
@R CEA>[CJF)<RHYJM#(5EZ&:W7&:P0V'5(NR,']I8JH 
@()BM[3M"%EV+Z2^QR0:! )*DGI/S165H[^BV:CU*L'X 
@-4TG16SKFAU6E:UW6!!*K,8;H*\+9\@:C*OKX,(,1"H 
@3F21($?E9.0ZX,CTK;C@+C@!*,]9IUC=BX%DDKM6:94 
@I5K=(0A'T;1;_DHX]C3OAQ*HIA3^"QMK?!+#*UAP"9\ 
@Z=<>\=LX^"LJ)JDOB:OF*4SZT?L=1YXYNIWEJ6(WYPD 
@XG=-L;T%Y(T"PJYS4 /!+CDG5%$?1O0I]8UA'$^W@U< 
@/_F$DU+M\(WGCJQ08R2&>5F+W83!1K.H4.<=3P&(5!, 
@7W IMS+W],.%<<P"XU!E>Z9A99E%G96;Z[=ZI2D"#S( 
@TD$3,'F>N.3OWX'JZS;+UP2P,?5H]:B[3OD2]"5'EWP 
@KDY/.N5&MN5R<D#_AG/4'@&3YH'YZWV71J^W>(H%":@ 
@-F:G6L]"[9 1\N?'DE947'79J$^:[!?I:/-P0_N6 V4 
@.4I%C+!=^-3^>0&.Q:#4"W2';LRW=;WU4>88L4Z9+%\ 
@0;$A5SO[6R_"F)A^ =RBFW"2"D1(=*^@QJE><YSW2X, 
@0'9:NWNH*PG+U:REPK391MA0:'83RNLS=P$4O)*>CB< 
@/#]07!IAQM'9)<*\))K )N )V]'XYAI%8P:QSVQW2'L 
@T_25UC%(?T$2=H-?!;MUGS=N41K(7Y44<X3?N1ZN4B\ 
@S3'5PA=0OAB29?'QT8].9U'F*+V13(X+%B(!'K53F?D 
@8A>'^B-M%I/Q"5WD^V)"]&>DM.!,>!*AEB&O/-@SE(< 
@)5C)FFUJ2/8PAD=7.^,'M(B;\P[@5-@PB"#+6V1!)+H 
@=<8>0]@03*[R":9-=O3H&%,;<'?>^J]1[,?=A!"$U'T 
@7BVL?[MQ;@/CS(K$+!1 ?#'%;;!W_]%K0S6FPZ1 =PD 
@:%=OHU)N9Q0E_5'H)%"\P,\HY/90,D361"ES5NJ-U>H 
@:I-02P 5CG%<7]L1J"6(A%MSK>E5*6;R.-TN"-!B_#4 
@9_1:*V!7"5>:/KD@E2E4@?0#*#>O#^F+4SIB \P +T, 
@1KKA5]'+Z*R3R%U\WKLT2>F.$*;<O%?-:9*+7T_R(C, 
@LJ^<RTOUTO+H ]JJC=V\A,2T<PU;98KG88KQ5.\/$2$ 
@9\1ON=8$_2-B#G/6MX=RK3$9Q+M0B$NF9BP9!,S2;W, 
@LN(!TK*]ON2$%X6GI'EA7X#3"\\>X'EVNN5VSP[*L?D 
@,H?0L;96-&/)NS$17Y-W#L*F.&D T23 _BEVPW-T"2( 
@<IX\+47,;+\&IR.PV2/X<%DS5F:]&2^#=6S(=2,\E8, 
@3GJPA.O[QN_X:+E;';=?<G;@2J$'5X16^";@VU/,X5$ 
@I>T%)I6\Q9/9]Z=M<UNZ^8Y^8N!-UW'+M&\*^8FR'Z$ 
@&<E+OJ&5)]*'8&+XJ@<O,JA:_F^QWA1.IBQ JL2$/A, 
@K3<T\NV#OF 8M157-&J?5:1,G7C,=0@,J>_LRYY9>DL 
@6(TJ7@8N=;A;!]H]LUU.D=$4B,0S*8^3ET*K\^H: "@ 
@"O!8CFK7_T]CJM'3\9EY*H.$IJZD$W$8TUG,+4>4MG< 
@/FP*3)? YN*R7T-1;</NPP-PK=GV^P\V5:':_X_]P@H 
@B_ZG8#P=66R1NH/<]9UMZ&SG@1]PWD53U!@U%!F;;;T 
@Y[ ']W>Y\R7':\[5M2,]@QKV1CZKPIC.?XV;'U&U#7T 
@GW!E%'_&2\A/F]_&#0;K8)W&\*&8Y8II$U+HT9E%KUL 
@S["AK("4NZ1/)9]OM4F/+_!T'5,&:[YC4>L,,LX,&/$ 
@Q>^UU^#B7/0((47GF!B3'P)]W.T'4@T&TR6WF;.Z"1D 
@8'XE0#27A^(.3#1A.\DV!KE'*";P<5/H=L.OKULD&?@ 
@)>0G.YMZ()A*]0I@9O:Q:$,4/?2B#(1K&(_5TK%26>X 
@>CO\T.T)09H PNMH$05T>HNP_GR:2E5CA"'<[Y*R=E4 
@7_F##<I54^MGYE5K%QP+UT!Q<>B:Y%+@RZBH*F*&WY@ 
@ %,QI8<G.1&8EP2.B9I*GJT!!)&%;%Q^6N84A5W2ZYH 
@CTQ'T31EI.KKC'[Z@'!64?T#HS;2-4=1\/"D0YP+.:( 
@*R!8^+KIK3Z9?K7V"0@*L/:#Y5E."[A,?ODJ#V_I4X4 
@?MHB&GG!8^2J;=J%!2HX_A\@(R W594;A63/%JH10P8 
@)>D%*C>7F-X>HM_?8(]S!CHUQYCM9V +@O9T!SE,C!H 
@E@*<J=':3?"W!^-/YC_KY)PWZ)HUW-PY8H$/6$BU5*\ 
@E9;/XM4VQ<_;0^9VSR3\/(U]%HK:BZ%ZU8O!*XO\,@( 
@O%)H&74R'_!\L.:J<+YU]V(CI:*;U"_VZ %L' JM]9< 
@<*HXGGMP3RV\%6=_*C,PH^]%DPRR4T.Q3LO[EQI4M3T 
@T-%XYQO;$LYW:3-N02 SO?$%Z@A#'X?6GLZ763=5!=T 
@I;EC"YG*CZIC;BW#'M_%Z<F9'P*845Z'%H$)%^Z3_+X 
@U(JBFU<*V/5%.'+'IN\ZQ[$%^3@+^PR_;S:YY.)=92< 
@T+!@7A17-^]TR\4[ Y?^=RU//$WE^96O/$^E:ET'7?H 
@/?G(U#NXE=V"404\O-RP<?$3+7^K'=;/+TKGAL%?@.  
@\>FGM_?79[(U\&J$ JYWX>\[,G;B<T]6+8?CJI#Z/8( 
@82I<H<L3+FL62=#PR1TGV]3AQ%S>KOIJA['Y6,YT("  
@=^B'T*Z\/31WW^]J*\(L4Z'NWB%"]O8OO69_ZV7^#W  
@VZ7E']G]+\!$OL>B)E<F?@/V,/U5EQ-F1^%;2*9Y854 
@>"]AF%*C6\#J@G=#W;EHB[UD.^ZF0\% V&+RB2%HE-$ 
@==:95UOT $/CS^]N![S%'+V?$:#',VGB02YM\:%- )$ 
@$-N/KV_8:R(ZWT_$<6$Z.D)3YW^('_]AZ(:- S'^ZX4 
@6?XF='G.(0@PP3L7Z^1Q*-4-YW@/?0V=A@**::PGVR@ 
@5*47=KT1<KIG?.H_Q "<XO9]'QO[IR@T7WYOWOGLUC0 
@E]4H+%MSH.))P+#<D1>[:$\)CR7:V6U+2::XWQ?O *L 
@+?WN_WLEH+\90$UOWM6_3K)=BEFM:UE0D5FHF3))I&L 
@H$2GD:ULS30I7.[N)Z[9?1/Y)UIF@3##AQ0>Y]&(6:4 
0;0&%%5? E!9.=%":;IJL<0  
01$[]@G65#*23_.9D P$!OP  
`pragma protect end_protected
