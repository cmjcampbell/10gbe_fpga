// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H.OZ>Y=VWXK"^WU4_-7^F2)#%0W[<R9K9%F(HLL!9YIN3RD,%"+BG:0  
H1 +?#!@A)!\$+6LYD=(*LG;HI^'$ZC%;XZLL*57>?#0NF,5RIH6<,P  
H(ESKARW^:N9!%' >0=*-<O%K<.6/T)*C1+5_,?BD= $UN;2+Y]O6*   
H>S4H0&EP;VBU%3=ZW3?]"-T:&<,6O4-4_AQ=#&]M:+YP,AZ\+X(EDP  
H/>CM13%%G\MFK8XBTYW+OO"C09<=R_DG1"B1O46>ZLY_1.8GR47.,   
`pragma protect encoding=(enctype="uuencode",bytes=2800        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@6HJD8?8[H$NG*UQUPEYJ_IS'WHH*(9L/;EITH]7#TKD 
@F$JYF0E^ZDVSQ2HU059YI"9K"IWGC8>H,S5S&(6F>), 
@-9%Q'\[ 7@,=L1!RI!C+ !=&(ZAO8PZWS.00\F^M$H@ 
@;0Q633:_YJ?M)&A*QB*(<7,"[ BW2&CJ  $1IEIZ+_  
@,;&3\"6#H.YA77B7>JXTHS00D9QCKA+6YMEVTD+>"_X 
@+P91D/56Q8HI'%.68G 3'-&\,B-O4I;FFC[0U")ZUI$ 
@X#RH;4D2,&+DY,O71>@DW6/F 5(IAXF -_!/];K_&/H 
@,,NC\_WCHP5W!8QL"7L7TG3SHY?NA4,ZH&,-X2G[O5$ 
@Z-FJ/0$7G_U,5@'FLP?/6P@_PNJG@9()+*#FR^H:)-, 
@ZP/CR[4**W" #C*QW67M%8F+[5\/%/?KQV<H[:4M!D< 
@5F($DLSP=0<,N,4*D\QS)K(U$M/PT^-R$$#L\F+8:<  
@NONF7+4RM#M."<$W'L-U8![H>8WD(Z\-5D=#F28AEU( 
@U5%T\X, <&].3VN:.7%+@P.EYAB)<_PLP6[@_IR(EB$ 
@O((,]W0M];-I]+QJS..W1;G^7'0UK,22YM:/#E,*=7\ 
@#!N7C>PI7BX$1&W/4O.[V2W%@FK.Z6@+B,-)U@F@XS$ 
@!7P&D.,SG=&)DZ */-&TVS6+I*I'NWL9E7KV:O/8M(\ 
@.K;)#RE>><X7%&9AO_"ZT*#ZA_+3ZM&=54CQRNE(&&4 
@PD?:XT?=]?*B6(8U'^TJS/$>SKFB@$,IF]HXF(LW;_X 
@&U-0<[)5J.*>4%;8(3D4T_=Y,U3"-A ]:UKG?3#HP"X 
@7IO?:L'!]^"6F!KW**8L\<=79>_?[DE7<+\A K-5&ML 
@J:G]TO7WPIETX;2M E5B&(GDP.LL"2EQE^P'Q#Q6-U( 
@)$1[KVRJUF &>!)WD58,!L]^L(3C\"XJOFA&D*?DX3< 
@**HV0A*WUF=+B*_+L[,\>JC*V\^-F*9ZF%/0AQG.^$\ 
@E\.[AWH!*&BI%Y8*-FG^CWF&AM>[NE=7/>D=DJF!.&T 
@+P_2J>N;@3,U/E9,;\:I(<%]S[4P8-508GNL[Q]=(O  
@WTD:"(_\T8 F5E#+IK:$TK.I/J",C*7"2RSZ$<SN^EX 
@V3HH[=SLJ%L)=/=MHG+RJ;%Y&R"GP?;B/+^95@$2&\0 
@5^\8WEQX0;]9@UI*.&,H.W.+YKYG?>DC+&V<MP$Q@Q@ 
@1\0>2_#%\:31[#B(O'&&1SS5C86W!U 7V^.2.,Z#XW, 
@I";,Q:VZ^>.LVPV,@BMHV!GT]/R@&/YW.HAB$?I^Q)D 
@,!R4V *-QF&ZQ+C'_J[T?C2>+CE3KCV0\^VJT_+RM]0 
@-[!8 ,KI)6D)3G:RJ%-W'^3.IXE!^6D.5%[AA*4U*>$ 
@0'9PPX3*UJEIU%!WLJSI#W&3:@1!>A7 JLS@_;H'B.D 
@1KRZ=@ C3<(PD55&=OX"S0^/,L)_]2?O)@IDK%+7*8, 
@J'P:SS0M?M4)YKXIKGG>GQKXT!<X*.X!C0D E^J$TQX 
@31?^'!<DJ^;V1@'-,VBX;RKO90L%[X#QA\:_/43>K;L 
@.S"1?\":1F$R/7PCCK%:$;')7-U5& D[T+=7Z%92WHH 
@87HAZ7^D)819X<I2%RI#CNI*]!*D7A24MVHM"I>W"\4 
@OVQ &S-C'A"!HX/ F '3L08-DD W#'JA)(?5VC[O*+  
@!E^.4UR9RWRL"-N\'[:VU@G->@N>-%161.^1Z:?R+/L 
@9PO^FQ ;NDES8^<CMXZ6]J;MNVN4-V4\)SWX'ITA6S0 
@I0=7R"-ZZQKOBDAU&.K&CV>?S [XD[$GOH['1O"/)G@ 
@W\;[^3(@,-1/(X/#$3:%_3,!43I+ >$7Y"2<14#V*#L 
@6V81*5*%#-LY&TB=NLZ)-W73(LQT:ML)$R!.Y$\%6:\ 
@-:'><LS)==IYRGN4<R=%Q@7 CE5B%VG& '8&CB\32ID 
@R:72N^R$SNX^&H$)_J]/_2A#-NG=JQK:Q,&'_,#39\  
@= 9WGV&(0.-R(.&<CMATQWS!"/ANU(F(<?:1:728E-8 
@U&VF%5J?O:9U[(;WQ>W3C_Y3&%?WY;S_/*@(&,IQXS$ 
@L=]1*CGAA4@IZ4.WJS;,J4%'FQ.*0Z1)JC^\VH@QT-( 
@RD5)=?YA\Q.6(7SA'])K)V%KZD=]B- HK>O0>R?Y%F, 
@L&C&TQ2U>B"?.'+;.U16!:/,T1&B\T=@VG>PX(@NP)0 
@Z7=7%)&B_ES,D2WT7(:A9RN  1VA5[QBP.QNID1KQ9< 
@9_;KX^N?&FF[CE0DU@W\V&Y<;<4;;\96!W+Z!21L?DL 
@%WDJ;X\U_TL(5,*6<04U65M-AHG>L]!52<771_""<I< 
@Q.NT<U6IA=@-WC,0<\I081%QS#%+1MGPK'.%9=A*!78 
@@-5M/QSS>_9<<37%S7GK/FXY=FMLIDQJ4#1D2=CIB@, 
@#@EYD1S,5[;:'KTVX6:\3<2Y>]:9B(D$9!(VN%C5MVP 
@OE2^7@+."<)YZ--H>(O=;-_6?BGK\8^6W#,LHUXKZ"T 
@M(3-[6J'VAI0^!>3U;ZN)L?_DXVQ>@JOE"*2%H^YMNH 
@@D6Q(/597*!]^;JFFDJL9)PL/_C_;<[O&=#1+?1:ZY8 
@XC[_FJGM5-?(%_^QF:D7[ @J^Y4V2>B16U&Z,KN;D^X 
@\21",3& 5?,1[\S.$$>KI4Y1A_'?(([N*9?[%\&<UBL 
@#.CGYC0$[MWD6_E/6MR@FKQD"N/=YV Y?,D^+^Y_U%\ 
@@7?G'C.I3MPAQ)BUM8257OGHRB"'+!OC4<=,)(%P>AD 
@][WW7?F*#P$9%(%I43'@BB:?$LI?%-B1A+J7<3VB_?X 
@/A!SR0BT$VC^-LK^@/K1/D';M895&]!WQ0&8OL#P:T@ 
@,[/*TK!WJ!/V<^5$'"#\K&++#2$00C"[V)-_U)I9>^@ 
@G.[1[F43^80KES 0O=!@D=[LYI&)+P61(63T?IQ_!,D 
@/21(6VJ'ZB[;N/WR^R?+H"NP@ 58ZP?[JN/U3$!?"6, 
@FH#I9$ZBM033\VJTZ"^"HMUC;UO$=DOI*;@QPJH)O2( 
@S!1:IF_#!PIU.E4<$')*%R,<E+<^8#NW\K"B)U"N>EL 
@4YSV%'!G:0.K=X>,'W4@*O=<>VL+6?L 4_[+#MS#1PL 
@=D<<QI";)*?CQS8HDVA@Y*[Q]R3$OWT#OCL6EUH-5ZX 
@,\2E>0";) N]^RC=T+@&_$.=9&B [8N5KBI+Q3X$1^< 
@3:T%G1K-TW2KTZOFNT%NL6F=,T)X \2RV!?^A_>PCA4 
@=@#/4=S$8.!?KD2SX_/$R6R S"7)2\8KWR?"O;Q25:4 
@'SLZ* ^N@X_S"0Q+!:Z7>%Q1LL'-B>1%&P6,'+>IU+T 
@&!$$>@$_L_8\+@[;+-HB+ZD@5I>OZ3[*HC0_KCCLA[D 
@X8H"&1-T?DA'0+6P[$)GMQA<MI^U76TU\*6'*5:@4AH 
@57RJ>L!'>BPHEXP%/IM:OA?% F$G>#ZX]A6"NJ'U9P$ 
@E1#5/ >A-J9\%/18P/K,+%EXK9&I<=LV1^H%+(ILCLD 
@P'F/LMMDMH >Y/%?.I7351?&T1DS?BUK^5-!#+(&/)8 
@X^H'"80FW/)]#S>)9<%Q^7[>/G)B[[L7\@#\.^MR5JL 
@Y7=J@\>WA^='LT5F?MS$CL"V:68,,D,*WT_D.NW45#@ 
@#$G9CS=-06E3W,5+Z9_T1!IP"J*R*O!X1S5-V,5*\+P 
@"..9_H_7W36)^.$'Y%78FTG.8RR9KHS+#3U#YD!SCB\ 
@B>"<7:GZD].WL6#4J,5+.(B!VG+L/Z742B<+F!VP.:T 
0SG^S"_C);YZ-)^)VX<F22@  
`pragma protect end_protected
