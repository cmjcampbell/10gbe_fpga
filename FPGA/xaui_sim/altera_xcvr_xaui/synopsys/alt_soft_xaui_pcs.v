// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H!.-UY:>)A2U]9,WE(S+<2*@IFXU#4TI+WT=OOEE D!ZR571:.)6ITP  
H;V=*>ZP0:125X)6KU#\\]C()RQ-S2.O^?XPB-)%._65V[;\84&))#0  
H-6 7!T05FX7,8[<[/%WG/& 4-0H-A:&_[7]M^W3L A5TTY^N1K22$@  
HA0KC:]H KA'S@BRS-FT+CX&=K12L&O169G0;B*Y;M.A!5.58B9:IK0  
H3L^@S.QN:!"G5^VP=$E06O#"94I#].H%$ET.7N%+5V,X\E#6Q)IYZ@  
`pragma protect encoding=(enctype="uuencode",bytes=5408        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@A?AMI3WYYES6_Q^.&ES;47<O%#RC]7\]*VZ7!AV@(+P 
@YY%D1/KB!Z%Y6IY4G1.,.%93*HOB;R;1?C_JK[YI=T0 
@#,ULJX*J'^'ERON/[$\!9^4>-BLX!R\"W?6&'+>%33$ 
@[)VB7+"#/*)#5&'M'F6F9R0*ZN NG-9OQJ6^@YFJPF$ 
@[Z'SZHV[ 3W'HIQJ94^%&@WCJG8K72RK_M"IA,<CC9( 
@C/H"(%X>8CJT@U1.!VPI#&S)1F'KV!VR Z"2"/=G7;0 
@L_?T8/6EIA$%5TP:#.V]/9:7\JR<YN6K%,B0208FK]\ 
@L0,H5]G5@FQ3H2$M]4, =J^PY%EK'+/4PN^$ 0U4;&P 
@CE_4.7,EH*?E#SW*I/Z*3<\5[$2&2E%DJ1HYU,%TE4  
@M:C1FVL=S2A754&'T>VNE/K(GR%6FP<2$V&K&1*=$!$ 
@8(8"*P.JZQ"-QYIS1PD5=^\/.J8WY2 &W%TA;6=\,3L 
@-:Z0QT\FDTW.Y>4(!U^5#ML0AA:M)!0[_JPHD7,,]8, 
@Y9*Z%FMMCMS.YFW=I*AKJ>/D#KS:MZW7&1(GH8'<A%0 
@6P@S@KF1>2Y<Y/[CL36,[<SX&)%7QWVMTUL-O (WKC  
@JMWC2$#9_FZ2*'FPC+A: 6_F%L32%PPSL_>0@> Y'T$ 
@?CUPAB;,M5@IE:I9J9_A:K.8N- T;MQ^N#7:#[?.R8( 
@CKG+H8J<)F=$Q,R-4(3;>$9>9TRJEU5^X>C+ID.8\20 
@_B'5B7.X9IVI) $%"1M-(%OMJA3PET'#:B&L%H+^06  
@VX.F/V9IVH7-PIDFOXMQ+"^$[WJGWS ,_C%C!>JB,5$ 
@F>GYSKCG/VP#>8F)\F;E@&_N$3$L2%+?^GRZ2VP,MC4 
@T,Y:EAP5P_,?Y;7#N]P.T^Q^A<&GD]8L+NPBL@GHB>T 
@O92 7GT;^F;KOHPG:)]C%V@[$;Z,>_KPW(KDS384\5X 
@>1_GR@,,<]J\UY)^C(H0#;DI)%=CNZ[I9C,;\4VAGI4 
@7SH>R$>+ISC?,6VHM."0HX9 .#V@#LFNNLR4?":1WS, 
@I9_^*&<W-D6KRDT!.)J!Q%L*WA3OP,K%V_IKSE-6)U0 
@4EOJDK?Z[9=O#7V56G0#FT^JVD4O%SFS4_EH[ Z21[P 
@+D]X)FV:8YHW0Z;DHA6B,B0\6/S&#X;_O\&<<AHSQ5$ 
@VWIFBX6 W7_[V'JY>OP-T_B48DYD#S]Q4\1KM" &4G$ 
@:KA4+P>N&!\Z@9+#TJ'_E!WK6X3=S%)^%CGA 3S.920 
@%8_KSCK771+8F_CB_S(5W =_M.4C8='KE9M2,$WOXFH 
@5^5O,\?<>(_D"P_\6+^T=%TD(,ES3]<$KH6@X#R*&K0 
@H2=:S@HY*VBL)0!Q[SGHBKDY9HCHUX->PF(3^7AX:^8 
@BBJ!F1-LJ9]FREWE)X>V=!YTN_CAT?(())*H.*=PFL$ 
@B@PAT%(T@)IU$IJ8 ;.+.)_GCTII88?Y(5KQ;OJ??%, 
@!/6)$&&2*SE+@/7TI8/1;^A-HY<7=%'8,LF=9:HM3\$ 
@)0%$M<R"965!O'_#_![K?[4CP<1 8RG]- @C3#% ?_P 
@V0=]=S)-K?92"A4Y%_TN6R$-TCH402?[<3Y T_44W $ 
@CJO#-91!;D\)L9*CC^112=KO(6D]#/EC\^H8_0P[:2< 
@6@>4UW!OZ!!1DQ FTT3ZOVOG+Z/.*YAL76J)S%_HZ$( 
@:%ZQP#\NR.BEE02Z_/#[37G>>SPOO4WP3)=RFL@\?U\ 
@L6"? PX>)NVI"'PP3SS>(>,?P<SA][S%S%%U )QVF3@ 
@[4$-0T-8()D$4*U,B/L1FN@6K]%8;MUJRRQ(;&-VQL( 
@FLW_P<OT8SV-D<T'E'EURSGK#G.-@\-8&I.)>79Q!ML 
@0H6VWK0)O&CCQ4RRS9()=X.XJ7Y4'&KD8 2I-%2IC$@ 
@$RL5TO('R]U143+G3TSB-H3C.<$F1L-Y@]]"^B[&>:D 
@ZUINRCHH/L?6L1R1.IJ%#0=7OR!KI47;2=VQE0)] D< 
@>&\4T2V^(H;)_8HJ-;./L&0'1NQ;60 IE)(5#(J5_P4 
@52ZB[3_X37\L(9)H'3M@9_;>??PQ YU>) 5VF=VS4[L 
@NV\>/W55..FV)^TX'5-^H4/3V2'%#=[?B8QOKZ,Y0[\ 
@;M_!NE?+OJ>,E=EKVLR3OSQU=1P0J2P!C5:G/[UP7/  
@R"8$A&FGZORL<<F*Q!:<I1MU EY]T1&XG.!HKMR UGL 
@NTFSZQF9CD[ 'K9X\G41T@PG^9K7ZPA49=_3  P*@7H 
@FV<&PI,++H<J/4G8(F'W]$/2]X(G9:? KW<1T/*F1($ 
@J"'--D(H\.KB(#C?2M$'^LD"36FKT$6%9QE88R(O5C@ 
@Z]X%S;B<R-M>[+N,MG#23:2Q'JZ[/QU?="U%RJ1./UP 
@, HE[L%\8\G2VJ1!Z+] O"$H!)2T7?%$X>#>@/.VH$8 
@7M>2HDJ7%RWVS-$PV^FP($F<FAW#/3^<B/.("[9R=L4 
@I ?2GN;_*F63*Y;;99M2HF--^7J=8N'CWX!GU<Z..54 
@:C=8MCM]>^.0*4U$'W0KE78W>W>2G4\ZHZ^$>\H]T38 
@"4\)"GJP#^L2Z2/'F0QAR$ST%ZM?*!W%[;V=FD/09,( 
@JV-R@XZ00]61?3V^_26J#58(A[+%"#9T1UUW 6A;J^$ 
@<05H!=DWC,HZLRS%C5R(MDJB_8<;',(8#,V RQIQ 6P 
@[RHD9%C1JDYWS;Y:? O85_,6B*]2@%G-KO\046V-WPD 
@7(= ]2-@^-Z3CX@'M&8LN8I#E?)7\%+[@#,3%+T6EN$ 
@-B+WET Q_D2%HF%!A )"B&LQ^T IQ1-WVJ/]]=* U"0 
@*Q#2/ASC:0V#3:;G(UNDJ3 PV3)P7A?SSJ))Y!10X(< 
@Y1$)FZ;:')_8O+2#V;,A6R0<!U>^0U-DS7^-3->&%A  
@3HK$IA_P2648N0*'/BSWU]-RH/]59I):.!SLZ_8^J0\ 
@ZM%U/&UVU3I2^1U$TA?;,5! HFO:C:QOF(;)\;'+BQ( 
@1\*/C.^9IQ&NV._3":I6YG5J;%YR5W-R[_Y.Z=8=D]  
@2L4C5'=?"[K(@I"MV23F"TD2;;*]2V&>[/IN$)N\G.D 
@L=L\:"*]HRF;OF]@7-A,6V'30'6S_8;^V0\G@B]'S!H 
@[?/A>U)[6K:_P=TT77RSQI;):=@4JL[A,'OKR%';7N\ 
@/6]O;9AV,A=[A3GVR%&C#GC831&4O1*&+O62_;9HG$0 
@[(:YY;X$ID14BS.6-F>U IBCV_O?%%C%Y=-7*E!<[+< 
@[+]R<KH ER>IZ16F925-,,+)8\X9$E@6W)A@SB*C<D( 
@4P2WL&U] MV(&_IQBE*X;%VKY.V>DPZ,=X\.[2UP%[0 
@\'WMS4'4CDG#9>D G_5_F@DHR+]VYDESF*J^.G[%,-H 
@3S;NOT1N2;TK. 8M<A#AL$T6$?7/L*1F;EH%>'H+;@\ 
@A=*,4QBKGR01-XJ 7N2)JM>WYB\\AK_/-+YU4M1%F:H 
@]XQNT:OQON]D76.AGE'&. 06)V5V15/AC(01+JP(_8, 
@)6-OK$#!%LZ7&0G3GQ#C0;T6$J$>J[+_BUP5>)4'?"D 
@@@2+23575H7(K9:X_'O&AL%EAVX3"GMYW1[IKS=%"J  
@9DSSX=>*1(415/1*^9]=[UQR\-6^#<N'I@RQ><4#H\H 
@V%]:=;":A^,)KR(T>5_J"G=1BJ'.K53<=B5L:EEW;L\ 
@,B6$M,DHQ/1I6?<\L$CQXXD=VXW9T!0\'T2(OCS/U@0 
@MJKD@.S,%=<#$ZU%)KCL>8(C"-H!<M8.@9WZFVX1" $ 
@:<EKZ0>05U/C1!CL$[XF N;F39VK"M%=7 =7-!4N<#8 
@DS0.)L?6%_XR//G4%#:%XPQ:]P'X?+0PG*Y06/>]P]\ 
@CH2 ZI.ZWQ(LVV9U &60@_%;&)G(7T39"K*2Z*HXC+8 
@6O.O!Z&81;,3X[A; 4DJ7,O+)+*Z?MOM'[X[6Z$:?*  
@3E?5AWN@:''W.G9%/Z.<XZJ]09:_X5-3JG;Y?9L*TSX 
@@^-5&8!.%Y)TX9N1NA@>KRS7>I&)")6672_T'.XQ!BH 
@&%D@'*MA:_/'?1%$S,A9)[7"?^(KK9=C0B[8,-KSWG< 
@0.&I?Z1:=:YOU5A7*>\#(]U2VPJS]OY&1$QAK=7Y+,$ 
@@[K9_?W3M2.WR$3@P1?>U9!7Z,/C@PD;>OOY[M&CK&$ 
@K*.HYQK?,X.!7%6S.D_I MZ+HI@\8S)D[JB]6.O93@L 
@X*TXEQ^O,,%U6YIWD[ QY2<I^T-WM,#8M;5"0'-!'5L 
@/*U5X/7SYS?;9PSV_4-M/GL$$FUAPJE?9T4N5VC67=H 
@/$NRF(%LG/AP?,<7/'Z;7Z$9U%_/W^KW3+Z&$Z@#I>P 
@"T1EV[;&&OZ<NIN+[ [&*Q,4Q)P7/56^0ZL64NW6S>8 
@Q:4Q3(Z -[L,,3LL@#L7LH[MR:JO, *%2OS#V9N:_M< 
@89X%F$#,)<=I#<1:N8DZI=4C5##(:(_=2UQN>*;VZ80 
@D14OF-OB0!J!QZ&P%:QL=[K*DEXR&D^-/:?!6(F!W-L 
@/VI7$14077CBIP\>9B+"()P/"A@OY<]@?+-UTH"HOQ$ 
@>7T2+NL2_L/A%:71[)&FWM;@=%I<?=J /2(/T&/QLE$ 
@(2I\5BU&HNS[&Z0(K<^PQW,ZP+=8P",NR.#B6%ZHA], 
@$)5"_]("M9#1TI<HCI&@\"&Z;L7K@8GMF@K:""G$^FX 
@^CK2V##,H/Q]KB$C&^\K$4->=)TD-T,HM1OP,JHX/00 
@I7<]V4(M8P-Y&THG<9* IS>:B>3<+ >8V/8L^);]0U  
@[M2N%HU@,1T:S\#.#8R*D3'0R.I% R%M5PX8B4D?3K( 
@&-_"$2Z:MC]O4E!5KLJPBH?O;EF^ #"(S9+*Y;AA\DX 
@X"/+/#3+2ZYKGDX'49BA=7'G1\6/AT2DODX.8":?I;\ 
@@;=(CM+W9,D<(P<H_ G+%RZN52SG!<WE;+ C314@DU( 
@\Q= 3*)5*C-I9?;T4SPQJ).S@N<5-W<6[\MW#,DE!U4 
@$EJ'$CW?+G)-B'0-A7N45"J]P?A%]U>SG3<B93LQV+$ 
@D*Z&VFZ-'7)>S]$ 6B= C".20B T[E@$T!?L><)_K2T 
@=NC7"FB[DZZ=+.P!O##[I]Q,=!;"RS"A-+!S"YV WK  
@%]=!$6CG,?1 Y]GI2<NO72JK-AX$\?A0-_0E-HX(.D\ 
@@;-7!1V0\>GZE@]JD*#5>VDX(Z#/N3O'&)PZV;LGD(T 
@10CZ_^HF&$S)6.E5NLS.-?30/-X@#R[RAH"-8B?U9K\ 
@$3@( ,%32EAU9 + X PQLI21\-(&D(*;0GW(V!PPPX@ 
@-Z3D_%NR^R&Z>2XNP_E9N)(.1 3UU++.Z)UEID#*E>< 
@VQW0&=$XPUNIEEN7*2(1!RTT;QL:3RSU04Q!M82>BY\ 
@KD\:%LN,H0;O<P!F1IKQ\?/([\9)0@B+)8$Q?173%#T 
@'7DV=-;^-$ F]'XEC22&8,RFK W#+-&9)YBPPB\4_T8 
@&3.-[/S6 P4O)RCI_WL!Q<E'O6=G#0!!/Q:!G23DYMX 
@37N/?\S-K,6>,)#I#7(K+15.NMI&B0BDA*GB\6?#<X4 
@,O".@.)")>V+8$+2"M0<^6-%UFM#>Z YOO(:JO.<YLP 
@;*_F]"NMGI6Q2'9H<CZ7+V^OO%>\>LB"IUO6=:,?\2  
@8-/0]WRZEX@6@N[ZCEK)2G6#6P%%TY1<XH6SL*3II8  
@@D383LHU;#:%_8FZIY-OX].8:ZJE/!>9XJDRE?<PIQ, 
@AJX,.IX_8;]ZCK2I^'Y@FG JA8<8!>#NE:34Z5:30KP 
@6O+!9"TBA(Q&;P>WP;R%54/<GFL!F6<K$[Z\O-Z*,_  
@/>R)>$S:,WSRG6[ASH>2IH2CO@YMR&8 T+[T;4^RT&D 
@[\R$J(3O' 9XH@M@'Y< :O[9RQW7N_QLK#<8=1MD2!  
@PXG8?HEX-RO ^K=L=@ R3J[@R9P&@W-IX#9SILUS)$  
@2K&+.C];514T&H0#$<T(2MAN0??EAE9+7U&;[NX?QP8 
@;T^6&!HKNUIV?<Y-,9 [QAEM>'QZ[>S320CE@68'5]T 
@,?UK[.Z:N7GA89T?'5"6ANU.+HGAE+HK#LS*@)^7P04 
@&NV1A[&V6\-0"*P9'V;.]V8$-1J81$*,V#KXV.+"KU8 
@N(0D69#0&U(K>F9I*I1*UG06_A)4&0[5KG_>HSP#=[@ 
@X^XYCPAG!VS'WR]^:B!^;WZG3$<P5#Y<?CF(D=U"IQ$ 
@;[B,]OY$)NCXRWIO^(C-EA0SPG)='*II!W6GFQ.%^K\ 
@-F)%@'ZG"AQ'*,2P_= &%?6;3LZBZMZ!*B+B?(MN GX 
@RK3GC>M+0'U0DZ__WW:G?6)5?^"RPDN_4/ :(8,9?2( 
@N-;.-%;L%49PB*)18',NZA]!NOKF&CW,GQX5D-S-_$0 
@<H,N6%@K)XH]UT^QR+]W,FJ$%&KH)4 ;^)=/'G@3J!( 
@YL Q,OP=*]L4YT:O:30.DL>P!O^ANT=&5H1;![L2=), 
@9'@N,<&O'["*Z!4(QYEDQ&8^F"W,J#NCY*UD0F'RKLH 
@I9GO.N.U&*>*+^''&LYT05MA-&P+M%G*VX=5[ HPEDH 
@BY[I=VR^QB>9>EBJB/8^1XCC9K]F3.V[@-YL$:EYRL@ 
@,%L'L.Q@ +3\#OVSL(8C_">](O85EV;-ZQM#1\*^7D8 
@?XRN@1&<YD,<;2+MEMI-*TQSPWWD/2CQZ7Z^EW26=Y< 
@#]5S%[3Y9']!6M<B/28LTM@8AP([$GK_WO)7[TOH?%L 
@!=? I1>"=6@A9K/M!&Y(8T8$B3;W]MZ7R<EO*GB%M:, 
@ @O7.(,+TUF:DIW+30K0.BR7!J=L%+X=F33JTG=W*:X 
@RG=S6ND=V'<65$WPX 11W0_C"@"JY4QSHC/+PM;^6#\ 
@N-V% @VIBR)(SMQ3L$N"X&.6%$ ?&K%=?&BH&NTC'W( 
@Y4,WM44W4DE4K_I=  WN8$]QRY'SQ$MH[=%M0!A7]P@ 
@J^#X*%L11(9](_]OBS<2\Z_VN\M*1#TE/#FIMMO)A)L 
@QI*T)N>*V+Q&D51&B 5;V;%YY:93M-<"NA]9WV/(?78 
@MN7I4^H^0";8:CQ^P<C2FGXL6I< -FK/"%.LJ@-LBV( 
@K-^!%>0J- _0XW=#*DSHT3R*!M:NXA_PZE[SM)KI)-@ 
@^^?^1DASDEO/1V\)EM.X;Z$(+N7YNWOG[7>'Y.WK<HP 
@G]Y2MAV=<E'H]';M(?/L. "VN$NZP"?<$,9K%!&(F(4 
@>GP#FQ*OT:$5^-IXN<:YPP*(<AHO/_/)>VW>VP&95TT 
@A".IS1H"*Z]1TY$P =7N[<W-?CDY%KO@"GX;JM#J<9@ 
0[;L:X9G9Q8-&L>J,R>&JL   
0_/#P)<*JB;<@%BB-3*ZN+   
`pragma protect end_protected
