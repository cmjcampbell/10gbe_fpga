// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H_?Q?T[BH('8R9BRC$/$.5?AU O+,N-J+7)NRR'&*8N:7>$?L.>U:]   
H(L-V'6XRO[?%J0=NNE#H/W?@DFPRN184X:2EQ4)_=O@GW[G&<W/O_   
HOGUFMOYV2]4V3;[?=,.BM8K*R>MBBX0>1$,$U&-OG<<<7[U$CWW+Q0  
HM@EP5O=;IF;>KUZ)2\K5-95^=T;O.>^FKD&F$->>!E%:C,!8.9,8V@  
H>7O%B2'FDTWF-P=0X#6"E =Q$YY3Y\3N?%U0U 4]-#\'??G+]=!IVP  
`pragma protect encoding=(enctype="uuencode",bytes=2960        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@YIQE)EB3DW?! OB0H]/!V1+)4BEC!9.9+5L^^:*C[), 
@*_],A_9:X*H5RQO""<(G(X5@UA<'ZC3")=@]R RZ:3( 
@ 1EYOIZ9/4L%"]S/_1"F"]!=N*!A:=AG%,,9[RNLP14 
@Z94,K)/W*P?2H4 C8(C?$#\ Z5DGR/E.4>$@77S2H", 
@SS0KPW$"!]4GC!FGC$!+G*'(^&6+==G!8.U#8%6"^]< 
@F17*2:\J&6';A'K5\#CXN\/"SWJPVP<S2+=(._$Y2AT 
@*G> !F_*)V-O:,4CTGRD)#447;:$.[:R ;#B'+H1FN, 
@PH,?9JCKR6)S-'U-^WU%)6^[TY3UZ@RA'N_Q8A!$*%T 
@-,:J#;5'_O54S)@#4&ZH]OOPQS8B"\]=R\+7D$RYS)4 
@!YN<W$CAJ3E7$FY]KY/9R1IFB:^8/!A,.!W$H$3D.B0 
@OCLCTIT0I2/A5H@.1# FL/2AG7#>,4X.]:^1=J7!(F\ 
@#E= 093[%&2E!10@6DOS>/>,#[O%9:K& =Z,QP]Y)W8 
@NEG-H[ GNGK_T*,V1 )A8" ZNQKOW=!V>[V,!KE*^9\ 
@+G^4ZC/$UY;^LI[SS'2@(YF@2'J/V. PK9&)^3T$!I< 
@2+A^"G".KOR>ES NUV2^*!?R.:O$SL]HI/R,#AY_& L 
@S348R&&K[9:BY0:N\A1(5V_E5FNEEJ(^A]@+UI@HS?  
@T>]0SIV6S)&8@X9;*MJ3;17]?A22T@N:!%9WVWA[7\$ 
@9D=TB+E4=TI<O>L C*.9_)2GG4J-!3:L>K/A#CK]C*L 
@D"E[9(@/HV$CQOHMU8NOM]- 1CD*Q6:H4BT)=U:TV[  
@\WQ&?J)R1)D$&<<ES0E.";V8@7]G>=@X_@TL48@KD,$ 
@DQ+YZ^F *#/+P*3RBROMV+^,_;[-3?T-M4:8]'T-L9@ 
@8R.D?58FXXFAK?-E'NY10FF6(*ACV.*5:J3^"!!3A1D 
@"#3L""V+T#NF\)(Y]O"72;YNAS]=/[3@AO5/%>R6:P, 
@'TZ#R$\^<0>Z\<>*':Y!N*"4J.L]72]_N,SH\Q0+ CP 
@<\_1B6!1,7GZ[P)I1<6P7Y(L@R3]6*=BAP ?*'8 ".T 
@$51_CH<,";_?^?Q)@#:>E*@JP2EHEJUYH@',6\_JUE@ 
@,'_TGTZH>T2Q ZYC<C G9'?:&7 @FM)Z1VD>E*/=:GP 
@TS3P?_?D@^0]>G>U36O.-AI<6%0HM,H&R[.? J?8((< 
@AZ=&;/8M/!T&^W)W]@5.,00/9S=J/9Q0"]$)O^E[20\ 
@C#R*SH3\/>6R5X+B!",>KUDJ9C,&]MW+K[(*?Z&5 40 
@$4$1]V5I>%.*60E[KXE7S!D%HVX>V9C5'N&F P(><(0 
@!!)?H<JY:@P9'GTB?)8<270&D+2H_K[ERBK6(?!>6;H 
@\EG+1!C58$77N)K.-=,@5"2LL>]:A/K-L1GYMQ8? #H 
@D]-'SQF[QK)49KH%9^U=GIY(_!T&J,6%"+SHV-*!' @ 
@[OQ9R3KSR[ND57;%P,C5C&D@)Z2F?QB&".-DC8*>-1< 
@6>@,=E\^PD,J!NSG>*FREL+#'O=!@2@X<^*=6NF%,;, 
@N:D;6C%_>KTO#/S?W,HF!-N=-1!7XW':(B++;E59:P8 
@JQ<6TSAI;.0YDN? FX2FIF#*E56AP,J7+<MP4;"G:\P 
@TA;BQ(V:<R]?C>3 F_&DR:00 B9AO4Z8 1=-B-H,WID 
@/?B >)8R18P?5"Y4\]UC=&>GME7K; ?C:;)KKO/<;>T 
@'4T#22%-B/_U>B;/>5)!-E(U4U'O%=L @WON#XW!] @ 
@WK!!R(GJ2*+F/<572(08=5WS/4(:;'IUJD-Q/!5=%1P 
@W]KQ)H0OXWD<QPM'QXQQ.-09W@IKLPRM?I6X.VXG3A4 
@'L+9[7HQHS]P#T5K.^7!*$2BGP"+!UJ66USR65XT 9\ 
@#O9 ,4S!5VP2C$M?E??^U/XY=]JVCM%#90/RSOK?#&@ 
@4R\\\@_%4^72]M^&D"+@4&3D8*6!OB$1^#:.I9FY:&T 
@R76M:7&*Y.S=I?33;TW5\51"6JV+9SW2*"9^W)RV->T 
@%$2(]=S>[)^-RG6RDW]Z^78I1ED"AB-CYWYO3!<MC'( 
@!RSNA%'><L;]$AJD=)F!4W2):Y:EA_5#RV# @V?3+.@ 
@\L#:L\?O+^<<$$KL/FS:!\(HH$\O<LDR6[5>KY6*.-H 
@A(D3$D=PD+X#55(&?84;.C'B1DM9>WH;,TW[_Y-OE>T 
@Y63Q$C!S )J" EFQ?>0?V(.,7<(9GD/[DP)>UQ@VC?@ 
@7?A1.5ZW9Y[C[X=0&ALJG>L8VJWJUPU)S:-S5I*.RL  
@!.74P KQ2N*S/,,GD) [O_'50;R77;-(D^?)8&HM>G$ 
@EUE3B51P67F<6M.*1<['KRWM=!,<1-!:/#-.@"^1\K@ 
@&Q]K^E;<!S:%YFN'IIP.$LNLJT3>=2%[!(%CU<O-EM8 
@OW\2*O:1N\3CI:D\*?)_B$;9_P?? ,$021BE*DZX8#D 
@:(00K*9&*H&80MP_;+'RV?N4VPQ?B=Q5AY'_U^SP]5@ 
@>4*R)G"/ NER;AYW22JBZ XD]F^W[A7UX>(IV[K&IV, 
@^@"B8QK<A5W?B?%MFEAWQ6Z\ Y%UKHN]5>LF"D*Z[=L 
@;3!TYA"A$YLZXP76C/7\]LSN=XD3FRE(2C.49QT.-$\ 
@8$:NZ@PBU?P<#7F3@R,1(*:G5U/RBEVJDA*S0L+?(UX 
@ED.3P>I&M5GT&)J?"]W'#XY(.$4BKT#)Q7-G2M%0.Q( 
@G:1I=NA#.C>@D<O[XZDQ6#T-MODSJ@@:,'\U)A( AL\ 
@7P!8M]2&^WOQH:K(KKVGY'#6X-/)F QU)5),_G"%I*P 
@\X<.K<#C+%TF!]BV4</+7:Q6\30LO6GAL87:E4M2$00 
@I'F=1=& TY0/&.0]^4(_U[.9S8G3O%A (ZZJBER&ASP 
@6!##>;:F\A7S)^$J4)WD(\??<8NKB!OWN[H_0,27#), 
@_VTX(B@;)\B:F]3W-?JI'W8,^"W)$W=H<,NCQ1]&0>H 
@'AA^=BD@.==(693"^V.1UCA=ST['B&W6\-MZO2'P]Z0 
@*.GVC79G7K626YE./.,]@M_;F8B/ZRPL]+RH10<'V-$ 
@9=^8ISW29A=$A->.\5TYP\4]<(^WC=Y.=R^)X"01MU, 
@5NG%<Y79^A'2^F!<T\EV-=&\/@+W'.)9 =06TL:V15  
@W>* P#E>3F=%&U=@IJ%Y'#;%A]1S*:SNB'/I+Y]R1HT 
@/^*-94G$S<B4^@83I?\8E(&'4S8[>5]R"K7CA''TPGT 
@!P.W<=N;_W:H"[C+!I)/!.6,\O///!.*Y4E0IXGC".\ 
@:U'G(/)'JU:5N(J,LQ+#SWU&.;LN@^F\S!JXZ2-YO[$ 
@,M!P;UA)Y?$G#OV*$R7W+Z/K-59S[EC2:$. D&:0DST 
@A(A),"S3UR=[P*@OA8O(=:2\$TM"\M@(9@5,SSNR '8 
@=W7US;?3?'3"71KF3JX*"F'X^4YOU!?%?$B@U-%LOJT 
@%&(LF-@Q$R(R_EFRM>-1'[C>K6)*O2IEYB.E@&6#++T 
@7$W.28K9C$?&=$IFD<Q?TYA<%QRA"%<1QQ@V>]]+P*< 
@&+R360=U;$OJ$L^R6J2?KXNR<@B*X\W%#2'P(S:(Q_P 
@ >A..D'A?H(G#JT:L&E% 2>XDS2>+P^8R-&K B7DB"T 
@13,OIGR;F-P3VA[T%DJTA\&"*QX_IBISMO?Y>@^(7QP 
@K=KHLD4@'/O3\RU_]^,/(EG$\9GQ(A X)[U (YS]+<  
@<ZEMN%(]!!UY 2X"5:=1)H]<H Z19;1_(G[C8(_6WR@ 
@R*>_'4 VS&.3BA[6%IKI80+OB@V"#[E?^$C.5$/"9]  
@I@8:.O]%".UW.:E_#IZXSIK&(2DLPO'V@5Y)>[99WRL 
@S &:A#@Z4EEI^<4;):'21=NLI;#*55RL,X!#(?Q^\^  
@T>XFV(TWU1.'B98/1JN[85PZ%W/G G7F18*#:G0 &J0 
@*IHB*3B/ 3TJB#Y<_F1@P@VUAU66XT&V2(I-.=N"?$T 
0ME9E*SH,A@CDTM?^"'2+]0  
`pragma protect end_protected
