// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HX;'\U6AF=3?DCM_JM,R5BV%T&8+7!KE#?)%,AI1J,$/-GC&&#S9ZJ@  
H1%O<?(V:MT7K)F"^HYIZM;D YA#0@+YJ_4[\O(^M2P!3!=LXG.UF-   
H_ RB/L>D4'R'[.,A)/)7+6*!#>O@/IU!2 X,66M5VC-/D=!+)CY\K   
HYO&%9*/#&):SZR6B-#0!PO<)D6TVV9SNS([-_,@E8PEGB]UV\$NA(@  
HRM8*Z42A,-P-'!'_U*\<;9F%UB=H=%A0+XQYI(0M8DM#4S85*I.SM0  
`pragma protect encoding=(enctype="uuencode",bytes=2352        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@WFU3!#=9J=/K,U$/>&L$FD=5L W.YBM:]TB_M.*/1M8 
@%K:9445UF-B>5'=K_?N#=8CD!MAR_-5\Z,:,@P!JV8  
@])G4U,#,JL, HQ+TYL:+ MS9:J<WU^6<47/,,=1YU6T 
@-0)-FY<F6EN^0WG4^5[<0]\,[:F++VR#DF%P9\ITF=X 
@[,9HLNHPT,1E-)SCY+T!8*]YV+"0)%59(6X350L1[/H 
@+LE6R<GK%K=]>#F$:.6W%#!APF^^CL46(U=K2+<V(YX 
@3%*2GQ5YTKP&Q^=E@O2%G69Z!Z\N%?"7Q[*.N>1'_BT 
@C.HBJF"TPM&&;6/?"X/2(3BLQ3L>WU(<Z  9V9AL6=H 
@I\<!^F) GTE'V^>6TF10TO]LF,IG)3SF=&#S!5Q.)]8 
@*Y)6WLHV[OB%;B;;5FH@7]Z2;JP(![OKU-?(1XB)W%< 
@8&O@G\C)$B.E"I1-.TT@"0Y\-BQ.X>QEZ354 @@/$-@ 
@'Y?P7O1L.J\X!G+T\OJ8Q*(,+RV:]WJFFJ(>6Z+N 4< 
@>:9)K48R;O1P)X&) !-B4/QE2Q$WY#I/J.S.?LS:79, 
@QW@TG0@ED5P^>IRB@%.$[/),-FBU^[Y$HQF<3]XXEB< 
@FH 0?JCHE.[?9X 7C4WBE]QA8TL&()BU5N"L*2^I))X 
@%3"ZX7;&C8;<PR.^1"!NN$\@RDJRUC&Y,GA'V/6UF*8 
@$UK-/>"(*O@^*DX^"L4MM36.GNY&QK4;@7E7 L\>4WL 
@2SGE@%BO%G?=-'6=/*R<-,X?SU;S/D17(;DVZ,-:DHH 
@?1(=1/"C_<R;7AYFXJF5G[UOE?Y'C<=F'&G*^D.<3Z  
@KQO,!<QH-I'?RX@6J@S]EN_(!5+1\U,BL$'Q>A!<@]L 
@K!/2S !6D//%"I2);3__KT3TEY!R]R).*-2N5I_ $>P 
@871F\_DE< LB(#/MQD(2-.;T:6^E'T[+9L-SY[2(\Z  
@K)STW&MR&>"4 6QD!9QW KLW!@P-_SW^OCR9 3:7&70 
@<:,I65&_7,5QHXK5Q5BJNAO*%%?)#F0I?;)?WB'>[TT 
@K6ONE<'.75"7E&UR BJ)P-UU7NDCM][15$XEE1^"A;( 
@G;%:2=4NUF/BEF25;J!45#$9V;^B#_TV\P[8D__.1ZH 
@)C9!)1?"LVV@_NF"V<8-(6T1A\:44Q1^Y;3Y*R-.DND 
@S! >MT'XY%3M 'F_)?<E-5WDNOYS"!ZJZVH<H]GKU%4 
@UPV;J&*_#Y:D].Z]TA-^*^P)8CA'[>C>2@QNYG O\KH 
@8[#3=J^T*<5.GJE:\JG"-TE<)&-Q6Y5MQG+_C?X<3;8 
@"W;=0^:43AW=(!>:C<&/7O7GUU-V^E9!9]Q?(+:J&%, 
@[69_F*'ZQSL;#[]63Q6T]?%G*WP<:,V0EK^<],N.X7\ 
@)*I8-4XR"]U!%6C/:#?^1=(X34X1GTDF(4,%.RC-YH0 
@B&K^_)H(A_DRC2IU)X 3>N<O=_!L$;3<T:F(^_H"%=0 
@TE#+\3.E./CI5*)P;1-?RG'U-7W$X_FM5(>$WDB,HG@ 
@!$(29H,S6E.G<QX%WKTO^45CGGX72?@_]L0))SVNW]0 
@]'D4\9_S@S72^=9Q7^0_C!/!&4,N^#!OJM7,.4L*.?4 
@KCS1G[K]G[.V[[RO<PPF/3C&CAN+ :=.07==)\-<-S\ 
@8G7BY!@H7Z!+ 0ZMYR&1U*KF!>AR+4DIR1&_9ZDZ2(X 
@FDF01EJTVO+,>9W4OLOE!CF#"O_^3:8H@:T4">'I&J( 
@+KD%S#D2^\,\//ZR+KJUZK83E8N.3RR3C3Q.E]SB*Z\ 
@#'07%)>;&!II4YU\HN7*-OCFN0?%V76)+47-L[+FS.L 
@CR@[-[3RE=P+/IJW6RW1$2N42E4.&+S=ZM)#29C\6V< 
@'.:"=)1;8_*T"+\B"^F^7JXX>4J!;72B871D%O3D^50 
@P2^\F-TW@5RLR4ST?BLZI53D^)]4GIC8MXI'T8W^";, 
@>>2O>*=VB7BX&3)*'C% 3!L<,'#MGX231VN>H,JNB-, 
@V)R'QANK[%N1 THN+SJ7^[Z4^06:0/VQ)1682AA&BE$ 
@*US9!C*SK,.BIBXX%++RY=//*D%OMSA^ ,=%ILR%(M, 
@_U1!AP1C4GF[*/OF64J_6UUG'7 .H(RO''6PNLJ[V8, 
@.^='5:F,]KTJOF. /G27_*2!#1+532<YJ[A-=3XZ"?$ 
@.BZ"T0[O.6._!5C4.1JJW0T&8@D4*S;W4D>2RT3X(/T 
@[':6TR,*5\&YAQW8M.RY,%$?H)FMQX$B?JJK 61:@MD 
@F.&U,!;H19N3RP:M@3K$^.9E,0.V:_'8LXY\:_Y#$WD 
@%QYJ= @5QBOXGSL>ER<HJJ&,<13D?X/6QI^5WS='-:  
@(;"!WJP^C_"YK0Q^8MWWQ=9;H31R8@L1@F(HV$'+S&\ 
@.E&*[ HM$U/(9<3H)&MZ6(*G68?"0>(PHNV1&\UN68X 
@6\?PK%'2MO!1)BP\H^5GD:08FBWJK:.: Q4!A?5 <SD 
@*!PSB4;'PP8Z'?_I/)E[1?42$_&/J23[TP%ZQRP?/D8 
@#ZQKG1Z3<KCH)Y'/<3GRZ9R1X1P1:EK?Q2@%)6#+L^  
@ @UZ<),\"'C,'+W+#ZN+8EQ+:<7Q-0?C"6 .LI(F638 
@AU)J"_ Y:A%+7C1MA.!8C[T+Q$YNZQ& [?!J%AK'D94 
@,YVB^4U1N]_V"&E4Z](ZU+DS*.H[2102:=%TC[PP)[P 
@N+/\I6P=3&-\&[P- 1]T ^C#J_0*^Z>?>)'HB''HM>H 
@GO'&^3NG'63-%1X-L&OX'NUFZRI@4]6?W'8<9.0OQG$ 
@'M4T+7N7]:^WQYQ_]U7%K[/I)!&GXP]2R3GD<[^E&C0 
@TO9<!Z_PQ Y_R8XM32?X!Z1#X^4.>]BO2OIR^_?+D1( 
@O&??+;H%7"F(9HXUFE#HP;GV4@G"P+P#U'!:5@:;1)( 
@=QV0GI]'<+B*ZC0(B:T!2P*P,0J0[D=RTP+9XB<EP\( 
@8*4-:I-""(NF*M?P,?M#M&]]$C3@:DUU]1V(2KW)_2( 
@2L&=)^OJ,P*#GDU<JSG<$T,47/B3W3".5;BSAS(.Q1D 
@@_P7[I0UK8HJA/5.F(UH@+TC(7(1/+0<UA(O24%$**D 
@5G P1)O95)@-+G]*]J-P7>!@5 OBS\PZZ7UY)DMV9E$ 
@1OF.9%^/==7GO"8LOYS6C685J-:AK!I(U9F)#+P>JGP 
08@AA1U%/FH,IWR(/G)#3GP  
`pragma protect end_protected
