// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HNS+#()GMF2J1R31DIO7SOM-HI>DE)3\CRX-R44^GPI06@QCI /$$*P  
HQ;;74G&XYD*9^(55-^H/+(]?JP@Z\N9AAE"S4[=L)HK!W<KI+AP:KP  
H1PA.=OOC7;[JWI(E&+?("1'Y4U_:Q*/<+:TG3BF>U>4@W<=[C_TV>@  
H"TPS]4@;IX*^MP9$FA?@H7&2(CX.79]:KSML0GXI6&H"SPT"?JIT6@  
H6%QL!X4_.O5_AFJ.L;'WI1;4EV*LF8U1"/.'!_![1"IZY;_94T]WT0  
`pragma protect encoding=(enctype="uuencode",bytes=1920        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@R@64@M_I$$ H$X0/2 -W-BC!@F')IDM=D+W &1E60,$ 
@ (;;[7Q&S[@/9L:12!4N:1+JGHU0&IS]79FE<%RK<!, 
@2'_<:KYQ'"$PV2LJ,AE.G<4< &<15VSBB_)!6$@/GBP 
@-1/8$Y^^!Z V68JF25B/Q5Y/??/*^1G),PMXD.!=Y*L 
@  ]_1P:_6XV>(Z;T_U6$B91+P@C\(;I(^?JQI5?KZ", 
@#Z>$EJ=N%!2H%O>E'-M4C,[('_N]P]"(&TV1]?]M(G< 
@^#O.5D1$H?VN97V$<"N1FO5K;,M(5A)V+; X/U="U-$ 
@"^M%3CXC8W^D[6:'^TS[:A3?.63MDJZ''^@XZ[XEW#  
@O8&(]FG@[9F[.P!]@^,[<]/#J6_LG:DN"N!<:E%)L8D 
@F74-HP:51H 2"?"*7?3S6XI05ZU(=9/31-+V>:AZ(V$ 
@OE*$M0:YU ([=WI@"Y_["C;MZ9KEX/:9DZX6R5)"N 0 
@B\ W:>A"= +#F<GS9 B:YW&;EZRD83T<.X;CLH'[/3D 
@E]-GN?]Q#9/9<TW@#1%_4)*Z)C%SK>19@DXD4>X[98< 
@^(F"TT]2@=Z%/R'K[G&_4\G=_G]5I+K'4#+"OQ9DV,  
@:1BBP;*H+P[5.)??DAEM8D3E7X2D=T$6_JO*\':[2,@ 
@0NY]K[!I[=E5=29'#VC%:X 4YWMF$028L0I5#(J,.V, 
@/A8_-%DW6-UJJUN^G+4T=>)6S:XIG'U@B_BNXG1E2>X 
@?@X7<&,G#<4_)B0F1GYI+IZPV%U)>.Z,- N<%O0QVY0 
@7MJ)RCQ#IZ("@\!EFR-9:[W4GMG0V2N1$=D8W"_5R44 
@\6XO.#L)X;]<IHUD/PN&Y<YD3B)I\91]"0?%/V^$:B\ 
@'K2=#JZ1#\_])P5,CV[K@-N$O7&E3=MRFMA>Q=BB-=, 
@U[;N4R_1S\WT!5[(4<"\4CL-N%VW5#DH3I-=Q1V/)WD 
@#=P&Z$4>XRJUV]-6Y6\.2,Y_GUW+@$M=3*(BN"#8S$L 
@AC=/\E##X[\],K=*NB+0D2ZG!JK$E5^FWOX:DDF%]I8 
@*9Z;3#RSWN"C""5D* E=M4C=D:M6&@DXERY@")A\_RL 
@MP.K)9*V0LE.OC*;Z9%GM*&-G*SU^ZNYE$T1%@:&R:D 
@BX-E;S%N>PF_66-<MK$9H4X*0]6-E(35HA3A='T<9JP 
@?M/GB[:[ ?!(K670(6NCPFFW.F48U]%$*1VX#;+6;L  
@)20:ZO:A*V/-9+ZW"_.\KFJ.UY*WUI9-Q3$\=,4[2>@ 
@6;W0TV1FT?$9F?8F?&JYK3I9_L"VYX.NE_'=*T<-S1\ 
@52R[8$AXS6'A9DY4M+[[S3.,]$:5*)4G06_66E7$/BX 
@3):H\J7SRGD7QH%4Q)]#^ 5]*K(8HDA9NAK?_Z637U< 
@9 .@3=/L&9&G1VEPH. "\\\FG,SLLV4%*O+HFUS,DOT 
@K;#ZSPLG^F !\\>N83:O-$[0''5MK^?!XE+0](M9'Q, 
@KX C]#(OB3^+CBO'P&Z6@K;KX:7\<X@,P 8.%5*':'$ 
@.RNXSFE'5<T3$!]/HT6#DN?JV>#SG365\MJ@.5?!T\P 
@X$TIA1KT3#5V^@!H1(4T&Y#<<R!VVSQ^64V.> W[S;  
@XBIVK](*V.96]=R^H,N!NASWE7%^_]:*K#_0@FSG2CX 
@:UAW!.R ?Y*=;#.02 A5-IKGS(.DIJ-\CW.2X=C2I9  
@J*$6ASQ7^-9%(".0V[+Z13I1KZ9U,VW)A>/G#>+K+O0 
@M/BAZHD8+B#M0(-F9FPNL09]64D0I^V9?.JDA?3"86H 
@%=,<2R],+)6(*D_31LF^:$I7M#CPQ]"M')]M+LY_AJ4 
@R)D4)R"BTN&0BO6O0'*IM*8RR+Q7+%AE%B\F<$V5!F0 
@, QYV;/_F_ZY<]R<E(IED@2JA$+Z%1]<;3GN,UFF8?D 
@1:,G@OD"+^O?1?_F @"2@8#G\YYN#+.]?8YGVZ8*G:0 
@:&II]<K #X)[3PD[G""K?AX25QO19#OA7Q;.LUK*L5, 
@G'1I-PB73P_WB 2'+:D\S^L=+62"]X^S"$#8* 4!80T 
@8+7@".W4U5NT?L%<R.<\3 )-H6&>[WMB\/G5MA:<TW( 
@K(7AG6^7ARU41IL4!6!=-=&E)?GPS+$64I18PKCG66X 
@9M^=G@/Y1C?'F*M H-[Z,&PPBTD!2#O9*A-R?E'A&(D 
@L#8.DO99S%,WMTUB]VM\?EH.U%G86/&()$54&-.H<$, 
@0]*)"0^1#&^DP/"2>B^M[WX?/8J1YXPHS2MG^B85Q#L 
@*5OZ/I6<%[A@J8UX0ZU%)53W.I,S.C43M7OI=NV/+U< 
@[*I0+ SD#*"58BBH6C74?;/KA D+$7T7(R.QN>O%[50 
@D5):AZ1;2Q=SQL9W(Z_,GM'(P:C0(YI2S>A*UO1J&RH 
@!^I$'"U2=[BUH,X;K5V+$ ,5NCBZQFLRVWKLXW>J2S@ 
@(J&EY3$ 4WAG A(\(?#TDCR9@$<9FLYPP^,7LI\A=8L 
@4@4D76$OAUD]L=H=;N'*_?.DK7J=:6'#:D<8MUP'[+4 
@U$!B8/+YZLY=[E^8' @=>.-XHN"ARPLLU*2RJ]W5#U, 
0:S]+%D[;[8RRSK&<8\LT=0  
0O8PA85+BM0W?\9:P2%$71P  
`pragma protect end_protected
