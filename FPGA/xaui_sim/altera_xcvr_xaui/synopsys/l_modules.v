// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H>ZSD0I=A+B+#"]8F7.V[O<I.8%F,92S4J3;#\*SZ%2!3Y$EF%4^I@   
HOA_)D+Y2<^+'GIJ%4X#1DY')UY:Y)O*2LMI,F$HW 7.0!SOMMZ53NP  
HN%)MS@A49_D2EB,)(R:LI:D+@U=VY64S/?\Z'4/^,JTW.K-^M%7#1@  
H;*_&/K'$SHF5/R]M_5"T'H*"-B3*Q2P/G-:>?8'$=782EI80+3U%CP  
H[WR24]K#! "*03;P'(U!7#$>I\\M*''O,PS*K-+\%7Z?K@[EI[8*L0  
`pragma protect encoding=(enctype="uuencode",bytes=1312        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@"()!8^62(ARVA&*G@6"_:<H$ C[+%T,&WOW44QVZ&^0 
@LUF A ZA$'KO;6A$AX8'-T6NG[-P^\TUWZY/539U%/P 
@;,@):EB0O,F2R#OX]0K*Q9V:G8?QF&/ANX/)6.-A.FH 
@5"VB_>U1>3KDRPX%D/U!OB.+N8O&VVP"U%^UP<X?<%4 
@0SH_/Z.7S($9YD&P#7&:+ #2-S?H D]UJ?>434B>C9L 
@#A89(WAN+@C,PP<AMS].5N2/*_TC@1\!H+I6;$6BBD( 
@L7BO\\K4\DY+GB,GVBO/DA:T:B,EI\J&*A1"]\%=\50 
@ 7:D23%'8?;W?61+"HR=V4^T6$>M'31=BH)=FL :IGX 
@. =_"/&*!_5*,:PIXV9:\FO-I%I_@SD89XZ5(FPAOW\ 
@_:JH43=A>CU_K R60L/K+\+.DN+^/V3!4<;LFAF[0Y( 
@ :R)/R@@C*:^+C86&7%FL=DOYY31KAR5P257YT<]!RP 
@Y7/]KZO%UE>#@>66X8 +7C ''$1X^*^S]"+P,Y&+6;@ 
@C7F4JZ(T^,"T"+AA6SIU*#6W[)?Z+$7&6_M%.[X69X4 
@J5*FS0/8V5DBA\K7LJP^">6&B2FGE*ZAZS2^#QZ7O8L 
@;).?C'COD2& :@'/0D+1,D.93F4(D0*OQ"0#QSKKQTT 
@OQ$@N*$199&8)Y4N6-$><ASV=G.K8X$970DJPWL1FT, 
@X"IY=9I^Y7U44T&LVT?EJN1C7U[@PU3A5^_(N*F5'1, 
@M(32V1?#ER0\*U\4\>>()6EVFW>ZC<ZG3,MN3G P'=8 
@NM%S2:.\/CR_.]Z8W,KIZ!/]2[L%NM*LW1]YQ>W=PGH 
@5OQ"A4.WY5-XX=KW7!PK%4!X;NYXERE3$F*9 DD172  
@3A-YO4N&C%<<\Q4$<VW^.>$XWJ:'],#;;7?L@CL+$F  
@NC")^K5U^UPNM^$=T(493?15>)X:UR$ZD-H.:H=7>_X 
@D*,Z!(Q%3*;V*L:F;\HG(.2GC>EQR5M=4J$]Y.4,GZT 
@:OYS\H8^Q@$,-U\#4=RPIGM BL:X*K$#29+_@OJ)I#, 
@S<TGF?^DNA;HDNC =%/1ZY:]!XJD( <P5G^H?XT.HOH 
@LT4B9["TV=%Y;PJ#9HVI M6RPUB7AL3^:N+$2U)".ML 
@9VV83Y/:9,+VW@0Y7(SJI19!,,/TP;MYF[)!:7@$_E, 
@@.7BT=8;1;*GHU>">$I*_]&KAE+-].S0#S_#OTK(J[@ 
@T0R@OP;\(@],29)"--"3DE%)>[$]69?KR0&$UF[8<:, 
@\,SZ04AN?Q#27T]6.1ZQ225,ED8VW=MW:"WO]R&Z"MP 
@&5S#HTF5P&>04X&3H:%,_$F-M3#P3OVKR]$N,W]YA?X 
@S;($ERS#&"?2PC($.?>9?LMXQCLH4%="7#F@(S0F6=D 
@3U=F)C/ @]GK7(7TEV_$!!Q7+*V(0:T?58;"D ^N?D0 
@!6S.<"=;^HL0EA/S$/"5?)YQ);P@TO!V2E3#4HDR,@H 
@D[5C?4PX#HEMGO/SQZ4XO% :J V"I&2H#%O8$\8[Y:\ 
@F,L?F]7BMR[W$CIP@CW\4WXHS3!*B(RG>G08FG%P7/( 
@""P'YWXA%W4CG$D3]')-PHN(I )78GVUK"<'6$I?ES  
@K:WW=/RG"<D&ULSI@0HV/JH&(A^D95;"G_)]#1C/V?0 
@9^:1UI2:)?+3)%#6%J8G)?7,PM/3(AW!KNUTUP?87VP 
@E4;;XCU&4@.?>F-Y"T_J@7U.".K1&_-]%_]T$7%RP!D 
0"AZ4>U$WGNBO[GP#TYO J0  
0U_NIZ5QQ4?:WIX)-BM)O-P  
`pragma protect end_protected
