// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:51 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
KuivlB4Amr406euJxhfYaLLsNyXIhDFozO4WphB4b/qI5eh/NzjzgkGuiKmq1vkG
fkizPuRVAUoCBAdJgwnu2bQyQ8ie8OZnL+vD5NA5ogKnH99ALUheXNyUfHnBpqr8
Y9VUdXLUOM6jFWResO9V17Wc6sHUbe2sy2VeICoI18U=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2992)
cDezrlt9mDS2Z4K0c9H5O0IZmyH94W3/k95sw2jmDCkTZG2t6sjIvQ6mwX85P7HL
MdLah+giLGJaSkW8EajDKI4z6C278FqJqg+HxpRDwtM0bx+xhlezkN13+ZjdkXCi
EHANtypf6p83k8yztdypnrjKHK+RJm32W1xeM8OxoZhDZck2faOziLBQwKqv5jLj
XslPpeHNLf9WUxbbK3zXd7nEWK057UNyZ1rdqeRAWNhuFBbLr6IPBCZoaBvLMlgO
1IfXQMLIG0gI+ptLIduw88qlXc8YsoqRLUH01h00lyOmZDWosGaJ8fgsKjnpazAO
Jvyk939NDe0boGovkUMagHu+u1isN3NkuXH6nUDX7Uhhuv+JhDfYtwU7DVC8v9rA
Suq2UmoJz369VrUbevuA6eH9XqQ3Jsfidd9lofA4bszqs6ulELPF2tgZnTjGvc2R
d9dM2ZKUa8sk2cbBy+I/dmAbv6qLRlIGInFPI9tC2kJ2taAs9f3Oa281U76lx3AM
4Vgn0PyzLIaxdvzsc5fpEeG8teF6EOQ+C/LOz/zJkN7ZI5JefmhZFwYgswxSLIwN
vqHsw87lG2bkHskG9AsHW8nBkm8wv9mcjgfon/9asyVGps0r5oYX+kLAldLz04pB
4CrAU+6A0l3yYGnc0Y4pOe3KoF2jptQmCzmf03+zh+p0d4tOFmn/nLAd6kDfAEXT
Ad67lITzjyWv+VgFMS+4pPjJFjclOoIfpMJbR2XZBy8CBfmlzarupRKpmxeyrqkJ
g60uf3pKDmlKno9pqCfda5C+d4oPvkdWhcW3i+9Xi9n+Py+xihhYQRyXYnKUNFCe
89sAucTRqIAufc8/tbHweI6Q42XiR0RQ92uSnncHwW/WdnaZwZRNPqY84JVR3XCM
1K1ynsmuXPOSIMdEe4g9Pcqv6rgJVq8i1XJwPMS1D1zOr12IqljFDkMDHz1waTnK
2jcI+gSWGyOIvGQjIkk3FUsZvtEziy+6phaxD0zM8yj/jb0qsas/2JFNKMKvmMt6
uYudK4eixKprQ0o24zvstYs0r2kWiHWeSt82RZmspIzA/1cMt4C+48XO5JNkFTkO
0TWU/YAm5R7KVs8HW6sn8KpRMq/7r9U6MLqaVhLfc0b2ys2mR/XvJ/gCtsLrpQz5
RzXmBIh/7ok4HDc+/+rEFNDenoWj6XuzfkldPYFfCX+YERghcRDk/FN9JquPXATr
PcEaBDN3fhhe06D0RRjMILJBbeMEsD8qnmb9hmKraqT+a1cXuA73LNGhHV97yczU
JkMCNQb0cjUL92SglhjlCCPm8ZlHsdSAGASFUfDEik/5IkPmpyT1QE68AwYJSzjE
zsQaVCFgFWjrE14VhNwTwbSi12gzNrHiRDqXLRoGubcpDp8W/npI7nwIjw4A4hR9
6j9btjh/yuaeufDPIELFelue8rzmY41DmRKxqFdWxQr6/ySao44bEACWOsBQ4OhN
a+DSp9u2qBoDiONjz4pnXwTg+Z4ltlQXoW9iUQBAv3SO4+pF0ewk+6ejAz9YBLrS
wxA7icR8Vr7YzY8AtoLNguzniCgdSlYbs0tbfKGrY3s7ijAIXz30sfkPv+gXQ+WL
P1tjJWp8k73yzMlu1F5AvJ7yxWHwvTxI2nMfEJxyXxkbeexHwjYESVFBSMSu4S6C
hs2X7I+1q6KHVWbvszWirczaarDTjF5mSvJKN+mOTUqHsmn80Y0nyECJ5SGOIhy2
HmRUxBz7+L53Gbn2GDuLbvNtObVVIOmCS6cYCNSwZ+8Sou2U5wAdVfe8Z/QoK680
AYe5ZMwIZc/+UUtkj4ESaerM1ia3067kWUgrwFQONHhQRK3RhVzHafuqkI2mTrMo
SgXQucz7ZCXek2Glgi6lHIMm7FrIeosA66WYeuxnE6XOqmQaYqKr+ANX/iAy2YU4
S+HZXIqc1b4ki8uaWvrVK9clIv9d/vzX53T190LEA+JjgTL1EY5MUA3awfqGD0P7
wPAytmo6tYTkSFdrn8MUIq3NWkhjy8/RmhuyozBKzV50t32XcNwV0Nt5x7kkkU8S
p9N4f0feJz7ZM88W+B7uMu9BcNfswXHmoDAe0KX6Wpfs7KjvLLKPTRWqrHGhe0/k
OR7khxIiq+grhqJb72h6vUAKfmfg8ilM5UX2o3YrbykmXh3ptZU7RqqollBrbjH3
hIOPiAt0lgw20OEFy4TIlJZK57kLxuvwQWKjMIwZyDqedwFDqEeOpAPTSbAD3vK2
z3yEv21qNUEJdJzpbQpfB3IMHmtDVQr39PQPJZbXfkdSQeC7Ar/jqVPTl4+tvUJI
sqxn/9M3BdTPxdmPXFg+qem5ljDuG5Eqyj4LjPJkwspBbJ+bCoAKQGbjDX5uBYC5
FfwlgWE3qWBlYU1YGasSJdkvZaLpA0oXcB8Gn5l6mJQUZV+os5c36UBe4CaPQ3aE
vn8EBB/7LmvSuMYykA9Kl37EgPF/vyEQhVJ9Dy5+oTrpGmUU8QJNpY49k0qOIays
KgtsPgIh2+d5K0LXe/xigciEtygsflHBJE1hlpqUU5iFLC2ahXVNR+gELxFxHjMM
53nj+V13U7VCR/HyF+8P/no2iJ13ewWy/qotBs+wItbilCsUa5yY2sG5K7V67N5t
coliTVg7KGWW8UMrPeTRaGSOPbsriOTdU/TEmKsSJwIkVcYv0dg+IGKGFL91nQLh
0f1CJOTyx4lzzjKfLgKKyzmrWrVU7PX60/y2/pjOXxQ6oQWuB/JmSS/FYyA3Senx
G/cYS+qlxo47zavP7tOIY5wfMlX3QLhWgQvZDXeFe/2trqVG+ef32YxW51wU+JDc
jzsZHUry+ZYqVINmvp0OOS8AH2lCaJWqk3CK58arzGUA6AOf+tIPuTiSyybWTnqY
kTWb4S9iqNUKCrA8W7gS8+PLjaK2IVxXAWghq1+sYZ1NkR5JJTIqVqs1dZWQdzxp
Am5v9+OLzJqFsIRUStN0jPXWYCdSZv4XaKxtYd9bk4r+3qXhYto+2aRNNYjVi2gK
q7AEV403IrGaK/mKFL8oJrCorCDIOwJL+k+oUWyPv+6qBykMNam+D87t9DTUMXFP
HDRu/tdKcvcQR5RCSD7i81v4klWvGbn84zueZ/mELcPCC7pmJK1tHuT3RVjA+leq
9t6NfF2U4ijZE95OfXzGkU2QUornLU7ftB+JUPAbEu5CW3hCwUHGc+8tC+4wpzla
akOd+Ee3/CjYTAXQDGWaqm6q02hgC7Ig12CYzION3DeXR1G8cZ6mLMVWkS1qqNIn
jBNcd+Qrg7x4YEJd0qr5PMjYm04orYgOTaOO4O4pfED3iS8NSq9AJCQ9HdfGFd8U
GhIslOSQ6RdBkjeVxBAIinBya1TSpQLpZz9XZMIPvf/WkoEVldGNWAjYr5l1r5o/
s3FjgpBAwfRYv1IiNGw1juFZ8WZUJW3xhwEitg95sZMOyXC6y0GlUDo3EMsVcUtP
iCJUZJlfl3OZHRzTPlEq73A7HscqDL1WVzGDkH/g/lCtLKMzITFFAs5+hd/U3lkY
5iEQrdpIK/muK9fjG8l4Pd8/C8xe9Pr8GUw2QJu9yqy6eBIJBg3bgaOyC6R/uX2X
tMBd+g0G4hCog2+72Rs7KRWnueZLFz3wP1vPmV/VdL5cOPAHJQ9s6lrMh/elV5UQ
8oAzXk3LzuARzPBr49qj4HZ2+EupbSGKpw8w3ZJo+I+XSOzz12MKqWFbeLt2kCdl
EXOdTlTznWjHcak4ANy7CI4SvC//aaU0VfXl2qD4odU2j0Ht7tPkC05ey0oMkIi1
+Zvd9z+j8rQRUxSNXOQK2DR2MAgxsl82IhFSWUx9yj7yF489OpqHFgQ2WsrhyMAg
KzntSmgZJAG7hd446nZY2G3NEk0JhmN/tzo8LNWiU51h5d5wCZC6JEn39aEEPuar
9667BUjtcT8Uz+4ew2sLaSyb574Ee6yb5BJM0N2s5hAFsPIUWjxsJ1ZMsahecgsR
9Hk5VEF6zNm4XP927qnn3w==
`pragma protect end_protected
