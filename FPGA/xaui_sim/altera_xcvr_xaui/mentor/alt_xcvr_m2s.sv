// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:52:02 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Mvn9Q/IVxrMWpKIA07dbqaUvk4n4NG2wxqIyFuK9hqBh/KfPpt7MRE7huhr+3j8l
8h5uFCiZ6H/jmkYvTetIv5wprkw7oSn4lP/gGVXuvijFL++fU3K7BDvQwocBV6QO
cZEa7qyyy9tgkbeJfhqdLm/wQ0Rkpc8Ry4MtVSmcWcM=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2592)
S0Qn95tpzZa5Nq6ro+wnbY8rM3U6D3eRHbEJIGvMvT5ZmSn9lj1ZtUczP2BHBDCj
1it0pTpFMewxK4n+MW4gAGivTTcMPHALsq/w7ZJPSNI3/WbNN/ZFwqN2fP8KZ4fp
RhWGc6Tr/RxZGtPatT8efSgJ+pOcsqjpkCIU2R1sRBGMCxNwfyK9RQ9ep27YKO5O
xuQYIkso2icyyXZGp/zl8Uhsom/3MW+ZZ4+J5fBMy/wHR7ZdE14oA9Uwdsy+hwUb
G0L2nxA/+m88E9b/eVjkLkOzSxLJMT+kTcpXGecTve+LhwcvuEOOthpgXPeZS0w1
ISufDRJhVhj0b2n7GH6Gkf4t4O5HXe4av5C+oHSAPl7X5sY5nkdi1mf89/U23J1z
fpr7UIhWVr9ePYD8o8vQR/2XRy74wmGVvF4ZuiX+p+Hcc1NXh7Fwb2pdZnalTSjX
ltxUw3VmlRYgmcUxiSUPub//qTrRZcBF/KCUzNWxaTpY7i8iTD7a+Sb75YZECDfH
OEWEI/6OdelSMZdJM2F/O/beP+RGnP8vOJkuHDW32qv5ccZBL85AM7SQI2jOvDrf
RiJZUDDwzx0ld/Ryhuyyv5Jk0/SyExJZkbvXcmiJ3LWKulViFncIkMpmjzwI07/V
RZhVBafBkLTGj8MSCbnmvLxVZz6tNol00WVDSxE21Aq0KZP91IfuxiMfpm2Vk7ux
iK5mG/G2M/mtNCAvKUvprCeRBzS0fb6Xw1Tb2CrxoSo5+DovxnJi0x/rXLAT8K+N
Mo7MoRm2LHCNYFoCOX1TrV3LSjrh9u2SBhr5CavLj491hhF++rNtii/1p+dbgLDT
j2+aAOPuJRrn1ZjLVnQHI42+5Ggf/2DIfy8M6h04+dbDa4NEZHz2tAeoCgIZZw6J
QFUyirtlMWDdeBT4m9RlKZSkn483ccMBCQhrtyN0w6UbG9epxOMQtsmZNyCfK7pM
3mbBoBSGUlrBQmQKIayFw1YtUVepmDsL9b5wNbyI1bdF1kH592ozzf0yYwM6fxm5
NRpXWsu4CwPQhth0n1Oir/IK84+NKiPrFkj2N3GRUbqPfiXILgF+UDqrBBZhy6Oo
hY4WsP7XXMEQsSoLHJu07eA+djGR9UX3t/sCP0aB4gFcJW18m09LIySCxzb863fM
w38MKLvzd3dnlFnmiw0p81qnVdzisCyz5fh0tI+pbgvKdSRxYD8ht1Zr8PEKhQGH
5dX8kvZzx0s/NuhL1b591NPqD1WtIADD+qMj9SbMksOy0qPH0OEheC0cqJuJPr1x
HJGDfc2S+mjDOQxgVvjDViCacQc62zAJUIRGmHxmY3UE/6EgGaBBa+I2FDh78vCH
vW7wVJxVgwuZR0E1+UTLBUtAP1wp437kCvjYnaE2WwmFMCWClIS0mdoHAz5a4+uf
ZY5ffUzF/qY0/fy3eePTxuO+MxLAT+xPPIJ3STfqw/a2mVKaeRfY5CdfRMJkSSqf
E4F9LqAtsnYQo/3L42FcumQeQmMeZYEgV8jdZUrxTYe1z2NkCJnCT7v/1XzNLXEw
nRURKLLtsC9JTEUyON59ES7xsWj9EGxvcAjxldgqxy44GLA+UqfZbiaYcq9y+TtH
h4VJaN1tKGPrzPRAWybhjjGvbj/zut2tnWpW8mZWolwQnDQs3Gha7M4N2WY0ZAcI
E8iakmhDGrmxhR8ThsNl17nMbDQfOeGQ/mJF7USXW7gTJwJPHZcHpFyue5QA8kKD
ViYG4HOSS5LAHhyljiBtqTkSX63xBLZ2nMebmI+fYPTc8JQ4L+x1UpdmSZgcZpN6
xQhdqo4IeauNzk06e3pyD1L1E2A8jC8DRCHydhi9z8tm/6ilO5eRR5ykdeLjvr3B
jUBCn9LEHXpehIzmUnmEF6D1Q5vvvIkhStFAJo6Bt8fCmVrSSD7t96E0JbvsEZfw
84LHvTHcW2vRRseWPkrM+mUfGUBSVTnmX4PHCg3uJgwNdlQ2j/lZf4t9Rbw1treL
WwGuec+qCX+LdBt9bUA2LGDohCY8wuJ8Es2m7RfYDlIR+Payg3DD3cmvaq6LRfIj
oS8tUmx1b9tewLq1DG7Um2nVRFWynhUyi4dsq9Md7Vzt3EqzTwa/ju+e5QLKru3/
a7FHyTCSSsHEk18yJyL9l7XgXLst2sgwpIi0jt0exbGwMYUtZdcUK8suRcYfJEY1
+CFzCeIxwe/JDX/yVR+0mFa0FassAouSJ3ZpgyZrmnzRVuU6Jo97ubH2rkhFXS77
E9eNDgSYL5axjXSQM9ZrMLs16axtOmTCbt2sFTaZ/S+ZYuDhR3vJdLwUEca/jiku
qPS6+4el0K32aLg3XUCfT6yWFuEddYGU7vpJu+vp13dK+KLMCOIi7ASv5gl4AzG+
SGvOwgUquO31hExeDyHaLJfZWQq8Ky64htsYVkykWYEEiyWs8xE5nmLvs1GCSEZ0
9Wg9xAGR0m7Mkc3zJFIGcqqmBWBTLMAUvI36hZ6bCaYNUTT2zF6xAdoN9uBSMzGp
iKoYduv4SBqsKYKKQUG54keXEcer+phclEl5HWXFz8N2iVgujEriou1MJeV8GJeU
z5bKLiErkmPaJ4AOSr0Ez08+li68jbUO4EbzJ85LH4Z06kCNKjZ7+ywalUKMV0Vq
j/UV2XMkluKi3vYUZl72Jl2Nn2V1pRVf53OYor5JA/mW18DnjPL4YPlcThXPwp65
NpzKUVTawwjiFqLOQX7+McGvb6GJuHcCTJ20bmg3+b0Z0nyKJ7VM6a0KPspTomZF
Yt7LtOXQP4rgj1ObYqbVo5HGNFvK7djH02k5aCPVBKVkNJae9HtxpsFnhQFk6Vhb
tbnA6vkd0a1dDAfZQikSYBf07LBWentcQxLW5DlzW6FTrGnVa1HEjT1K38yp1weG
/LDLqW3xa7+8K0rhd2FkcIrxNAZvh5qbGOaQVkpIpMnaMhBYTXV+rfquCnj2cbxV
iYEh2+hSFt/jRV0LHMcyWPF/HaaRTKw/Fl32Gf9BH2m2hXo4YZJbCUo/np/1Y8En
2PIb5kILbxKoCuKO6qWGQcO3sTicTXDdBximS8ZgVNs/OWofkLkJeq+pD6CAzDs5
stnSdGVbdAGAh0rcWhT+zUKITHbWn5Db+ZLkbCSF0Vade+4pXq0CsKprnH38Qr3/
FENmwsEgvuEl0L7FXKWqEWAhpBqxXZdzPLjVaG/Q/X6ob6SRmSPdmGUszo2YYeJw
DxB4VQdm2tpjqiEdO3J2N4lk09BtFPF6z1YR+bKBy6075M7Y8/y+9G4EooVCQTJe
+47gqSB6wpXb+HKhQJo3zEdQuoppzKlcTxsEqZelxjtrRWvEo3EzPnVY/MyyKTjp
G992pE3nTQqA9X1guCpneRvvZSRnDRiODIwuOos6wPx7ykOM9uZioWUbbZQLex7x
hZFh3Y49UPZhqpO8IIU4UiNn2fiOD8cxGH5hjxt4R/4haCB2Ce5A/XLpSAg1WMD8
`pragma protect end_protected
