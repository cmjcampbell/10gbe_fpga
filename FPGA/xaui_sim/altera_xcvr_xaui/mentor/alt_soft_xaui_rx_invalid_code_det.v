// Copyright (C) 1991-2013 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
// ALTERA_TIMESTAMP:Thu Oct 24 03:51:51 PDT 2013
// encrypted_file_type : mentor_tagged
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
l2wL0YDGqtAe4+zQXs5kJyuGLM7vsoc4yHN5dy7HfYcR8ho2M/xlsmX0d3nhH3mo
STY9SY2dIAgcRfIncAiyPgpbA2n9tXjhbjyPPAlOXOie+1IVWDrrTNhRkDZvnJn3
XW9TAhd0PV91xmCB6lxjg+rm7jOUC/FNhksWOWCn3cY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 4624)
Q26uTty1HUbQBOHOBfNZ1Qbxx849fYEB+G8VSrENNwl6KCn9kPTD5/FKvHK7sfZh
+wlBgUvsFXpbEFSRtTNpUx4DU5HqFkCUCtGvCKVOfK2+RKX8l3jL07Znf0kTtDNv
jJIoeJBK+V5/A91jvYHMXnOQwm6fMoARq6Wd3xvGoaQglMTCVqWp6/DFTCYH3gda
RiHSVGCa4uj5htsWnFreGJ+JcGpYBRBm5vsp+bTLeFdYt17aL6ftEGY2dFOcFBuX
W2YCCraY1VpMpmjlMuXsG9H620pGYNM/5cIs0e3dQYL0XRtdDLNhSviqeyaQ2e28
51d3baKqZSVQvnJTOKQQl0uXAuiLNCCQOY5wRMTkYrO8ksPUij3pvE6gCJj/Zo4M
bia52jv9sWoWp37F3VSgMI6vhF79doTc0MVt53YVVe0xMruhOwSOOZgfsTgfh62Z
MG3k5Fv9CNqEwwxYpls22PYsUE87SElRJrs1eU5TDVXyVTpL41spd4AMz0NjEM47
Bp3ghOwKzPp+mYe6tXq4T9piXJ9SBO6dhVm3eu8zbOcgcKaWJG9iNWiB3ywoqmbC
pJ81pViLeRKM0LmqLsPXFNQvDgWVtoOjVWLKUJoJ+io8Ws38OT8p9Je/Wzr3nBX3
4uVZHYYDQnw4PRdmoAV0MdrPvefZaXpOhLIrHB1+4bXwJi8g+zQovyCP8FyWk1xv
T8WxCZLKKC9a7Og93CSL7xmuinx16sFfcBSR2Ik9aNBieiFSlzqG3pb0w5xg9sJ5
kxZIOg1g2p8GCtJ9ty46tMuFCPZfgN8wPdm1k5BBJmdE9Y223am7CofMqaA084ol
VSRCT/SR7pvTJjGQ27+AKlo1TKaSkke/Www7PU5KfcduQeswRF1XrQ/kEdHI98R7
Af4s9DeIoEr8NAr2xwrsGhq7Q3okRYVDcujNO7V+Ub9mRTQxAHpdydSmbaI/qPlX
5Q8j06xf2Lr+JMO7wDrBtMbI3I2XVaQrhZWT6vDFcyVuigT39kYG1QvLG3l04nLQ
UbpWqUb01/aaayEW3IHx6iNzayEyEo2qF74ydfiTWzWNV0iDWCwlHvZiXRA+W1a0
4jjrCzJhU2ajcmlFhg9KCJzyeq0vg6jbLtHmJU6eZJDmf/294OM7eQme+RXF2kwJ
DhnhXhZRdU2swJYeuuo4tiYPYAZbVqOlqyDnfLJescVBvie6zV6qThgmLr06VFHa
4poCPnwj+RELg7mTwkOUt3IF3ymxyqz8nenWEtesytWeOlddYOn0vh7fsgnVtLV8
nYPhGgQKNwRylGmfjYKeJtqS1F8CuOL1YlkE59qFrcEYTv/t1palTUbztuVYmzDq
GekiEC/VX87BiaMdSKFw5yRjogc4vug5xG1whC6dvxySTqyPQ5p565HAss5J5nI2
A1/+w4PsuENjlNMD4PUEa/1RuDLWEQSuTgO5PEYvI+GUaeIQiJSu24F0IUJ1x7Xy
r2CLTEohzd8y/ml+xInc0/1Z9NHOGR6OnSEqi75SIR7kpIRJ2YU07JZF+4/ULN9U
sUQrQg7v3TnVv1GnBk7S1+xYknoKdRVMT6CJQ6AmrcGCv1nHEIVvGe8Vs3WhobP8
ippW2fvLBb1m4tlpkmq1R4hS96pLT83X0vjSObFByay+/yiwqHsQQ+akljFMZZru
T6+xVvtxub8M19CTIIJEwMoaiF8qvpLsT5+lSsxq5CytTUJe0B8BIt4E5AKfByrw
COwT/IQhGXa2p0CkwAk8Q1zMnsoV+7iTOK5F2cjWyglp7Gi/MJZ7Y6btUigNr7up
e+no2bNk+UMx9U7/LCkKD8uW3VUZofDb9DuFAGL69Rfe8YqKFH2hVK2K56AwLln3
V5TSjycuXienuI0NkvrCsiscoOrf601VCz9hPs+joBUwYEE322Li+STWbZd1EcBj
tcaTmQ9WIvZWOlr/pOpNl60FzU0yYdZ0IzP3pAn1B4S7XKeHRBjYlP8h/6GyUwR5
LLVxCGBQkQnoeiZZzmHqPls6KnseEapcRbCbVistuGGmerMbCZSiMBJzhaTLG/zR
cpyLtltTvfs9Tx3gVK3kl0cqzwl56S8PsQqAQalOkHGU4Awd187mNZdS5Wup+Ufo
G90E2WdryB0uuJLSFYr0J0Ek/RmUBILNwftAhjTiR+ATgZvqRp4nWHTQ9/t8VFUw
DHJ9dOUqWDH7G8v3sAtopht2QLKdT+1P/JqA84l3olRMMSY+6YK0rridLRhdkXrh
hLI8T+b8CedYJiNoApuzBcOtxDglYGElBBCw8uD1Zw6GL6ud0JCDrGwUvim4FCp3
v0/vjS6eiV9EjRb6f2OCLuUYVUP6xZ37EgvX9pWBc8NeRPDjut+JnDkgXcpo1xAl
tH32iOV/Y1lGgYiDlK6P4dX+NeXG8bqKCPXeBpeV22tvKJiGcdRd/XkWGzohTIaz
KEsA/7NV7PRKpgXuizHbdjtrZsyKAan5qkzB0IJX8k2tnAWAIxPh79ik0LjyNcFS
L9oe7tXlRCdYQQH90Cj6jx3vPIKldJQ0NIQMDeW37ZD9f3V328qdCpuIrh3zckyR
DmJdVWKGerM/wH+KP2EcZz3dMfUhjKXMDzHL5UgddG1hSD5IJFWTCcx7A9JGvXwN
zmLeUX3igfR8Utc+3COZOFCXHLObu5p/K7SOfmABh13fbEZEzzJ4R/FkVL7MH/L3
mNJPzIuGKNQknUUESeXLWr57+Q8FAu0hx9DwmE743otTLwnLL6sb9caNSJ3fphEF
n9UepLKBVJbzhisFEuxywoRrRGoFjhbAUbnZuOUCEgapMLmA7A70y0aaCm6pjZZI
ji1s1DRnKLCeLUow91jWDw5L0v8CSjFuW+YZRjUw0IfHcDmHZSS2X52DBOqcSoEN
eT+H8Yq11nEPaRmRMGD2VRmYuBBpEXbT1BDTnLAbheBeSwUSCwA+MVmAKnTzg6sU
aGQncWzUNjx4fj3Kx94DsK2U0vdQjAcu1aoURbR4+/B95XgeYEiOBEFa9J7NQ0ER
gGgAbN0ZyHwA6IvpxQBmL2KZd2LCZJzFgL1I+EPAzd0AT3IvvNMlyreHSKxIQBi6
2qnnaPvdOPYx+nl4Jt7q58KIgKQDBKGtvOPdrjdwdtiTRbITNTkAUBoZdHy9sIP3
TJJiAOhCIeP+Kyx2Hskq07LwzKAG29kj6ZuSS370zofMf/eIsuvymRFmqBetSHzw
0miosoE6OUloXuNVA1xpT92k1W6yEXGhqSNgtnAqsYBMtaPLlnds/wr8eRAceuAA
UnA1Q2Mei0tCIy/EH9ySoo/9NXfwPPujNtTcouVi9/jYD1FCN8+do8uiYnCypZHG
sXUc4ErMeSF/UUUzoa9nLnTaymXSjGY9QYLi4Zr0hCyLk48uWubuEO4/HY/C0ILf
UydcldyMwofUjVxfAh/9bikxte7sGc9i1sMJPHy99bM95seOK7Vs1k10AsE+N279
/iNM+hBrMDG519iDI7kTCE8yMQv8EUPrM1OSrUMlY0QMF1kKUnnf3tFvRCyf3ow8
qZAJAMeyY2ylrPjLflOIU4mL3bfp//G1t5Cou3ImQBl8F+58iyZq0J994HTEHL3Q
A8U+1ebm3IUVleScnkaCkIkTNdbhfg8cCxmIGkvVhaLLGzFzp02/FiGPB7BV9h5c
KBJHs3WCUeLtYFiDnCaodD4aId5RvGz66EgsPkAEsPK3oI0xDbyFGqE4VM4yW3rH
wYmuZnZYu4S/K2cgjHu8k4M5V/B1TBgHfPgxvjQsK7sreldT2No2Y/CsgBqhQEOW
vk35PwDbSlYeKy/4ja8zweDZHeN15BFQzO0KKxEPbvr4GZBJ7IbvJ80bJoHp8p96
NdAMG9ynWn9KgDxX0NEO9bd/FbGmP2+oliCEYIG1+ZDJyisdQU19pltEnlG2h5lO
3U4Fh+F7rUPMTMximVSFJreKiuZEnNknDnFeCnTZ9SfcJFwuuytjCyz82tHWsObd
R2STLZLvneqgMgqQi0ILZbjkcGtvdn7smKCV/WnblaOL33HX6PEnBncxpVdz1YIp
eiMdbbcwef5H5al98Q2tXFV4FaV7UX2ShDe2SfPbkUNDJzPfv3LCg51IBxjcDDcB
sFtyRgCGO0qb6eCq/3+RSHUH9Y2sGzUPVDBW3tJnqAFn6QRWo25m7VdaHX0lnFpU
5ofQJLEoHXoqs5A77QQUjpcdlkYOV0bl+eP9txWmFjfLRj680PD4DtjhnML3956m
GbtHNmpy5f2i8KUXbWfJnDHP5WqVuQdwkzv9y2TrtD6luk1Umjyf/fyOSUlpEBVn
WKfqIdU7ty3n5Q3mD6j+ILqbDolY6P81BENJ6PTGfd2DPJVYyheAczKlE5UNRdGa
CxGyBPgAxjPEVUpOj1HVOpCxMNEzl4ssLs0yLLQu9GX44BSulYGm4/QwpdzVl/Vj
l8Fncs4g8oAorLNsb3L0XGSAjTusFY8vxED6I1Zl91l6YUgGrJLElH6ghjsvYhFe
z7efDPee9pt20NkMpP80AJQModejz9XwXoGd9Lpl9gU8iAgOmnuuOXqntf3yHl0H
Puk10rIwtFDLkQIxQI8fONYKsGp88jkZIaOkyjO2Npk7t85NqssXTowvfW2m2LSg
eszWZbOWGM+24wQYRJqoHrG/zcRK1267B6QkQK0s3WRQwNCRqETXCR6kDZw5+SW7
6+38//X6JYUM7A/CZ4jgkMCGOQWb7OV0hT0bjWx+Yc5DIsNMKnDnloSBjZ+avhcL
fIkaw5ZIj28XCVLWtc3pUtQPCU3RbPlRx7RaaSQqgzD1cZXSAB/IGJf8vjLjwZPR
PaBCAwSlQA4AVL+HPkvkHRC10XaU3s5d+66iMns/BLRj83F15tSRRH/gaiEYSX02
puExyeS2a46eVJNdANJm8tJQSvY/wu0ryoBry3EtVEDUR39JLPrAKZd+CEMjN2A0
R6z+he/VusvjCOflzH/v+hA6N1/hSbUUXxs0Rt7NgbzONcdVkqgrrV1aINUcVHzn
M7RAi8jRiNLBpwB2LailsvzZW2l+eZOvMDuZrbItP/ircuAf4g2LQeKXMIO4ida0
6WHpPGz1w8/q/OmDlrwgiKlt+lAAWL+iRce2Ku8IMUXpdCIM1NjItrciwsAogpzh
YRon2mcNIice0GUkmjtkQhG0lHj/CYptvhKB8UjY9ELVgP2YDMDjdB+1rjLfU4EI
+j9IYW9Zs+fyN8umPzl3CSl+sclcd5ZhthvLZN0NGUTp9FI9dmejWXouVgYvziJ1
mOhptts133WvNdRqcnXYDzeBR/z34U+WHgMaS5OwXVIARR+TGiJ7+J2R9WKdrYbi
zfKxUUsnVkS2jfFznP2zJ0zn+aQerEbeXlf/E09AcjPrR4JFeiSoL2m71a7oKjoI
gjAom6X+VBiyrWgb5c5iXAdVcHbPHY+zMwIa7ubkAIB8ZT5ghSkJQmkMan0gdz16
GNGWD9awhX4C4J3IQSEAi+dIeWRDI2UaGv8extOXmaK6BNcOuUxmeBztqtL+hT4P
4SZxGCBhDZlatkbkrmZuEE7sClxrKNDLdAR4vc5wHLQClJSHCFU7OOzN+h5AD06E
5oaE/NHjdq2ouKZ96wn7kVA1meqbttlPkDL5t9Xv0yntMzlBZK8mPdG+cZE1FVt5
e4sIZ+wvVIwLbLWHiWT7XEK9teHFAruH+0QCGIQcPlGYKTVGaToM+4To8WusLYRr
ZzAEHowgJDHbD5gUOEaskJyJ88bXkmDIbc+UPuYITBfpHtpgluEsYQTljn+P37Qd
M8PBAeO3lUN9oE3eBvcMD4/dl+oa5TC0N0+85ak8L4cECc8jvomeEZQYELhByKch
jrJQmajNVVaj2le+liwRco1r+J1Z0KgibrGkDGVFojc+dJa3Eg0PTHZ1++STngQP
hOZvJlnD9zXL7FNLXROlxvbqRtf2hc6HXX7kH6dD+8R3ADKChfUztwChEGOsGtz3
YKBeTGWOkB0m04ktdsorTCfxroFiIkKyIdJJgl7ZyTHyn1raJCIiUraQ5m9VWxDf
O9qZqnCiJkurY5Gz2EfWw+YZGhtTJyRCHaLLeYyDyKnHqJcDKf2ILFpr8CgcEC+j
MyqkkCoOnQ5Y+0zeZGQfOHle2jHAFsCvWg4iGCevWbKXwAO0BwtfFU7t1M/XUZM9
sVxxR60Cl7mGLUJ1bTmRtw==
`pragma protect end_protected
