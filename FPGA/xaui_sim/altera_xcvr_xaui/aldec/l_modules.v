// (C) 2001-2013 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 13.1
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip, Riviera-PRO 2011.10.82"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC08_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64")
ScOVHkMLz38vKqiI3SqG1kXovMNifaY1sfV9xxDe7hyUa7vdqxDH3ZL/o7rmDgyTK67CaC2dcYX9
IKwghYisSpFO48F7d+UGNm0bZcMQO83z5KSLkQ4K9OqRb0284j2v591KzWk4XHuILWySYnQxvOpG
fMI8gr2naHvfQ4MFkIzmnTmnHMxee2eeLxw/5ZIpw0LJ/h/gJCGGLaxrcpsT806FDpmkMYk4ti9l
ZItgRMRn2zzDJKPMyinqzaEa0P1gosQF0uFoeHAUJ8p+VWlNCWjdUg7AFzCBBFQ/v2FaMtFrfqdq
Pnx1y0X/2yzUsLZTVUt0NkezPuQiYdsVsVI+0g==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_method= "aes128-cbc"
`pragma protect data_block encoding= (enctype="base64")
m2Bsp8eMgvio8YR1bJhjiNE/xRpGY6AH2lWV6UhafiqMzlogKK/+0sdV8ZyIdnPE0pP4V5STJ0UG
oGXf1LF6XXTgddbbni5xMDU1K9R+ejEzfjO7FgZ7reJmqFlwyuLwkmsQcSSYhAT0hNokIyNTa8sX
IHWcoek58mbYv1hnwLIxZVW4DoQQ9S4JSNjEu81C/FQPiQsXYiNUPYMsvw3fVaNp8Urkf5awNG6a
9Ux5wnZ5IAmk6g//TmnYa7/s4yYEDEZJFCVE4DrqUYF2DeGkEBFka9bdKdftKelM4MaxZj8+Qfwp
c9/1xhffef+X6JPOZzc+JJAOeLmTdFIbBorsdBCcGME20WxflvR4B8YKv5JiHhnLliYD5v60HRiY
Bfb3Mk7Uyq5ltex1ojJnzo/O8Vjb5+XYsojjcJ0Vrg7Rw6TqPHF/FYi9z9BaDXo5neTL3PkDXnI+
XLKTirDIYA9X6miVCXhklKjqMWQp54W1YwsycbUurrbVMCLBCxKJlZhLEGzkQFjmvCFsdB7NgbFM
QY7YyfK2jcL44F95Tqgbh+IKZ/xyMxxXs64y/LMBvgF5BIhIF1KBVhGjstZettQ6zpvRXJyh/Y1J
TIoNy9mLBp80TDCHuAd+o6X8rwPquW2iIL8E+jMawUJrTQFN81ocQ70bKWSF8CAaW4sYs8MJHU/K
p9sHKlzicKVrnOWVnOKsgdOZmK43Vd8sYXaVKfrRxGgWgb/A7I52314ug2vnZzhckmBc0VQZcwAO
HUHvd013SJdKUFG2obWZXqtTTRYNy3S+EbOIuznbe8RmPB+svXAVcmQreIDoLBLxy5UUpU9WQ65P
HYpbZ1BhM+X3WAHZS7ME197mYLKfecjR0kdjQ76Tp3wFXSTFzMpzzUj60QrasL3ufUWmpvCrPPP0
8FaGuENi2PWQtJAYYmttcYMLXDaOvKbuLyCVEddwGV3bOJEmLfGC6e4VYS1s+mPQLWizCIorlOPq
qDb2PGr485Lzxk3ruWJiUF3DnR/pJAtaA4cURh8GOt19yPeo0Oe8JXwzscjzGGRWvkGRHuHznLew
Uy1/LqrMqkpP9y4YSFohzcsvH5F/yNBSUJar4kvGJqEgaylYP2ecoD7jdSosJJ6tGRrKJkrPiq45
FxA5gjyQcta1jDGJ1LcCgc8PSmhDrXZPqzij9QkTlB8HbvGVJHYTYse+3KHtizWPQSbhFVry6Bb5
6zh+fC39z4B3MdAi4nDz/4HdpRK8QhDLaal4xRPZ/bom2nAjZy1fjDC5AgxY2m1XyRcI/tiQSevj
d4ydZDLt/UeBpaQZ3kqE2Vr3GYBFv96H1rQE5y3Iq2LVkWbd65oZ2+uaaEzLFqVhGPAtOrVEZ4fd
nIe5n+7gcJsueROMeeFl7dv2ukeWRzIy7Fr49u6/LTAinnFUwnKBHOsa7NOwMMPNpE1q4NfG5b9f
X7uAaRnoZqy+Aa8CHdOEI6YrHu1jaXWhkZjjy7orGdI0emijJqe3P3GJBKOll9pDPwM32QKPG2dc
9wr+lAt4d6em7ejU57VEhdS7MihvNCC6g1PNjfQPD5q1VJ3IQrahaKCn4Sla4vom9KGSiwvO861J
w7pCpHr2FnZBXdVpLOxYJowXHNlvvHVObTZqsvM+UzXrevWB7+QtnUiXz/V0NDjqixJT+0Z9cDDv
L+ukW9IwM976F+ya2VQRXC0si0qNExV/Xw+s2wD9iD+Q6/SyqFSLYNDDp5mIGM7OaqRMRSpr1hVa
vV6vsUmMhWiv3eV1DXmD60rODyCSb/VSZmuBj/G/nrcLRcAMFETVHnVnYXYI2JllQIH4WUgjtUIq
BZgK9FWU0Hni0cQkZJMcChbJnLz6x+xCvhKzvnQMjVgxmQcYxu5loW5esvGbsR3R1rxKpCKk0WWF
F9MdYH8fT+8tLcIn3pxVNECIYwv5eXY397IH9JM2YRRWltAAxz/RAzBQgrE0e+uCxDuwlrdUpTUY
eSTN/jpUU5Xhg0SierfcxkJmHiR1KXiuxaW4AoOeY8kyRoUDht5fymkxHAQvzOZW4eEFtvMzdwIE
qhFrWkVuhwlP8fDqaZhYzkii5rbCadS4ScWUXHUa2bozVDmTgijr4eLBlnMB4NrrnDm00ZwezEpF
j4ruhoio3OvNuCllNeoEHTBNRLcQGrynXtg+tGl4PgpczP0NzGAtdq16g71JqX11ZuMKQOMGKMeS
8rrUyKeuoSuWZYvWJvYUzGSE04D/ahUso641kUrzZcNJf5NzdTUema2F2djAwOefUvipAMZ+pTR1
Ung1sZFd2Irb//gq0EIudaMKtW6mjXQGtrbP9sBbPiZfOxpneCwTPd5WnJ3bD+ueWycyKgISNg3I
3aU6NW0LlCUBYBo/mxlfclYAz+DXKE8AkPH9YSW0AVPjJECR2F9bUSOk8/3uY6BehIDlLo/W8S/z
2sPGrUGQ9ia9hTuXz59kUwuQtoQa0BZ0MC70yoSy7qnPpHB9FnM+IXkBOGP9w1l/JuqLBqIxWlDQ
sQRwhl3VXvD1Yc8soeEibGYFHJjW1sYv/DaoDz/C7gV25bQg5x4RNdwSc9JVvp5s6EpkXaRLDUSQ
1SiPmRANIGdjTl2DZ4UINJMSDRup/ZTyk893a0/RjdCqQqYN5F1zbz9OeKzIPwEUup38HKeJ0EZG
az3Wz5qmfpskM2FPT2xsolNl2BKE7uPZp13dO1oqLG+85P9R4WAYa35y7fUDltWBh2p1yTGL9fjX
0QzGS96CRPqFCRLN8JJUldNUhB0Azm/9S8VuYRL1F9VqK9f0tT0cz477N30f7xGh2LGkQ4NnD/Qf
F9/3w+tK1uhbW4pRkEU9xeZFHzUNZHca+gTtEiSJKJ2VJuB4d4z2BTw0DXQsksTlEv361OmNYTDG
okbNlJ/WKDfVZHXIsI4uXP2uK6fyBQhIn44FODs6TONVh5QBFEynA7O9hYsvmUm/5mXvv7PtIgLq
C5Zzzr1XbA4HH22clnq4aW6pdzwd+r8gCeOMAuyqyR5lQHXymfKCuIhcUv2jmdeiFg==
`pragma protect end_protected
