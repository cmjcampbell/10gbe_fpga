// xaui.v

// Generated using ACDS version 13.1 162 at 2015.04.28.12:20:18

`timescale 1 ps / 1 ps
module xaui (
		input  wire         pll_ref_clk,          //         pll_ref_clk.clk
		input  wire         xgmii_tx_clk,         //        xgmii_tx_clk.clk
		output wire         xgmii_rx_clk,         //        xgmii_rx_clk.clk
		output wire [71:0]  xgmii_rx_dc,          //         xgmii_rx_dc.data
		input  wire [71:0]  xgmii_tx_dc,          //         xgmii_tx_dc.data
		input  wire [3:0]   xaui_rx_serial_data,  // xaui_rx_serial_data.export
		output wire [3:0]   xaui_tx_serial_data,  // xaui_tx_serial_data.export
		output wire         rx_ready,             //            rx_ready.export
		output wire         tx_ready,             //            tx_ready.export
		input  wire         phy_mgmt_clk,         //        phy_mgmt_clk.clk
		input  wire         phy_mgmt_clk_reset,   //  phy_mgmt_clk_reset.reset
		input  wire [8:0]   phy_mgmt_address,     //            phy_mgmt.address
		input  wire         phy_mgmt_read,        //                    .read
		output wire [31:0]  phy_mgmt_readdata,    //                    .readdata
		input  wire         phy_mgmt_write,       //                    .write
		input  wire [31:0]  phy_mgmt_writedata,   //                    .writedata
		output wire         phy_mgmt_waitrequest, //                    .waitrequest
		output wire [229:0] reconfig_from_xcvr,   //  reconfig_from_xcvr.data
		input  wire [349:0] reconfig_to_xcvr      //    reconfig_to_xcvr.data
	);

	altera_xcvr_xaui #(
		.device_family                ("Cyclone V"),
		.starting_channel_number      (0),
		.interface_type               ("Soft XAUI"),
		.data_rate                    ("3125 Mbps"),
		.xaui_pll_type                ("CMU"),
		.BASE_DATA_RATE               ("3125 Mbps"),
		.en_synce_support             (0),
		.use_control_and_status_ports (0),
		.external_pma_ctrl_reconf     (0),
		.recovered_clk_out            (0),
		.number_of_interfaces         (1),
		.reconfig_interfaces          (5),
		.use_rx_rate_match            (0),
		.tx_termination               ("OCT_100_OHMS"),
		.tx_vod_selection             (4),
		.tx_preemp_pretap             (0),
		.tx_preemp_pretap_inv         ("false"),
		.tx_preemp_tap_1              (0),
		.tx_preemp_tap_2              (0),
		.tx_preemp_tap_2_inv          ("false"),
		.rx_common_mode               ("0.82v"),
		.rx_termination               ("OCT_100_OHMS"),
		.rx_eq_dc_gain                (0),
		.rx_eq_ctrl                   (0),
		.mgmt_clk_in_mhz              (150)
	) xaui_inst (
		.pll_ref_clk              (pll_ref_clk),          //         pll_ref_clk.clk
		.xgmii_tx_clk             (xgmii_tx_clk),         //        xgmii_tx_clk.clk
		.xgmii_rx_clk             (xgmii_rx_clk),         //        xgmii_rx_clk.clk
		.xgmii_rx_dc              (xgmii_rx_dc),          //         xgmii_rx_dc.data
		.xgmii_tx_dc              (xgmii_tx_dc),          //         xgmii_tx_dc.data
		.xaui_rx_serial_data      (xaui_rx_serial_data),  // xaui_rx_serial_data.export
		.xaui_tx_serial_data      (xaui_tx_serial_data),  // xaui_tx_serial_data.export
		.rx_ready                 (rx_ready),             //            rx_ready.export
		.tx_ready                 (tx_ready),             //            tx_ready.export
		.phy_mgmt_clk             (phy_mgmt_clk),         //        phy_mgmt_clk.clk
		.phy_mgmt_clk_reset       (phy_mgmt_clk_reset),   //  phy_mgmt_clk_reset.reset
		.phy_mgmt_address         (phy_mgmt_address),     //            phy_mgmt.address
		.phy_mgmt_read            (phy_mgmt_read),        //                    .read
		.phy_mgmt_readdata        (phy_mgmt_readdata),    //                    .readdata
		.phy_mgmt_write           (phy_mgmt_write),       //                    .write
		.phy_mgmt_writedata       (phy_mgmt_writedata),   //                    .writedata
		.phy_mgmt_waitrequest     (phy_mgmt_waitrequest), //                    .waitrequest
		.reconfig_from_xcvr       (reconfig_from_xcvr),   //  reconfig_from_xcvr.data
		.reconfig_to_xcvr         (reconfig_to_xcvr),     //    reconfig_to_xcvr.data
		.rx_recovered_clk         (),                     //         (terminated)
		.tx_clk312_5              (),                     //         (terminated)
		.rx_digitalreset          (1'b0),                 //         (terminated)
		.tx_digitalreset          (1'b0),                 //         (terminated)
		.rx_channelaligned        (),                     //         (terminated)
		.rx_syncstatus            (),                     //         (terminated)
		.rx_disperr               (),                     //         (terminated)
		.rx_errdetect             (),                     //         (terminated)
		.rx_analogreset           (1'b0),                 //         (terminated)
		.rx_invpolarity           (4'b0000),              //         (terminated)
		.rx_set_locktodata        (4'b0000),              //         (terminated)
		.rx_set_locktoref         (4'b0000),              //         (terminated)
		.rx_seriallpbken          (4'b0000),              //         (terminated)
		.tx_invpolarity           (4'b0000),              //         (terminated)
		.rx_is_lockedtodata       (),                     //         (terminated)
		.rx_phase_comp_fifo_error (),                     //         (terminated)
		.rx_is_lockedtoref        (),                     //         (terminated)
		.rx_rlv                   (),                     //         (terminated)
		.rx_rmfifoempty           (),                     //         (terminated)
		.rx_rmfifofull            (),                     //         (terminated)
		.tx_phase_comp_fifo_error (),                     //         (terminated)
		.rx_patterndetect         (),                     //         (terminated)
		.rx_rmfifodatadeleted     (),                     //         (terminated)
		.rx_rmfifodatainserted    (),                     //         (terminated)
		.rx_runningdisp           (),                     //         (terminated)
		.cal_blk_powerdown        (1'b0),                 //         (terminated)
		.pll_powerdown            (1'b0),                 //         (terminated)
		.gxb_powerdown            (1'b0),                 //         (terminated)
		.pll_locked               (),                     //         (terminated)
		.cdr_ref_clk              (1'b0)                  //         (terminated)
	);

endmodule
