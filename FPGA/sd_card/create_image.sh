#! /bin/bash

sudo /opt/altera-linux/bin/make_sdimage.sh  \
  -k socfpga.dtb,u-boot.scr,uImage,soc_system.rbf  \
  -p preloader-mkpimage.bin  \
  -b u-boot.img  \
  -r rootfs/  \
  -o sd_card_image.img  \
  -g 2G
