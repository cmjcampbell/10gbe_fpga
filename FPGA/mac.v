`timescale 1ns / 1ps
module mac
	(
	input 			clk, 			//clock for mac module
	input 			rst,			//reset signal, active high
	input  [63:0]	tx_data,		//data input of format Avalon-ST for transmition path
	input 			tx_sop,			//Avalon ST interface for transmition
	input 			tx_eop,
	input 			tx_valid,
	input  [2:0]	tx_empty,
	output 			tx_ready,
	output [71:0]	tx_xgmii,

	input 			rx_ready,
	input  [71:0]	rx_xgmii,
	output [63:0]	rx_data,
	output			rx_sop,
	output 			rx_eop,
	output 			rx_valid,
	output [2:0]	rx_empty,
	); 
	//------------Input Ports--------------
	// Already defined in pot list
	//------------Output Ports-------------
	//

	//------------Internal Constants-------
	parameter SIZE = 3
	parameter IDLE = 3'b001, 
	//-----------Instantiated Module-------
	mac_tx tx();
	mac_rx rx();

	assign sop = sopin && valid;
	assign eop = eopin && valid;
	assign ready = 1;
	
	always @(posedge clk)
		begin
			eop1 <=eop;
			eop2 <=eop1;
			eop3 <=eop2;
			valid1 <= valid;
			stdata1 <= stdata;
		end
		          
    integer i;
    integer g;
    integer k;
	 
	initial begin
		newcrc<=32'hFFFFFFFF;
		xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
	end
	 
	 
    always@* begin
        for (i=0;i<8;i++)begin
          for(g=0;g<8;g++)begin
             xgmiirev[g+i*8]=stdata[(7-g)+i*8]; 
          end
        end
    

        for (k=0;k<32;k++)begin
           cc[k] = ~newcrc[31-k];
        end
     end      
      
        
	always @(posedge clk) begin

		if (rst == 1)begin
				newcrc<=32'hFFFFFFFF;
		end else if (sop)
			begin
					xgmii <= {1'b0, 8'h55,1'b0, 8'h55,1'b0, 8'h55,1'b0, 8'hD5,1'b1, 8'hFB,1'b0, 8'h55,1'b0, 8'h55, 1'b0, 8'h55};
			end else if (valid1) //middle cases
				begin
					xgmii <= {1'b0,  stdata1[31:24], 1'b0, stdata1[23:16], 1'b0, stdata1[15:8], 1'b0, stdata1[7:0], 1'b0, stdata1[63:56], 1'b0, stdata1[55:48], 1'b0, stdata1[47:40], 1'b0, stdata1[39:32]};	
				end else if (eop2)
					begin
						xgmii <= { 1'b1, 8'hFD,1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,1'b0, cc[7:0], 1'b0, cc[15:8], 1'b0, cc[23:16], 1'b0, cc[31:24]};
					end else if ( eop3)
						begin
							xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
							newcrc <=32'hFFFFFFFF;
							end else 
								begin
										xgmii <= {1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07, 1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07,1'b1, 8'h07};
								end

	end
								
endmodule


