module endian_swap(
	input logic 		clk,
	input logic			reset,	//active -high
	input logic [71:0] 	in_xgmii,// input data
	output logic [71:0] out_xgmii//output data
	); 

//   71 62 53 44 35 26 17 8
//	 63 54 45 36 27 18 9 0  
	always_ff @(posedge clk)
	begin
	if(reset) begin
		out_xgmii[71:63] <= 9'b100000111;
		out_xgmii[62:54] <= 9'b100000111;
		out_xgmii[53:45] <= 9'b100000111;
		out_xgmii[44:36] <= 9'b100000111;
		out_xgmii[35:27] <= 9'b100000111;
		out_xgmii[26:18] <= 9'b100000111;
		out_xgmii[17:9] <= 9'b100000111;
		out_xgmii[8:0] <= 9'b100000111;
	end else begin
		out_xgmii[71:63] 	<= in_xgmii[44:36];
		out_xgmii[35:27] 	<= in_xgmii[8:0];
		out_xgmii[26:18] 	<= in_xgmii[17:9];
		out_xgmii[17:9] 	<= in_xgmii[26:18];
		out_xgmii[8:0] 		<= in_xgmii[35:27];
	    
	    out_xgmii[44:36] <= (in_xgmii[71:63] == 9'b100000111&& in_xgmii[44:36] == 9'b111111101) ? 9'b110111100 : in_xgmii[71:63];
	    out_xgmii[53:45] <= (in_xgmii[62:54] == 9'b100000111&& in_xgmii[44:36] == 9'b111111101) ? 9'b110111100 : in_xgmii[62:54];
	    out_xgmii[62:54] <= (in_xgmii[53:45] == 9'b100000111&& in_xgmii[44:36] == 9'b111111101) ? 9'b110111100 : in_xgmii[53:45];
		end
	end
	   
endmodule
