/*-----------MAC_TX module-------------
Mealy FSM for tx module
timing closure: sop raise with valid, valid should be valid during the whole package

*/

module mac_tx(
	input 			clk, 			//clock for mac module
	input 			rst,			//reset signal, active high
	input  [63:0]	tx_data,		//data input of format Avalon-ST for transmition path
	input 			tx_sop,			//Avalon ST interface for transmition
	input 			tx_eop,
	input 			tx_valid,
	input  [2:0]	tx_empty,
	output reg			tx_ready,
	output reg[71:0]	tx_xgmii);

	//------------Input Ports--------------
	// Already defined in pot list
	//------------Output Ports-------------

	//------------Internal Constants-------
	parameter SIZE = 5;
	parameter IDLE = 5'd1, SOP = 5'd2, SEND = 5'd3, BUF = 5'd4, EOP = 5'd5, END =5'd6;
	parameter IDLE_XGMII = {1'b1, 8'h07, 1'b1, 8'h07,
				 			1'b1, 8'h07, 1'b1, 8'h07,
				  			1'b1, 8'h07, 1'b1, 8'h07,
				  			1'b1, 8'h07, 1'b1, 8'h07};
	parameter PRE_XGMII =  {1'b0, 8'h55, 1'b0, 8'h55,
							1'b0, 8'h55, 1'b0, 8'hD5,
							1'b1, 8'hFB, 1'b0, 8'h55,
							1'b0, 8'h55, 1'b0, 8'h55};
	//------------Internal Variables-------
	reg [SIZE-1:0]	state;
	reg [SIZE-1:0]	next_state;
	reg				crc_rst;
	//------------Internal Wires-----------
	wire [31:0]		crc_out;
	reg [63:0]		data_buf;
	reg [63:0]		data_locked;
	reg [2:0]		empty;
	reg [71:0]		out_buff;
	//-----------Instantiated Module-------
	//-----------Packet CRC calc module----
	crc	crc_tx(tx_data, tx_valid & tx_ready, crc_out, tx_empty, crc_rst, clk);
	//-----------IPG counter---------------
	//for XGMII 10 Gbps, Minimum IPG 5 Bytes
	//For maxium throughput, should be able to start packet from 5th XGMII
	//Will implement later if have time
	//-----------Initial Value-------------
	initial begin
		tx_xgmii = IDLE_XGMII;
		tx_ready = 1'b1;
	end
	//-----------Next State----------------
	always @ (state or tx_valid or tx_sop or tx_eop) begin: FSM_COMBO
		case(state)
			IDLE: begin
				if(tx_sop && tx_valid) begin
					next_state = SOP;
				end else begin
					next_state = IDLE;
				end
			end
			SOP: begin
				if(tx_valid == 1'b0) begin
					next_state = IDLE;
				end else begin
					next_state = SEND;
				end
			end
			SEND: begin
				if(tx_valid == 1'b0) begin
					next_state = IDLE;
				end
				else if(tx_valid && tx_eop) begin
					next_state = BUF;		//no IPG, IDLE takes one cycle
				end else begin
					next_state = SEND;
				end
			end
			BUF: begin
				next_state = EOP;
			end
			EOP: begin
				next_state = END;
			end
			END: begin
				next_state = IDLE;
			end
			default: next_state = IDLE;
		endcase
		//$display("@%d, state = %h, next_state = %h, data_buf = %h", $time, state, next_state, data_buf);
	end
	//-----------Seq Logic----------------
	always @ (posedge clk) begin: FSM_SEQ
		data_locked <= tx_data;
		data_buf <= data_locked;
		tx_xgmii <= out_buff;
		if(rst == 1'b1) begin
			state <= IDLE;
		end else begin
			state <= next_state;
		end
	end
	always @(posedge clk) begin
		if(tx_eop == 1'b1) begin
			empty <= tx_empty;
		end
	end
	//----------Output Logic-------------
	always @* begin: OUTPUT_LOGIC
		case(state)
			IDLE: begin
				out_buff =  IDLE_XGMII;
				crc_rst =  1'b0;
				tx_ready = 1'b1;
			end
			SOP: begin
				out_buff =  PRE_XGMII;
				crc_rst =  1'b0;
				tx_ready = 1'b1;
			end
			SEND: begin
				crc_rst =  1'b0;
				out_buff =  {1'b0, data_buf[31:24], 	1'b0, data_buf[23:16],
								1'b0, data_buf[15:8], 	1'b0, data_buf[7:0],
								1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
								1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
				tx_ready = 1'b1;
			end
			BUF: begin
				crc_rst =  1'b0;
				out_buff =  {1'b0, data_buf[31:24], 	1'b0, data_buf[23:16],
								1'b0, data_buf[15:8], 	1'b0, data_buf[7:0],
								1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
								1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
				tx_ready = 1'b1;
			end
			EOP: begin
				crc_rst = 1'b0;
				case(empty)
					3'b000: begin
						out_buff =  {1'b0, data_buf[31:24], 	1'b0, data_buf[23:16],
									1'b0, data_buf[15:8], 	1'b0, data_buf[7:0],
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};
							end 	
					3'b001: begin
						out_buff =  {1'b0, data_buf[31:24], 	1'b0, data_buf[23:16],
									1'b0, data_buf[15:8], 	1'b0, crc_out[7:0],
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
							end
					3'b010: begin
						out_buff =  {1'b0, data_buf[31:24], 	1'b0, data_buf[23:16],
									1'b0, crc_out[7:0], 	1'b0, crc_out[15:8],
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
							end
					3'b011: begin
						out_buff =  {1'b0, data_buf[31:24], 	1'b0, crc_out[7:0],
									1'b0, crc_out[15:8], 	1'b0, crc_out[23:16],
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
							end
					3'b100: begin
						out_buff =  {1'b0, crc_out[7:0], 	1'b0, crc_out[15:8],
									1'b0, crc_out[23:16], 	1'b0, crc_out[31:24],
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, data_buf[39:32]};	
							end
					3'b101: begin
						out_buff =  {1'b0, crc_out[15:8], 	1'b0, crc_out[23:16],
									1'b0, crc_out[31:24], 	1'b1, 8'hFD,
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, data_buf[47:40], 	1'b0, crc_out[7:0]};		
							end
					3'b110: begin
						out_buff =  {1'b0, crc_out[23:16], 1'b0, crc_out[31:24],
									1'b1, 8'hFD, 			1'b1, 8'hbc,
									1'b0, data_buf[63:56], 	1'b0, data_buf[55:48],
									1'b0, crc_out[7:0], 	1'b0, crc_out[15:8]};	
							end
					3'b111: begin
						out_buff =  {1'b0, crc_out[31:24], 1'b1, 8'hFD,
									1'b1, 8'hbc, 			1'b1, 8'hbc,
									1'b0, data_buf[63:56], 	1'b0, crc_out[7:0],
									1'b0, crc_out[15:8], 	1'b0, crc_out[23:16]};	
							end
				endcase
				tx_ready = 1'b0;
			end
			END: begin
				crc_rst =  1'b1;
				case(empty)
					3'b000: begin
						out_buff =  {1'b1, 8'hFD,
							    	  1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,
							    	  1'b0, crc_out[7:0], 	1'b0, crc_out[15:8],
							   		  1'b0, crc_out[23:16], 	1'b0, crc_out[31:24]};
							end
					3'b001: begin
						out_buff =  {1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,
							    	  1'b0, crc_out[15:8], 	1'b0, crc_out[23:16],
							   		  1'b0, crc_out[31:24], 	1'b1, 8'hFD};
							end
					3'b010: begin
						out_buff =  {1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,
							    	  1'b0, crc_out[23:16], 	1'b0, crc_out[31:24],
							   		  1'b1, 8'hFD, 	1'b1, 8'hbc};
							end
					3'b011: begin
						out_buff =  {1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,
							    	  1'b0, crc_out[31:24], 	1'b1, 8'hFD,
							   		  1'b1, 8'hbc, 	1'b1, 8'hbc};
							end
					3'b100: begin
						out_buff =  {1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc, 1'b1, 8'hbc,
							    	  1'b1, 8'hFD, 	1'b1, 8'hbc,
							   		  1'b1, 8'hbc, 	1'b1, 8'hbc};
							end
					default: begin
						out_buff = IDLE_XGMII;
					end
				endcase
				tx_ready = 1'b0;
			end
		endcase
	end									
endmodule


