// ============================================================================
// Copyright (c) 2013 by Terasic Technologies Inc.
// ============================================================================
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// ============================================================================
//           
//  Terasic Technologies Inc
//  9F., No.176, Sec.2, Gongdao 5th Rd, East Dist, Hsinchu City, 30070. Taiwan
//  
//  
//                     web: http://www.terasic.com/  
//                     email: support@terasic.com
//
// ============================================================================
// ============================================================================
//
// Major Functions:	SoCKit_Default
//
// ============================================================================
// Revision History :
// ============================================================================
//  Authors: Christopher Campbell (cc3769@columbia.edu)
//           Jihua Li             (jl4345@columbia.edu)
//
//  Last Mod. Date: December 21st, 2015
// ============================================================================

`define ENABLE_MDIO

///// Top Level Module /////
module SoCKit_Top(

///// PORT DECLARATIONS /////

    ///////// HPS PINS ///////
    input  wire 			memory_oct_rzqin,    
    input  wire 			hps_io_hps_io_emac1_inst_RXD0,
    input  wire 			hps_io_hps_io_uart0_inst_RX,
    input  wire 			hps_io_hps_io_emac1_inst_RX_CTL, 
    input  wire 			hps_io_hps_io_emac1_inst_RX_CLK,       
    input  wire 			hps_io_hps_io_emac1_inst_RXD1,         
    input  wire 			hps_io_hps_io_emac1_inst_RXD2,         
    input  wire 			hps_io_hps_io_emac1_inst_RXD3, 
    input  wire 			hps_io_hps_io_usb1_inst_CLK,  
    input  wire 			hps_io_hps_io_usb1_inst_DIR,           
    input  wire 			hps_io_hps_io_usb1_inst_NXT,     
    input  wire 			hps_io_hps_io_spim0_inst_MISO,   
    input  wire 			hps_io_hps_io_spim1_inst_MISO,    
    output wire  [14:0]     memory_mem_a,     
    output wire  [2:0] 		memory_mem_ba,                         
    output wire 			memory_mem_ck,                         
    output wire 			memory_mem_ck_n,                       
    output wire 			memory_mem_cke,                        
    output wire 			memory_mem_cs_n,                       
    output wire 			memory_mem_ras_n,                      
    output wire 			memory_mem_cas_n,                      
    output wire 			memory_mem_we_n,                       
    output wire 			memory_mem_reset_n,                        
    output wire 			memory_mem_odt,                        
    output wire  [3:0] 		memory_mem_dm,                                     
    output wire 			hps_io_hps_io_emac1_inst_TX_CLK,       
    output wire 			hps_io_hps_io_emac1_inst_TXD0,         
    output wire 			hps_io_hps_io_emac1_inst_TXD1,         
    output wire 			hps_io_hps_io_emac1_inst_TXD2,         
    output wire 			hps_io_hps_io_emac1_inst_TXD3,                  
    output wire 			hps_io_hps_io_emac1_inst_MDC,          
    output wire 			hps_io_hps_io_emac1_inst_TX_CTL,              
    output wire 			hps_io_hps_io_qspi_inst_SS0,           
    output wire 			hps_io_hps_io_qspi_inst_CLK,           
    output wire 			hps_io_hps_io_sdio_inst_CLK,                      
    output wire 			hps_io_hps_io_usb1_inst_STP,            
    output wire 			hps_io_hps_io_spim0_inst_CLK,          
    output wire 			hps_io_hps_io_spim0_inst_MOSI,         
    output wire 			hps_io_hps_io_spim0_inst_SS0,          
    output wire 			hps_io_hps_io_spim1_inst_CLK,          
    output wire 			hps_io_hps_io_spim1_inst_MOSI,         
    output wire 			hps_io_hps_io_spim1_inst_SS0,           
    output wire 			hps_io_hps_io_uart0_inst_TX,  
    inout  wire  [31:0] 	memory_mem_dq,                         
    inout  wire  [3:0] 		memory_mem_dqs,                        
    inout  wire  [3:0] 		memory_mem_dqs_n,       
    inout  wire 			hps_io_hps_io_i2c1_inst_SDA,           
    inout  wire 			hps_io_hps_io_i2c1_inst_SCL,           
    inout  wire 			hps_io_hps_io_gpio_inst_GPIO00,  
    inout  wire 			hps_io_hps_io_emac1_inst_MDIO,
    inout  wire 			hps_io_hps_io_qspi_inst_IO0,           
    inout  wire 			hps_io_hps_io_qspi_inst_IO1,           
    inout  wire 			hps_io_hps_io_qspi_inst_IO2,           
    inout  wire 			hps_io_hps_io_qspi_inst_IO3,  
    inout  wire 			hps_io_hps_io_sdio_inst_CMD,           
    inout  wire 			hps_io_hps_io_sdio_inst_D0,            
    inout  wire 			hps_io_hps_io_sdio_inst_D1,   
    inout  wire 			hps_io_hps_io_sdio_inst_D2,            
    inout  wire 			hps_io_hps_io_sdio_inst_D3,            
    inout  wire 			hps_io_hps_io_usb1_inst_D0,            
    inout  wire 			hps_io_hps_io_usb1_inst_D1,            
    inout  wire 			hps_io_hps_io_usb1_inst_D2,            
    inout  wire 			hps_io_hps_io_usb1_inst_D3,            
    inout  wire 			hps_io_hps_io_usb1_inst_D4,            
    inout  wire 			hps_io_hps_io_usb1_inst_D5,            
    inout  wire 			hps_io_hps_io_usb1_inst_D6,            
    inout  wire 			hps_io_hps_io_usb1_inst_D7,                
	
    ///// HSMC /////
    input  logic			HSMC_CLKIN_p,
    input  logic  [3:0]     HSMC_XAUI_RX_p0,
    input  logic           	OPRXLOS2,
    input  logic           	OPTXFLT2,
    input  logic           	LASI2,
    input  logic           	SFP_TXDIS2,
    input  logic 			SFP_TXRS20,
    output logic  [3:0]     HSMC_XAUI_TX_p0,
    output logic          	MDC2,	
    output logic          	MDC1,
    output logic          	PRTAD02,
    output logic          	PRTAD01,
    output logic          	PRTAD4,
    output logic          	PRTAD3,
    output logic          	PRTAD2,
    output logic          	PRTAD1,
    output logic          	TXONOFF1,
    output logic          	TXONOFF2,
    output logic          	OPINLVL,
    output logic          	OPOUTLVL,
    output logic          	PHYRESET,       // active low
    output logic  [7:0] 	USER_LED_G,
    output logic  [7:0] 	USER_LED_R,
    output logic            CONFIG1_1,
    output logic            CONFIG0_1,
    output logic            CONFIG1_2,		
    output logic            CONFIG0_2,
    output logic            SS338_CLKIN,				
    output logic            SER_BOOT,
    output logic            SMBSPDSEL1,
    output logic            SMBSPDSEL2,	
    inout  logic         	MDIO2,
    inout  logic         	MDIO1,
    inout  logic            GPIO0_1,		
    inout  logic           	GPIO1_1,
    inout  logic            GPIO0_2,
    inout  logic           	GPIO1_2,
    inout  logic           	SMBWEN,						 
    inout  logic           	NVMA1SEL,
    inout  logic           	NVMPROT,			
    
    ///// SW /////
    input  [3:0]        SW,

    ///// KEY /////
    input  [3:0]        KEY,        // active low

    ///// LED /////
    output  [3:0]       LED,        // active high
    
    ///// SI5338 /////
    inout       SI5338_SCL,
    inout       SI5338_SDA,

    ///// CLOCK SOURCES /////
    input       OSC_50_B3B,
    input       OSC_50_B4A,
    input       OSC_50_B5B,
    input       OSC_50_B8A,
    
    ///// RESET /////
    input       fpga_rst_n
);
		  
///// REG/WIRE/LOGIC DECLARATIONS /////

    wire        	MDIN1;
    wire        	MDOEN1;
    wire        	MDO1;
    wire 			clk_ref;
    wire 			clk_rx;
    wire   [229:0]  reconfig_from_xcvr;
    wire   [349:0]  reconfig_to_xcvr;
    logic           mac_tx_ready;
    logic           mac_rx_ready;
    logic [71:0]   	xgmii_rx;
    logic [71:0]   	xgmiialigned;
    logic [71:0] 	xgmii_tx;
    logic [31:0] 	crclink;
    logic 			eop;
    logic 			sop;
    logic 			valid;
    logic 			ready;
    logic 			checksum_err;
    logic [2:0] 	empty;
    logic [63:0]    data;
    logic [1:0] 	state;
    logic 			eoptx;
    logic 			soptx;
    logic 			readytx;
    logic [2:0] 	emptytx;
    logic [63:0] 	stdata;
    logic 			validtx;
    logic [31:0] 	crclinktx;
    logic [31:0] 	cctx;
    logic [63:0] 	xgmiirevtx;
    logic [71:0] 	xgmiitx;

    //parameter idle = 0, trig = 1, lock = 2;
    
///// STRUCTURAL CODE /////

    assign CONFIG0_1 = 1'b1;	// Configure BCM8727 from EEPROM
    assign CONFIG1_1 = 1'b0;
    assign CONFIG0_2 = 1'b1;
    assign CONFIG1_2 = 1'b0;
    assign SS338_CLKIN	= 1'b0;

    // BCM8272C allow spi-rom to be removed 
    // 1 = Boot microcode from spi proms
    assign SER_BOOT  = 1'b0;	
    assign SMBSPDSEL1 = 1'b0; 
    assign SMBSPDSEL2 = 1'b0; 
    assign SMBWEN    = 1'b1;
    assign GPIO0_1   = 1'b0;
    assign GPIO1_1   = 1'b0;
    assign GPIO0_2   = 1'b0;
    assign GPIO1_2   = 1'b0;

    // 1: EEPROM Slave Addr 52, 0: 50 addr: 
    // During deassertion of BCM8727 reset, 
    // latched into bit 10 of register 1.8002h 
    assign NVMA1SEL = 1'b1;

    // When high protect non volatile memory 
    assign NVMPROT  = 1'b0;			

    `ifdef ENABLE_MDIO
    assign MDIO1 = !MDOEN1? MDO1 : 1'bz;
    assign MDIN1 = MDIO1;
    `else
    assign MDC1	 = 1'bz;
    assign MDIO1 = 1'bz;
    `endif
    assign MDC2	 = 1'bz;
    assign MDIO2 = 1'bz;

    assign {PRTAD4,PRTAD3,PRTAD2,PRTAD1,PRTAD01} = 5'b00000;
    assign  PRTAD02 = 1'b0;

    assign TXONOFF1 = 1'b1;
    assign TXONOFF2 = 1'b1;
    assign OPOUTLVL = 1'b0;				// 0 for active low OPTXENB/OPTXRST
    assign OPINLVL  = 1'b1;				// 1 for active high OPRXLOS/TXONOFF
    
    assign ETHRESET= KEY[0];
    assign PHYRESET =  KEY[1];
    
    assign LED[0] = ETHRESET;
    assign LED[1] = PHYRESET;
    
///// MODULES /////

    ///// XAUI  PHY /////  
    xaui phy(
        .pll_ref_clk			(clk_ref),
        .xgmii_tx_clk		    (clk_rx),
        .xgmii_rx_clk		    (clk_rx),
        .xgmii_rx_dc		    (xgmii_rx),
        .xgmii_tx_dc            (xgmii_tx),
        .xaui_rx_serial_data	(HSMC_XAUI_RX_p0),
        .xaui_tx_serial_data	(HSMC_XAUI_TX_p0),
        .rx_ready				(LED[2]),
        .tx_ready				(LED[3]),
        .phy_mgmt_clk		    (OSC_50_B4A),
        .phy_mgmt_clk_reset		(!ETHRESET),        // active high
        .phy_mgmt_address	    (3'h000),
        .phy_mgmt_read          (0),
        .phy_mgmt_readdata      (8'hFFFFFFFF),
        .phy_mgmt_write         (0),
        .phy_mgmt_writedata     (8'h00000000),
        .phy_mgmt_waitrequest   (0),
        .reconfig_from_xcvr     (reconfig_from_xcvr),
        .reconfig_to_xcvr       (reconfig_to_xcvr)
    ); 

    ///// SWAP /////
    xgmii swap (
        .clk(clk_rx), 
        .xgmiidata(xgmii_rx),      // input data
        .reset(!ETHRESET),
        .flag(),
        .xgmii(xgmiialigned)       // output data
    );
    
    ///// RX XGE MAC /////
    mac_rx  rx (
        .clk(clk_rx),
        .rst(!ETHRESET),
        .rx_data(data),
        .rx_sop(sop),
        .rx_eop(eop),
        .rx_valid(valid),
        .rx_empty(empty),
        .rx_ready(ready),
        .rx_xgmii(xgmiialigned)
    );
    
    ///// TX XGE MAC /////
    mac_tx tx (
        .clk(clk_rx),
        .rst(!ETHRESET), 
        .tx_data(stdata),
        .tx_sop(soptx),
        .tx_eop(eoptx),
        .tx_valid(validtx),
        .tx_empty(emptytx),
        .tx_ready(readytx),
        .tx_xgmii(xgmiirevtx)
    );
    
blazepps u0 (
    .clk_clk                                    (OSC_50_B4A),
    .hps_0_h2f_reset_reset_n                    (),
    .hps_io_hps_io_emac1_inst_TX_CLK 	        (hps_io_hps_io_emac1_inst_TX_CLK),
    .hps_io_hps_io_emac1_inst_TXD0 		        (hps_io_hps_io_emac1_inst_TXD0),
    .hps_io_hps_io_emac1_inst_TXD1   	        (hps_io_hps_io_emac1_inst_TXD1),
    .hps_io_hps_io_emac1_inst_TXD2   		    (hps_io_hps_io_emac1_inst_TXD2),
    .hps_io_hps_io_emac1_inst_TXD3   		    (hps_io_hps_io_emac1_inst_TXD3),
    .hps_io_hps_io_emac1_inst_RXD0   		    (hps_io_hps_io_emac1_inst_RXD0),
    .hps_io_hps_io_emac1_inst_MDIO   		    (hps_io_hps_io_emac1_inst_MDIO),
    .hps_io_hps_io_emac1_inst_MDC    		    (hps_io_hps_io_emac1_inst_MDC),
    .hps_io_hps_io_emac1_inst_RX_CTL 		    (hps_io_hps_io_emac1_inst_RX_CTL),
    .hps_io_hps_io_emac1_inst_TX_CTL 		    (hps_io_hps_io_emac1_inst_TX_CTL),
    .hps_io_hps_io_emac1_inst_RX_CLK 		    (hps_io_hps_io_emac1_inst_RX_CLK),
    .hps_io_hps_io_emac1_inst_RXD1   		    (hps_io_hps_io_emac1_inst_RXD1),
    .hps_io_hps_io_emac1_inst_RXD2   		    (hps_io_hps_io_emac1_inst_RXD2),
    .hps_io_hps_io_emac1_inst_RXD3   		    (hps_io_hps_io_emac1_inst_RXD3),
    .hps_io_hps_io_qspi_inst_IO0     		    (hps_io_hps_io_qspi_inst_IO0),
    .hps_io_hps_io_qspi_inst_IO1     		    (hps_io_hps_io_qspi_inst_IO1),
    .hps_io_hps_io_qspi_inst_IO2     		    (hps_io_hps_io_qspi_inst_IO2),
    .hps_io_hps_io_qspi_inst_IO3     		    (hps_io_hps_io_qspi_inst_IO3),
    .hps_io_hps_io_qspi_inst_SS0     		    (hps_io_hps_io_qspi_inst_SS0),
    .hps_io_hps_io_qspi_inst_CLK     		    (hps_io_hps_io_qspi_inst_CLK),
    .hps_io_hps_io_sdio_inst_CMD     		    (hps_io_hps_io_sdio_inst_CMD),
    .hps_io_hps_io_sdio_inst_D0     		    (hps_io_hps_io_sdio_inst_D0),
    .hps_io_hps_io_sdio_inst_D1      		    (hps_io_hps_io_sdio_inst_D1),
    .hps_io_hps_io_sdio_inst_CLK    		    (hps_io_hps_io_sdio_inst_CLK),
    .hps_io_hps_io_sdio_inst_D2   		        (hps_io_hps_io_sdio_inst_D2),
    .hps_io_hps_io_sdio_inst_D3      		    (hps_io_hps_io_sdio_inst_D3),
    .hps_io_hps_io_usb1_inst_D0      		    (hps_io_hps_io_usb1_inst_D0),
    .hps_io_hps_io_usb1_inst_D1      		    (hps_io_hps_io_usb1_inst_D1),
    .hps_io_hps_io_usb1_inst_D2     		    (hps_io_hps_io_usb1_inst_D2), 
    .hps_io_hps_io_usb1_inst_D3     		    (hps_io_hps_io_usb1_inst_D3),
    .hps_io_hps_io_usb1_inst_D4     		    (hps_io_hps_io_usb1_inst_D4),
    .hps_io_hps_io_usb1_inst_D5      		    (hps_io_hps_io_usb1_inst_D5),
    .hps_io_hps_io_usb1_inst_D6     		    (hps_io_hps_io_usb1_inst_D6),
    .hps_io_hps_io_usb1_inst_D7     		    (hps_io_hps_io_usb1_inst_D7),
    .hps_io_hps_io_usb1_inst_CLK    		    (hps_io_hps_io_usb1_inst_CLK),
    .hps_io_hps_io_usb1_inst_STP    		    (hps_io_hps_io_usb1_inst_STP),
    .hps_io_hps_io_usb1_inst_DIR    		    (hps_io_hps_io_usb1_inst_DIR),
    .hps_io_hps_io_usb1_inst_NXT    		    (hps_io_hps_io_usb1_inst_NXT),
    .hps_io_hps_io_spim0_inst_CLK   		    (hps_io_hps_io_spim0_inst_CLK),
    .hps_io_hps_io_spim0_inst_MOSI  		    (hps_io_hps_io_spim0_inst_MOSI),
    .hps_io_hps_io_spim0_inst_MISO  		    (hps_io_hps_io_spim0_inst_MISO),
    .hps_io_hps_io_spim0_inst_SS0   		    (hps_io_hps_io_spim0_inst_SS0),
    .hps_io_hps_io_spim1_inst_CLK   		    (hps_io_hps_io_spim1_inst_CLK),
    .hps_io_hps_io_spim1_inst_MOSI  		    (hps_io_hps_io_spim1_inst_MOSI),  
    .hps_io_hps_io_spim1_inst_MISO  		    (hps_io_hps_io_spim1_inst_MISO),
    .hps_io_hps_io_spim1_inst_SS0     	        (hps_io_hps_io_spim1_inst_SS0),
    .hps_io_hps_io_uart0_inst_RX     		    (hps_io_hps_io_uart0_inst_RX),
    .hps_io_hps_io_uart0_inst_TX     		    (hps_io_hps_io_uart0_inst_TX),
    .hps_io_hps_io_i2c1_inst_SDA     		    (hps_io_hps_io_i2c1_inst_SDA),
    .hps_io_hps_io_i2c1_inst_SCL     		    (hps_io_hps_io_i2c1_inst_SCL), 
    .memory_mem_a                               (memory_mem_a),
    .memory_mem_ba                              (memory_mem_ba),
    .memory_mem_ck                              (memory_mem_ck),
    .memory_mem_ck_n                            (memory_mem_ck_n),
    .memory_mem_cke                             (memory_mem_cke),
    .memory_mem_cs_n                            (memory_mem_cs_n),
    .memory_mem_ras_n                           (memory_mem_ras_n),
    .memory_mem_cas_n                           (memory_mem_cas_n),
    .memory_mem_we_n                            (memory_mem_we_n),
    .memory_mem_reset_n                         (memory_mem_reset_n),
    .memory_mem_dq                              (memory_mem_dq),
    .memory_mem_dqs                             (memory_mem_dqs),
    .memory_mem_dqs_n                           (memory_mem_dqs_n),
    .memory_mem_odt                             (memory_mem_odt),
    .memory_mem_dm                              (memory_mem_dm),
    .memory_oct_rzqin                           (memory_oct_rzqin),
    .ref_pll_clk_clk                            (clk_ref),
    .reset_reset_n                              (ETHRESET),           // active low
    .xaui_clk_clk                               (clk_rx),
    .xaui_reset_reset_n                         (ETHRESET),           // active low  
    .xcvr_reconfig_cal_busy_in_cal_busy_in      (0),
    .xcvr_reconfig_from_xcvr_reconfig_from_xcvr (reconfig_from_xcvr),
    .xcvr_reconfig_to_xcvr_reconfig_to_xcvr     (reconfig_to_xcvr),
    .xge_rx_sc_fifo_in_data                     (data),
    .xge_rx_sc_fifo_in_valid                    (valid),
    .xge_rx_sc_fifo_in_ready                    (ready),
    .xge_rx_sc_fifo_in_startofpacket            (sop),
    .xge_rx_sc_fifo_in_endofpacket              (eop),
    .xge_rx_sc_fifo_in_empty                    (empty),
    .xge_tx_sc_fifo_out_data                    (stdata),
    .xge_tx_sc_fifo_out_valid                   (validtx),
    .xge_tx_sc_fifo_out_ready                   (readytx),
    .xge_tx_sc_fifo_out_startofpacket           (soptx),
    .xge_tx_sc_fifo_out_endofpacket             (eoptx),
    .xge_tx_sc_fifo_out_empty                   (emptytx)
);

endmodule

