`timescale 1ns / 1ns
module test;
	reg clk = 1;
	wire [63:0] dataout;	
	reg reset = 1;
	wire sopout;
	wire eopout;
	wire validout;
	wire [2:0]	emptyout;
	reg 		ready=1;
	reg [71:0] xgmii=72'h83c1e0f0783c1e0f07;
	reg [3:0] ptr = 0;
	wire [31:0] crc_out;
	reg en = 0;
	reg [71:0] data [0:12];
	initial $readmemh ("data.txt",data);

	initial begin
		$dumpfile("mac_test.vcd");
		$dumpvars(3, test);
		# 5  reset = 0;
		# 180
			 $stop;
	end
	always #5 begin
		clk = !clk;
	end 
	always begin
		forever #10 begin
			xgmii = data[ptr];
			ptr=ptr +1;
		end
	end
	mac_rx mac(clk, reset, dataout, sopout, eopout, validout, emptyout, ready, xgmii);
endmodule
