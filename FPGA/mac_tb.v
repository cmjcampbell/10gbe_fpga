`timescale 1ns / 1ns
module test;
	reg clk = 1;
	reg [63:0] datain = 0;	
	reg reset = 1;
	reg sopin = 0;
	reg eopin = 0;
	reg valid = 0;
	reg [2:0]	empty = 0;
	wire 		ready;
	wire [71:0] xgmii;
	reg [3:0] ptr = 0;
	wire [31:0] crc_out;
	reg en = 0;
	reg [63:0] data [0:12];
	initial $readmemh ("out.txt",data);

	initial begin
		$dumpfile("mac_test.vcd");
		$dumpvars(2, test);
		# 5  reset = 0;
		# 6	 sopin = 1;
			 valid = 1;
		# 10  sopin = 0;
		# 90  eopin = 1;
			  empty = 0;
		# 10  valid = 0;
			  eopin = 0;
			  empty = 0;
		# 50  sopin = 1;
			  valid = 1;
	    # 10  sopin = 0;
	    # 90  eopin = 1;
	    	  empty = 2;
	    # 10  valid = 0;
	    	  eopin = 0;
	    	  empty = 0;
		# 30
			 $stop;
	end
	always #5 begin
		clk = !clk;
	end 
	always begin
		#1
		forever #10 begin
			datain = data[ptr];
			ptr=ptr +1;
		end
	end
	mac_tx mac(clk, reset, datain, sopin, eopin, valid, empty, ready, xgmii);
	initial 
		//$monitor("@%t, reset=%d, datain = %h, sop=%d, eop=%d, valid=%d, empty=%d, ready=%d, xgmii = %h",
		//		 $time,reset, datain, sopin, eopin, valid, empty, ready, xgmii);
		$monitor("0x%h", xgmii);
endmodule
