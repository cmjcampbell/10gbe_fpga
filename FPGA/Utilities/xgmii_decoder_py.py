import sys
from math import log

def main(argv):

    print("*** XGMII DECODER ***")
    print("(all input/output data should have the msb on "+
                        "the left and the lsb on the right)")
    print

    xgmii_hex = argv[0]    
    xgmii_binary = bin(int(xgmii_hex, 16))[2:].zfill(len(xgmii_hex) * 4).rstrip("L")

    print("* XGMII *")
    print(xgmii_hex.upper())
    print(xgmii_binary)
    print

    data_binary = ""
    i = -1
    for bit in xgmii_binary:
        i = (i + 1) % 9
        if (i == 0):
            continue
        data_binary += bit 

    data_binary = data_binary.zfill((len(data_binary) % 4) + len(data_binary))
    data_hex = (hex(int(data_binary,2))[2:].upper()).zfill(int(len(data_binary) / 4)).rstrip("L")

    print("* DATA *")
    print(data_hex)
    print(data_binary)
    print

if __name__ == "__main__":
    main(sys.argv[1:])
