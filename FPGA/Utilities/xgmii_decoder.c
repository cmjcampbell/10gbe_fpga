#include <stdio.h>
#include <string.h>
int main()
{
	char s[] = "6A954AA552A954ABFB";
	//char s[] = "2A954AAD5FD954AA55";
	//char s[] = "00004001700800BC00";
	char partial_input[10];
	int ind =0;
	int partial_data = 0;
	memcpy(partial_input,s,9);
	long long int temp_int, data;			//64 bits containing half of xgmii data
	sscanf(partial_input,"%llx",&temp_int);
	for(;ind < 4; ind++){
		partial_data |= (temp_int & 0xff);
		temp_int >>=9;
		if(ind <3){
			partial_data <<=8;
		}
	}
	printf("low=%x\n", partial_data);
	partial_data =0;

	memcpy(partial_input,s+9,9);
	sscanf(partial_input,"%llx",&temp_int);
	for(ind = 0; ind<4; ind++){
		partial_data |= (temp_int & 0xff);
		temp_int >>=9;
		if(ind <3){
			partial_data <<=8;
		}
	}
	printf("high=%x\n", partial_data);
}
