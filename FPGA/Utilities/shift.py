import re
import sys
if __name__ == '__main__':
	shift = int(sys.argv[1])
	f = open("input.v", "r")
	w = open("output.v", "w")
	for line in f:
		replaced = re.sub(r'data_in\[(.*?)\]', lambda x: 'data[' + str(int(x.group(1)) + shift) + ']', line)
		w.write(replaced)
	w.close()
