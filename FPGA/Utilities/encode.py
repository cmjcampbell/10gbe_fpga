#!/usr/local/bin/python
from bitstring import BitStream
import bitstring
import sys
f = open(sys.argv[1],'r')
f = f.read().split('\n')
del f[-1]
s = BitStream()
for lines in f:
	s += bitstring.pack('bin:1, hex:8', lines.split(' ')[0], lines.split(' ')[1])
print s.read('hex:72')
